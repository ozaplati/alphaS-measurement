dijet2015_ABM16_yStar1_pereventscale chi2= 2.3447e+01 pValue= 7.1036e-01  for 28 bins
dijet2015_ABM16_yStar2_pereventscale chi2= 3.1140e+01 pValue= 3.1088e-01  for 28 bins
dijet2015_ABM16_yStar3_pereventscale chi2= 2.2896e+01 pValue= 6.9056e-01  for 27 bins
dijet2015_ABM16_yStar4_pereventscale chi2= 2.8229e+01 pValue= 2.5051e-01  for 24 bins
dijet2015_ABM16_yStar5_pereventscale chi2= 2.5819e+01 pValue= 2.1344e-01  for 21 bins
dijet2015_ABM16_yStar6_pereventscale chi2= 8.5985e+00 pValue= 3.7729e-01  for 8 bins

dijet2015_CT14_yStar1_pereventscale chi2= 2.1878e+01 pValue= 7.8689e-01  for 28 bins
dijet2015_CT14_yStar2_pereventscale chi2= 3.2137e+01 pValue= 2.6897e-01  for 28 bins
dijet2015_CT14_yStar3_pereventscale chi2= 2.3442e+01 pValue= 6.6103e-01  for 27 bins
dijet2015_CT14_yStar4_pereventscale chi2= 2.8065e+01 pValue= 2.5731e-01  for 24 bins
dijet2015_CT14_yStar5_pereventscale chi2= 2.1560e+01 pValue= 4.2524e-01  for 21 bins
dijet2015_CT14_yStar6_pereventscale chi2= 7.8436e+00 pValue= 4.4889e-01  for 8 bins

dijet2015_HERAPDF20NLO_yStar1_pereventscale chi2= 2.3537e+01 pValue= 7.0573e-01  for 28 bins
dijet2015_HERAPDF20NLO_yStar2_pereventscale chi2= 3.0868e+01 pValue= 3.2296e-01  for 28 bins
dijet2015_HERAPDF20NLO_yStar3_pereventscale chi2= 2.3408e+01 pValue= 6.6294e-01  for 27 bins
dijet2015_HERAPDF20NLO_yStar4_pereventscale chi2= 3.3232e+01 pValue= 9.9276e-02  for 24 bins
dijet2015_HERAPDF20NLO_yStar5_pereventscale chi2= 3.3387e+01 pValue= 4.2101e-02  for 21 bins
dijet2015_HERAPDF20NLO_yStar6_pereventscale chi2= 1.0193e+01 pValue= 2.5173e-01  for 8 bins

dijet2015_MMHT2014_yStar1_pereventscale chi2= 2.5622e+01 pValue= 5.9382e-01  for 28 bins
dijet2015_MMHT2014_yStar2_pereventscale chi2= 3.3152e+01 pValue= 2.3021e-01  for 28 bins
dijet2015_MMHT2014_yStar3_pereventscale chi2= 2.5425e+01 pValue= 5.5065e-01  for 27 bins
dijet2015_MMHT2014_yStar4_pereventscale chi2= 2.8010e+01 pValue= 2.5962e-01  for 24 bins
dijet2015_MMHT2014_yStar5_pereventscale chi2= 2.2849e+01 pValue= 3.5211e-01  for 21 bins
dijet2015_MMHT2014_yStar6_pereventscale chi2= 7.7102e+00 pValue= 4.6227e-01  for 8 bins

dijet2015_NNPDF30nlo_yStar1_pereventscale chi2= 2.7337e+01 pValue= 4.9995e-01  for 28 bins
dijet2015_NNPDF30nlo_yStar2_pereventscale chi2= 3.4305e+01 pValue= 1.9100e-01  for 28 bins
dijet2015_NNPDF30nlo_yStar3_pereventscale chi2= 2.6688e+01 pValue= 4.8071e-01  for 27 bins
dijet2015_NNPDF30nlo_yStar4_pereventscale chi2= 2.7553e+01 pValue= 2.7935e-01  for 24 bins
dijet2015_NNPDF30nlo_yStar5_pereventscale chi2= 2.3610e+01 pValue= 3.1233e-01  for 21 bins
dijet2015_NNPDF30nlo_yStar6_pereventscale chi2= 8.3043e+00 pValue= 4.0433e-01  for 8 bins

dijet2015_ABM16_yStar1yStar2yStar3yStar4yStar5yStar6_pereventscale chi2= 1.6541e+02 pValue= 4.3760e-02  for 136 bins
dijet2015_CT14_yStar1yStar2yStar3yStar4yStar5yStar6_pereventscale chi2= 1.5964e+02 pValue= 8.1122e-02  for 136 bins
dijet2015_HERAPDF20NLO_yStar1yStar2yStar3yStar4yStar5yStar6_pereventscale chi2= 1.9589e+02 pValue= 5.9164e-04  for 136 bins
dijet2015_MMHT2014_yStar1yStar2yStar3yStar4yStar5yStar6_pereventscale chi2= 1.6332e+02 pValue= 5.5154e-02  for 136 bins
dijet2015_NNPDF30nlo_yStar1yStar2yStar3yStar4yStar5yStar6_pereventscale chi2= 1.5776e+02 pValue= 9.7646e-02  for 136 bins


incljet2015_ABM16_eta1_pereventscale chi2= 3.2376e+01 pValue= 4.9799e-01  for 33 bins
incljet2015_ABM16_eta1_perjetscale chi2= 3.3050e+01 pValue= 4.6483e-01  for 33 bins
incljet2015_ABM16_eta2_pereventscale chi2= 5.0455e+01 pValue= 2.0143e-02  for 32 bins
incljet2015_ABM16_eta2_perjetscale chi2= 5.0491e+01 pValue= 1.9980e-02  for 32 bins
incljet2015_ABM16_eta3_pereventscale chi2= 2.9305e+01 pValue= 5.5335e-01  for 31 bins
incljet2015_ABM16_eta3_perjetscale chi2= 2.9535e+01 pValue= 5.4138e-01  for 31 bins
incljet2015_ABM16_eta4_pereventscale chi2= 5.5597e+01 pValue= 4.3004e-03  for 31 bins
incljet2015_ABM16_eta4_perjetscale chi2= 5.4939e+01 pValue= 5.0808e-03  for 31 bins
incljet2015_ABM16_eta5_pereventscale chi2= 4.6520e+01 pValue= 1.5387e-02  for 28 bins
incljet2015_ABM16_eta5_perjetscale chi2= 4.2845e+01 pValue= 3.6028e-02  for 28 bins
incljet2015_ABM16_eta6_pereventscale chi2= 2.0603e+01 pValue= 5.4539e-01  for 22 bins
incljet2015_ABM16_eta6_perjetscale chi2= 1.9842e+01 pValue= 5.9292e-01  for 22 bins


incljet2015_CT14_eta1_pereventscale chi2= 2.8832e+01 pValue= 6.7486e-01  for 33 bins
incljet2015_CT14_eta1_perjetscale chi2= 2.8498e+01 pValue= 6.9091e-01  for 33 bins
incljet2015_CT14_eta2_pereventscale chi2= 4.5469e+01 pValue= 5.7813e-02  for 32 bins
incljet2015_CT14_eta2_perjetscale chi2= 4.4177e+01 pValue= 7.4344e-02  for 32 bins
incljet2015_CT14_eta3_pereventscale chi2= 2.7535e+01 pValue= 6.4504e-01  for 31 bins
incljet2015_CT14_eta3_perjetscale chi2= 2.6589e+01 pValue= 6.9266e-01  for 31 bins
incljet2015_CT14_eta4_pereventscale chi2= 5.3532e+01 pValue= 7.2156e-03  for 31 bins
incljet2015_CT14_eta4_perjetscale chi2= 5.0938e+01 pValue= 1.3472e-02  for 31 bins
incljet2015_CT14_eta5_pereventscale chi2= 4.4887e+01 pValue= 2.2649e-02  for 28 bins
incljet2015_CT14_eta5_perjetscale chi2= 3.8659e+01 pValue= 8.6570e-02  for 28 bins
incljet2015_CT14_eta6_pereventscale chi2= 1.9357e+01 pValue= 6.2322e-01  for 22 bins
incljet2015_CT14_eta6_perjetscale chi2= 1.8893e+01 pValue= 6.5191e-01  for 22 bins

incljet2015_HERAPDF20NLO_eta1_pereventscale chi2= 3.6460e+01 pValue= 3.1089e-01  for 33 bins
incljet2015_HERAPDF20NLO_eta1_perjetscale chi2= 3.6643e+01 pValue= 3.0350e-01  for 33 bins
incljet2015_HERAPDF20NLO_eta2_pereventscale chi2= 4.8686e+01 pValue= 2.9710e-02  for 32 bins
incljet2015_HERAPDF20NLO_eta2_perjetscale chi2= 4.8096e+01 pValue= 3.3704e-02  for 32 bins
incljet2015_HERAPDF20NLO_eta3_pereventscale chi2= 3.0252e+01 pValue= 5.0430e-01  for 31 bins
incljet2015_HERAPDF20NLO_eta3_perjetscale chi2= 3.1226e+01 pValue= 4.5485e-01  for 31 bins
incljet2015_HERAPDF20NLO_eta4_pereventscale chi2= 6.0247e+01 pValue= 1.2623e-03  for 31 bins
incljet2015_HERAPDF20NLO_eta4_perjetscale chi2= 6.0125e+01 pValue= 1.3049e-03  for 31 bins
incljet2015_HERAPDF20NLO_eta5_pereventscale chi2= 4.9608e+01 pValue= 7.1509e-03  for 28 bins
incljet2015_HERAPDF20NLO_eta5_perjetscale chi2= 4.8324e+01 pValue= 9.8861e-03  for 28 bins
incljet2015_HERAPDF20NLO_eta6_pereventscale chi2= 2.6051e+01 pValue= 2.4948e-01  for 22 bins
incljet2015_HERAPDF20NLO_eta6_perjetscale chi2= 2.5422e+01 pValue= 2.7733e-01  for 22 bins

incljet2015_MMHT2014_eta1_pereventscale chi2= 2.9325e+01 pValue= 6.5075e-01  for 33 bins
incljet2015_MMHT2014_eta1_perjetscale chi2= 2.8967e+01 pValue= 6.6829e-01  for 33 bins
incljet2015_MMHT2014_eta2_pereventscale chi2= 4.5058e+01 pValue= 6.2695e-02  for 32 bins
incljet2015_MMHT2014_eta2_perjetscale chi2= 4.3196e+01 pValue= 8.9396e-02  for 32 bins
incljet2015_MMHT2014_eta3_pereventscale chi2= 2.8147e+01 pValue= 6.1358e-01  for 31 bins
incljet2015_MMHT2014_eta3_perjetscale chi2= 2.8070e+01 pValue= 6.1756e-01  for 31 bins
incljet2015_MMHT2014_eta4_pereventscale chi2= 5.3183e+01 pValue= 7.8612e-03  for 31 bins
incljet2015_MMHT2014_eta4_perjetscale chi2= 5.0085e+01 pValue= 1.6431e-02  for 31 bins
incljet2015_MMHT2014_eta5_pereventscale chi2= 4.4743e+01 pValue= 2.3420e-02  for 28 bins
incljet2015_MMHT2014_eta5_perjetscale chi2= 4.0019e+01 pValue= 6.5867e-02  for 28 bins
incljet2015_MMHT2014_eta6_pereventscale chi2= 1.7941e+01 pValue= 7.0945e-01  for 22 bins
incljet2015_MMHT2014_eta6_perjetscale chi2= 1.7813e+01 pValue= 7.1702e-01  for 22 bins

incljet2015_NNPDF30nlo_eta1_pereventscale chi2= 2.9999e+01 pValue= 6.1729e-01  for 33 bins
incljet2015_NNPDF30nlo_eta1_perjetscale chi2= 2.9155e+01 pValue= 6.5909e-01  for 33 bins
incljet2015_NNPDF30nlo_eta2_pereventscale chi2= 4.5303e+01 pValue= 5.9745e-02  for 32 bins
incljet2015_NNPDF30nlo_eta2_perjetscale chi2= 4.3382e+01 pValue= 8.6362e-02  for 32 bins
incljet2015_NNPDF30nlo_eta3_pereventscale chi2= 2.7127e+01 pValue= 6.6576e-01  for 31 bins
incljet2015_NNPDF30nlo_eta3_perjetscale chi2= 2.6930e+01 pValue= 6.7567e-01  for 31 bins
incljet2015_NNPDF30nlo_eta4_pereventscale chi2= 5.3134e+01 pValue= 7.9565e-03  for 31 bins
incljet2015_NNPDF30nlo_eta4_perjetscale chi2= 5.0784e+01 pValue= 1.3968e-02  for 31 bins
incljet2015_NNPDF30nlo_eta5_pereventscale chi2= 4.4001e+01 pValue= 2.7776e-02  for 28 bins
incljet2015_NNPDF30nlo_eta5_perjetscale chi2= 3.9465e+01 pValue= 7.3735e-02  for 28 bins
incljet2015_NNPDF30nlo_eta6_pereventscale chi2= 1.8262e+01 pValue= 6.9032e-01  for 22 bins
incljet2015_NNPDF30nlo_eta6_perjetscale chi2= 1.7763e+01 pValue= 7.1997e-01  for 22 bins


incljet2015_ABM16_eta1eta2eta3eta4eta5eta6_pereventscale chi2= 4.7503e+02 pValue= 4.2083e-29  for 177 bins
incljet2015_ABM16_eta1eta2eta3eta4eta5eta6_perjetscale chi2= 4.5461e+02 pValue= 2.5108e-26  for 177 bins
incljet2015_CT14_eta1eta2eta3eta4eta5eta6_pereventscale chi2= 4.1929e+02 pValue= 1.0440e-21  for 177 bins
incljet2015_CT14_eta1eta2eta3eta4eta5eta6_perjetscale chi2= 3.9914e+02 pValue= 3.4588e-19  for 177 bins
incljet2015_HERAPDF20NLO_eta1eta2eta3eta4eta5eta6_pereventscale chi2= 4.3206e+02 pValue= 2.3819e-23  for 177 bins
incljet2015_HERAPDF20NLO_eta1eta2eta3eta4eta5eta6_perjetscale chi2= 4.2761e+02 pValue= 8.9866e-23  for 177 bins
incljet2015_MMHT2014_eta1eta2eta3eta4eta5eta6_pereventscale chi2= 4.3052e+02 pValue= 3.7792e-23  for 177 bins
incljet2015_MMHT2014_eta1eta2eta3eta4eta5eta6_perjetscale chi2= 4.0478e+02 pValue= 6.9578e-20  for 177 bins
incljet2015_NNPDF30nlo_eta1eta2eta3eta4eta5eta6_pereventscale chi2= 4.0392e+02 pValue= 8.8920e-20  for 177 bins
incljet2015_NNPDF30nlo_eta1eta2eta3eta4eta5eta6_perjetscale chi2= 3.8441e+02 pValue= 2.0956e-17  for 177 bins


incljet2015_CT14_eta1eta2eta3eta4eta5eta6_pereventscale_SplitSyst_JES_Flavour_Response_Opt7_JES_MJB_Fragmentation_Opt17_JES_Pileup_Rho_topology_Opt18_isu2ndOption_Opt17_AltScC_Opt7_NP_Opt7 chi2= 3.8412e+02 pValue= 2.2688e-17  for 177 bins

incljet2015_CT14_eta1eta2eta3eta4eta5eta6_pereventscale_SplitSyst_JES_Flavour_Response_Opt7_JES_MJB_Fragmentation_Opt17_JES_Pileup_Rho_topology_Opt18_isu2ndOption_Opt20_AltScC_Opt17_NP_Opt7 chi2= 3.6127e+02 pValue= 1.0206e-14  for 177 bins

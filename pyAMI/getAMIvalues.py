import pyAMI.client
import pyAMI.atlas.api as AtlasAPI
import os, sys

client = pyAMI.client.Client('atlas')
AtlasAPI.init()

######################################################################################################
# EDIT
######################################################################################################

useEVNT = True

samples = [
      "mc16_13TeV.364700.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364701.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364705.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364706.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364709.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364710.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364711.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
      "mc16_13TeV.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",  
  #"mc16_13TeV.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749"
  #"mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_STDM11.e3668_s3126_r9364_r9315_p3749"
  #"mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_STDM11.e3569_s3126_r9364_p3749"
  #"SAMPLE_1" ,
  #"SAMPLE_2" ,
]

###########################################################################################################
# DO NOT MODIFY
###########################################################################################################

# Loop over samples
for dataset in samples:
  run = dataset.split(".",2)
  info = str(run[1])
  prov = AtlasAPI.get_dataset_prov(client,dataset)
  arr = prov['node']
  for dict in arr:
    d = dict['logicalDatasetName']
    if run[1] in d and "minbias" not in d:
      if not useEVNT:
        if ".AOD." in d:
         dd = AtlasAPI.get_dataset_info(client,d)
         xsec = dd[0]['crossSection']
         eff = dd[0]['genFiltEff']
         break
      elif "evgen" in d:
        dd = AtlasAPI.get_dataset_info(client,d)
        xsec = dd[0]['crossSection']
        eff = dd[0]['genFiltEff']
        break
  info += " " + str(xsec) + " " + str(eff) 
  print info			  

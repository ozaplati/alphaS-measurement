Workflow to contribute to this repository

- Clone repository:

git clone https://gitlab.cern.ch/ozaplati/alphaS-measurement.git


- Create new local branch:

git checkout -b BRANCH_NAME

- Make changes

- Add and commit to stash:
 
git add ...

git commit ..

- Make sure you are still up-to-date:

git pull --rebase origin master 

- Push to remote branch:

git push origin BRANCH_NAME

Recommended Git tutorial:

https://twiki.cern.ch/twiki/bin/view/AtlasComputing/GitTutorial

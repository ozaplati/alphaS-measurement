#!/usr/bin/python

import os, sys

param=sys.argv

##################################################################################
# Edit
##################################################################################
sampleType = "MC16"

#######
# JETM1
mcSample = "/eos/home-o/ozaplati/yAnalysis/DAOD/DAOD_JETM1/mc16_13TeV/DAOD_JETM1.16824998._000224.pool.root.1"

#localPATH = "PATH"
localPATH = '/afs/cern.ch/work/o/ozaplati/public/yAnalysis/WorkAreaII/'

#rel21 = False # Meaningless for MC15
rel21 = True # Meaningless for MC15
JETM1Kernel = True
test = False

##################################################################################
# DO NOT MODIFY
##################################################################################

Logs = "mkdir -p Logs"
print Logs
os.system(Logs)

# See if MC or data
MC = True
if "data" in sampleType:
  MC = False

# Protection
if sampleType == "MC16" or sampleType == "data17":
  rel21 = True
if sampleType == "MC15":
  rel21 = False

command = ""

# Choose appropiate JSON file
dataConfig = "xah_run_DATA.json"
mcConfig   = "xah_run_MC.json"
if rel21 and MC:
  if JETM1Kernel == True:
    mcConfig = "JETM1_test.json"
  else:
    mcConfig   = "xah_run_rel21_MC.json"
  
  localPATH += "/source"
if rel21 and not MC:
  if "15" in sampleType:
    dataConfig = "xah_run_rel21_DATA_2015.json"
  if "16" in sampleType:
    dataConfig = "xah_run_rel21_DATA_2016.json"
  if "17" in sampleType:
    dataConfig = "xah_run_rel21_DATA_2017.json"
  localPATH += "/source"

if not MC:
  if "15" in sampleType:
    command = "xAH_run.py --files "+dataSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+dataConfig+" --submitDir outputTreesData15 direct > Logs/log_Data15_WorkAreaII 2> Logs/err_Data15 &"
  if "16" in sampleType: 
    command = "xAH_run.py --files "+dataSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+dataConfig+" --submitDir outputTreesData16 direct > Logs/log_Data16_WorkAreaII 2> Logs/err_Data16 &"
  if "17" in sampleType:
    command = "xAH_run.py --files "+dataSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+dataConfig+" --submitDir outputTreesData17 direct > Logs/log_Data17_WorkAreaII 2> Logs/err_Data17 &"

if MC:
  if "15" in sampleType:
    command = "xAH_run.py --files "+mcSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+mcConfig+" --submitDir outputTreesMC15 direct " #> Logs/log_MC15_WorkAreaII 2> Logs/err_MC15 &"
  if "16" in sampleType:
    command = "xAH_run.py --files "+mcSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+mcConfig+" --submitDir outputTreesMC16 direct "#> Logs/log_MC16_WorkArea 2> Logs/err_MC16 &"	
  if "16" in sampleType and JETM1Kernel ==True:
    command = "xAH_run.py --files "+mcSample+" --config "+localPATH+"/xAODAnaHelpers/data/" + "xah_run_rel21_MC_JETM1Kernel_test_noWeights.json" + " --nevents=10000 " + " --submitDir JETM1_test_1005_noWeights_AllSys " + "direct " #+ " > Logs/JETM1_test_error 2> Logs/JETM1_test_log &"	


print "before apply os.system(command)"
print "\n\n" 
print command
os.system(command)


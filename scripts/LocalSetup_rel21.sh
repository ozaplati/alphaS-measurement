#! /bin/bash

######################################
######################################
# SETUP CURRENT PATH
PATH_SCRIPTS=$(pwd)



######################################
######################################
# SETUP PATH TO SOURCE OF XADOANAHELPER

PATH_COMPILE_SOURCE="/afs/cern.ch/work/o/ozaplati/public/yAnalysis/WorkAreaII/source"
printf "\n\ncd ${PATH_COMPILE_SOURCE}\n\n\n\n\n"
cd ${PATH_COMPILE_SOURCE}




setupATLAS
# SETUP THE ANALYSISBASE COMPATIBLE WITH XAODANAHELPER
asetup AnalysisBase,21.2.65,here


######################################
# COMPILATION
cd ../build
cmake ../source
make
cd ..
source build/${CMTCONFIG}/setup.sh

#####################################
# GO BACK TO CURRENT DIRECTORY
printf "\n\n\nGO BACK TO SCRIPT DIRECTORY:\n"
printf "cd ${PATH_SCRIPTS}\n\n"
cd ${PATH_SCRIPTS}

#!/usr/bin/python
######################################
#    Download nTuples using rucio    #
#          Ota 28.3.2019             #
######################################
import os, sys

##################
# Setup CERN user
user = "ozaplati"

##################
# Set up Key string of rucio conteiners
KEY = "12_06_2019_JETM1_OUTPUT_ALL_WEIGHTS_10000EVT"

##################
# Set up JZ slices
JZX = [
    "JZ0",
    "JZ1",
    #"JZ2",
    "JZ3",
    "JZ4",
    #"JZ5",
    #"JZ6",
    #"JZ7",
    #"JZ8",
    "JZ9",
    #"JZ10",
    #"JZ11",
    "JZ12",
]

########################
# Set up output path
outPath = "/eos/home-o/ozaplati/yAnalysis/12_06_2019_JETM1_OUTPUT_ALL_WEIGHTS_10000EVT/"

########################
# Current path
scrPath = os.getcwd()

##################################################################################
## DO NOT MODIFY
##################################################################################

os.system("mkdir -p " + outPath)

os.chdir(outPath)
for JZ in JZX:
    print("\n\n////////////////////////////\n////\n////")
    print("////     Working on " + JZ + " slice")
    print("////\n")
    
    
    
    # syntax example: rucio -v download user.ozaplati:user.ozaplati.26_03_2019_JETM1_allSlices.JZ11_tree.root
    command_base = "rucio -v download " + "user." + user + ":" + "user." + user + "." + "*" + KEY + "*" + "." + JZ
    command1 = command_base + "_tree.root"
    command2 = command_base + "_duplicates_tree.root"
    command3 = command_base + "_metadata.root"
    command4 = command_base + "_cutflow.root"
    command5 = command_base + ".log "

    print command1 + "\n\n"
    os.system(command1)
    print command2 + "\n\n"
    os.system(command2)
    print command3 + "\n\n"
    os.system(command3)
    print command4 + "\n\n"
    os.system(command4)
    print command5 + "\n\n"
    os.system(command5)

os.chdir(scrPath)

print("\n\nDONE!")



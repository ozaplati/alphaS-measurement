#!/usr/bin/python

import os, sys

param=sys.argv

##################################################################################
# Edit
##################################################################################

#sampleType = "MC15" # Options: MC15, MC16, data15, data16, data17
sampleType = "MC16" # Options: MC15, MC16, data15, data16, data17

#mcSample   = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/treeExamples/MC/mc15_13TeV.root"
#dataSample = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/treeExamples/Data/data16_13TeV.root"
#mcSample = "root://eosatlas//eos/atlas/user/o/ohaldik/user.ohaldik.01_10_2018_JETM9_Pythia_MC16d.JZALL/JZ_ALL_weighted/tree/JZ6_tree.root"
#mcSample = "mc16_13TeV:mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
#mcSample = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/DAOD_STSM11/mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_STDM11.e3569_s3126_r9364_p3749/DAOD_STDM11.16765767._000002.pool.root.1"
mcSample = "/eos/home-o/ozaplati/yAnalysis/DAOD/DAOD_STSM11/mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_STDM11.e3569_s3126_r9364_p3749/DAOD_STDM11.16765767._000002.pool.root.1"

#localPATH = "PATH"
localPATH = '/afs/cern.ch/work/o/ozaplati/public/yAnalysis/WorkAreaII/'

#rel21 = False # Meaningless for MC15
rel21 = True # Meaningless for MC15

##################################################################################
# DO NOT MODIFY
##################################################################################

Logs = "mkdir -p Logs"
print Logs
os.system(Logs)

# See if MC or data
MC = True
if "data" in sampleType:
  MC = False

# Protection
if sampleType == "MC16" or sampleType == "data17":
  rel21 = True
if sampleType == "MC15":
  rel21 = False

command = ""

# Choose appropiate JSON file
dataConfig = "xah_run_DATA.json"
mcConfig   = "xah_run_MC.json"
if rel21 and MC:
  mcConfig   = "xah_run_rel21_MC.json"	
  localPATH += "/source"
if rel21 and not MC:
  if "15" in sampleType:
    dataConfig = "xah_run_rel21_DATA_2015.json"
  if "16" in sampleType:
    dataConfig = "xah_run_rel21_DATA_2016.json"
  if "17" in sampleType:
    dataConfig = "xah_run_rel21_DATA_2017.json"
  localPATH += "/source"

if not MC:
  if "15" in sampleType:
    command = "xAH_run.py --files "+dataSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+dataConfig+" --submitDir outputTreesData15 direct > Logs/log_Data15 2> Logs/err_Data15 &"
  if "16" in sampleType:
    command = "xAH_run.py --files "+dataSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+dataConfig+" --submitDir outputTreesData16 direct > Logs/log_Data16 2> Logs/err_Data16 &"
  if "17" in sampleType:
    command = "xAH_run.py --files "+dataSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+dataConfig+" --submitDir outputTreesData17 direct > Logs/log_Data17 2> Logs/err_Data17 &"

if MC:
  if "15" in sampleType:
    command = "xAH_run.py --files "+mcSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+mcConfig+" --submitDir outputTreesMC15 direct > Logs/log_MC15 2> Logs/err_MC15 &"
  if "16" in sampleType:
    command = "xAH_run.py --files "+mcSample+" --config "+localPATH+"/xAODAnaHelpers/data/"+mcConfig+" --submitDir outputTreesMC16 direct > Logs/log_MC16 2> Logs/err_MC16 &"	


print "before apply os.system(command)"
print "\n\n" 
print command
os.system(command)


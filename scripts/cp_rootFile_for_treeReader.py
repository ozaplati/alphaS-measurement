#!/usr/bin/python
import os, sys
from argparse import ArgumentParser


##################################################
# INPUT PARAMETERS FROM BASH
parser.add_argument("-p", "--path",
                    help = "path to directory with merged JZx tree Directories (e.q. JZ0_tree, JZ1_tree, where trees are located in each directories JZ0_tree/JZ0_tree.root) ",
                    default = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/",
                    dest='path')
        
inputPanser = parser.parse_args()
            
pathToRucioDwn = inputPanser.path 
#"/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/"

##################################################
# PREPARE STRUCTURE OF DIRECTORIES FOR OUTPUT
command_mkDir1 = "mkdir -p " + pathToRucioDwn + "/treeExample"
command_mkDir2 = command_mkDir1 + "/MC"
command_mkDir3 = command_mkDir1 + "/Data"

print("\n\nmkdir\n\n")
os.system(command_mkDir1)
os.system(command_mkDir2)
os.system(command_mkDir3)


##################################################
# FOR LOOP OVER ALL CONSIDERED SLICE-DIRECTORIES
for folder in os.walk(pathToRucioDwn).next()[1]:
  # JZ0 .... JZ12
  for iJZ in range(0, 13):
    treeDir = "JZ" + str(iJZ) + "_tree"
    if treeDir != folder:
      continue
    else:
      inputFile = pathToRucioDwn + "/" + folder + "/" + folder + ".root"
      outputFile = pathToRucioDwn + "/" + "treeExample/MC/."
      command_cp = "cp " +  inputFile + " " + outputFile
      print(command_cp)
      # COPY THE TREE.ROOT FILES
      #os.system(command_cp)
      
print("DONE!")

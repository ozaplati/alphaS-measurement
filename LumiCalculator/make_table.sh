#!/bin/sh

help() {
    echo
    echo "Specify one argument: periods/runnumber"
    echo "Example: G_H_I"
}

error() {
    echo -e "\nERROR:"; echo "  $1";
}

[[ $# -lt 1 ]] && { help; return; exit; }

echo
echo "Step 1: Will compile"
echo

code="MakeTable.C"
exec=./make_table.exe
CMD="g++ `$ROOTSYS/bin/root-config --cflags --glibs` -o $exec $code"
echo $CMD

success=0
 # compile
$CMD && success=1

[[ $success = 0 ]] && { error "Compilation failed"; return; exit; }

$exec $1 $2

echo
echo "All done"
echo

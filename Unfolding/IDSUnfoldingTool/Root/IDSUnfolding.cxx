#include "IDSUnfoldingTool/IDSUnfolding.h"

#include <iostream>

#include "Rtypes.h"
#include "TCanvas.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TRandom3.h"
#include "TDatime.h"
#include "TMath.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TLatex.h"

//----- For Vlasta

TH1* GetIDSUnfoldedSpectrum(const TH1 *h_RecoMC, const TH1 *h_TruthMC, const TH2 *h_2DSmear, const TH1 *h_RecoData, Int_t iter)
{
   Int_t nbinsx = h_RecoData->GetNbinsX();
   Int_t nbinsy = h_RecoData->GetNbinsY();
   Int_t nbins  = nbinsx*nbinsy;

   // Sanity checks
   if (h_TruthMC->GetNbinsX() != nbinsx || h_TruthMC->GetNbinsY() != nbinsy ||
       h_RecoMC->GetNbinsX()  != nbinsx || h_RecoMC->GetNbinsY()  != nbinsy ||
       h_2DSmear->GetNbinsX() != nbins) {
      std::cout << "Bins of input histograms don't all match, exiting IDS unfolding and returning NULL." << std::endl;
      return NULL;
   }

   // Put inputs into vectors, and if necessary turn 2-D inputs into 1-D inputs
   TVectorD reco(nbins), truth(nbins);
   TVectorD data(nbins), dataerror(nbins);
   Int_t i = 0;
   for (Int_t by = 1; by <= nbinsy; ++by) { // loop over pt
      for (Int_t bx = 1; bx <= nbinsx; ++bx) { // loop over gap_size, for each pt value
         reco[i]  = h_RecoMC->GetBinContent(bx, by);
         truth[i] = h_TruthMC->GetBinContent(bx, by);
         data[i]  = h_RecoData->GetBinContent(bx, by);
         if (data[i] > 0.0) {
            dataerror[i] = h_RecoData->GetBinError(bx, by);
         } else {
            dataerror[i] = 1.0;
         }
         i++;
      }
   }

   // Make transfer matrix and project matched MC spectra
   TMatrixD migmatrix(nbins, nbins);
   TVectorD recomatch(nbins);
   TVectorD truthmatch(nbins);
   for (Int_t i = 0; i < nbins; ++i) {
      recomatch[i] = 0.0;
      truthmatch[i] = 0.0;
      for (Int_t j = 0; j < nbins; ++j) {
         recomatch[i]   += h_2DSmear->GetBinContent(i+1, j+1);
         truthmatch[i]  += h_2DSmear->GetBinContent(j+1, i+1);
         migmatrix[i][j] = h_2DSmear->GetBinContent(i+1, j+1);
      }
   }

   // Apply matching inefficiency from reco MC to data
   for (Int_t i = 0; i < nbins; ++i) {
      if (reco[i] > 0.0) {
         data[i] *= recomatch[i]/reco[i];
         dataerror[i] *= recomatch[i]/reco[i];
      } else {
         data[i] = 0.0;
         dataerror[i] = 0.0;
      }
   }

   // Matrix unfolding
   Double_t lambdaL = 0.;
   Double_t lambdaUmin = 0.5;
   Double_t lambdaMmin = 0.0;
   Double_t lambdaS = 0.;

   TVectorD result0(nbins), result(nbins);
   PerformIterations(data, dataerror, migmatrix, nbins,
                     lambdaL, iter, lambdaUmin, lambdaMmin, lambdaS,
                     &result0, &result);


   // Apply matching efficiency from truth MC to unfolded matched data
   for (Int_t i = 0; i < nbins; ++i) {
      if (truthmatch[i] > 0.0) {
         result[i] *= truth[i]/truthmatch[i];
      } else {
         result[i] = 0.0;
      }
   }
   
   // Make 1-D or 2-D histogram
   TH1 *h_DataUnfolded = (TH1*)h_RecoData->Clone("unfolded");
   h_DataUnfolded->SetTitle("unfolded");
   h_DataUnfolded->Reset();

   i = 0;
   for (Int_t by = 1; by <= nbinsy; ++by) {
      for (Int_t bx = 1; bx <= nbinsx; ++bx) {
         h_DataUnfolded->SetBinContent(bx, by, result[i++]);
      }
   }

   // Return result
   return h_DataUnfolded;
}

//----- Helper functions

void formatTex(TLatex *tex)
{
  tex->SetNDC();
  tex->SetTextFont(42);
  tex->SetLineWidth(2);
  tex->SetTextSize(0.05);
}


void rescaleAxis(TH1D *h, Double_t scale)
{
  Int_t nBins = h->GetXaxis()->GetNbins();
  Double_t *bins = new Double_t[nBins+1];
  for (int i=1; i<=nBins; i++) {
    bins[i-1] = h->GetXaxis()->GetBinLowEdge(i) * scale;
  }
  bins[nBins] = (h->GetXaxis()->GetBinLowEdge(nBins) + h->GetXaxis()->GetBinWidth(nBins)) * scale;

  h->GetXaxis()->Set(nBins, bins);
  h->Scale(1./scale);

  delete [] bins;
}

void divideBinWidth1D(TH1D*h)
{
  // Multiply by the bin width (to report number of events)
  Double_t bin = 0;
  Double_t content = 0;
  Double_t error = 0;
  Double_t width = 0;

  for (Int_t xbin=1; xbin<=h->GetNbinsX(); xbin++) {
      bin     = h->GetBin(xbin);
      content = h->GetBinContent(bin);
      error   = h->GetBinError(bin);
      width   = h->GetBinWidth(bin);

      h->SetBinContent(bin, content/width);
      h->SetBinError(bin, error/width);
  }
}


void divideBinWidth2D(TH2D*h)
{
  // Multiply by the bin width (to report number of events)
  Double_t bin = 0;
  Double_t content = 0;
  Double_t error = 0;
  Double_t width = 0;

  for (Int_t xbin=1; xbin<=h->GetNbinsX(); xbin++) {
    for (Int_t ybin=1; ybin<=h->GetNbinsY(); ybin++) {
      bin     = h->GetBin(xbin, ybin);
      content = h->GetBinContent(bin);
      error   = h->GetBinError(bin);
      width   = h->GetBinWidth(bin);

      h->SetBinContent(bin, content/width);
      h->SetBinError(bin, error/width);
    }
  }
}


void divideBinWidth3D(TH3D*h)
{
  // Multiply by the bin width (to report number of events)
  Double_t bin = 0;
  Double_t content = 0;
  Double_t error = 0;
  Double_t width = 0;

  for (Int_t xbin=1; xbin<=h->GetNbinsX(); xbin++) {
    for (Int_t ybin=1; ybin<=h->GetNbinsY(); ybin++) {
      for (Int_t zbin=1; zbin<=h->GetNbinsZ(); zbin++) {
        bin     = h->GetBin(xbin, ybin, zbin);
        content = h->GetBinContent(bin);
        error   = h->GetBinError(bin);
        width   = h->GetBinWidth(bin);

        h->SetBinContent(bin, content/width);
        h->SetBinError(bin, error/width);
      }
    }
  }
}

void multiplyBinWidth1D(TH1D*h)
{
  // Multiply by the bin width (to report number of events)
  Double_t bin = 0;
  Double_t content = 0;
  Double_t error = 0;

  Double_t width = 0;
  Double_t widthx = 0;

  for (Int_t xbin=0; xbin<=h->GetNbinsX()+1; xbin++) {
      bin     = h->GetBin(xbin);
      content = h->GetBinContent(bin);
      error   = h->GetBinError(bin);
      
      widthx   = h->GetXaxis()->GetBinWidth(xbin);
      width = widthx;

      h->SetBinContent(bin, content*width);
      h->SetBinError(bin, error*width);
  }
}

void multiplyBinWidth2D(TH2D*h)
{
  // Multiply by the bin width (to report number of events)
  Double_t bin = 0;
  Double_t content = 0;
  Double_t error = 0;

  Double_t width = 0;
  Double_t widthx = 0;
  Double_t widthy = 0;

  for (Int_t xbin=0; xbin<=h->GetNbinsX()+1; xbin++) {
    for (Int_t ybin=0; ybin<=h->GetNbinsY()+1; ybin++) {
      bin = h->GetBin(xbin, ybin);
      content = h->GetBinContent(bin);
      error   = h->GetBinError(bin);
        
      widthx   = h->GetXaxis()->GetBinWidth(xbin);
      widthy   = h->GetYaxis()->GetBinWidth(ybin);
      width = widthx*widthy;
        
      h->SetBinContent(bin, content*width);
      h->SetBinError(bin, error*width);
    }
  }
}


void multiplyBinWidth3D(TH3D*h)
{
  // Multiply by the bin width (to report number of events)
  Double_t bin = 0;
  Double_t content = 0;
  Double_t error = 0;

  Double_t width = 0;
  Double_t widthx = 0;
  Double_t widthy = 0;
  Double_t widthz = 0;

  for (Int_t xbin=0; xbin<=h->GetNbinsX()+1; xbin++) {
    for (Int_t ybin=0; ybin<=h->GetNbinsY()+1; ybin++) {
      for (Int_t zbin=0; zbin<=h->GetNbinsZ()+1; zbin++) {
        bin     = h->GetBin(xbin, ybin, zbin);
        content = h->GetBinContent(bin);
        error   = h->GetBinError(bin);
        
        widthx   = h->GetXaxis()->GetBinWidth(xbin);
        widthy   = h->GetYaxis()->GetBinWidth(ybin);
        widthz   = h->GetZaxis()->GetBinWidth(zbin);
        width = widthx*widthy*widthz;
        
        h->SetBinContent(bin, content*width);
        h->SetBinError(bin, error*width);
      }
    }
  }
}

//-----   IDS code

Double_t Probability( Double_t deviation, Double_t sigma, Double_t lambda )
{
   if( lambda < 0.00001 ){ return 1.;}
   if( lambda > 1000. ){ return 0.;}
   return 1-exp(-pow(deviation/(sigma*lambda),2) );
}

Double_t MCnormalizationCoeff(const TVectorD *vd, const TVectorD *errvd, const TVectorD *vRecmc, const Int_t dim, const Double_t estNknownd, const Double_t Nmc, const Double_t lambda, const TVectorD *soustr_ )
{
   Double_t Nknownd = estNknownd;
   for(Int_t i=0; i<dim; i++){
      if( ((*vd)[i] - (*soustr_)[i] >= 0.) && ((*vRecmc)[i]!=0) ){
         // The first test shall also be done when computing Nmc & estNknownd
         Double_t ef = Probability( fabs((*vd)[i]- (*soustr_)[i] - estNknownd/Nmc*(*vRecmc)[i]), sqrt( pow((*errvd)[i],2) /* + pow(estNknownd/Nmc,2)*fabs((*vRecmc)[i]) */ ),lambda );
         Nknownd += (1-ef)*( (*vd)[i] - (*soustr_)[i] - estNknownd/Nmc*(*vRecmc)[i] );
      }
   }

   return Nknownd;
}

Double_t MCnormalizationCoeffIter(const TVectorD *vd, const TVectorD *errvd, const TVectorD *vRecmc, const Int_t dim, const Double_t estNknownd, const Double_t Nmc, const TVectorD *soustr_, Double_t lambdaN, Int_t NiterMax, Int_t messAct)
{
   Double_t Nkd = 0., estNknownd_ = estNknownd;
   for(Int_t i=0; i<NiterMax; i++ ){
      Nkd = MCnormalizationCoeff( vd, errvd, vRecmc, dim, estNknownd_, Nmc, lambdaN, soustr_ );
      if( fabs((estNknownd_ -  Nkd)/Nkd) < 1e-6 ){ break; }
      if( (i >= NiterMax-1) && (messAct != 0)  ){ std::cout << "WARNING: a number of " << i+1 << " steps were performed for the normalization " << (estNknownd_ -  Nkd)/Nkd << "  " << (estNknownd -  Nkd)/Nkd << std::endl;}

      estNknownd_ = Nkd;

   }
   return Nkd;
}

void Unfold( const TVectorD &b, const TVectorD &errb, const TMatrixD &A, const Int_t dim, const Double_t lambda, TVectorD *soustr_, TVectorD *unf)
{
   // compute the mc true and reco spectra and normalize them
   TVectorD reco_mcN(dim), true_mcN(dim);
   Double_t estNkd = 0., Nkd = 0. , Nmc = 0.;
   for(Int_t i=0; i<dim; i++ ){
      reco_mcN[i] = 0.;
      true_mcN[i] = 0.;
      for(Int_t j=0; j<dim; j++ ){
         reco_mcN[i] += A[i][j];
         true_mcN[i] += A[j][i];
      }
      if(b[i] - (*soustr_)[i] >= 0.){
         Nmc += reco_mcN[i];
         estNkd += b[i] - (*soustr_)[i];
      }
   }

   // # known data
   Nkd = MCnormalizationCoeffIter(&b, &errb, &reco_mcN, dim, estNkd, Nmc, soustr_ );

   for(Int_t i=0; i<dim; i++ ){
      reco_mcN[i] *= Nkd/Nmc;
      true_mcN[i] *= Nkd/Nmc;
      (*unf)[i] = true_mcN[i] + (*soustr_)[i];
   }

   // compute the prob(j|i) matrix
   TMatrixD prob(dim, dim);
   for(Int_t i=0; i<dim; i++){
      Double_t si = 0.;
      for(Int_t j=0; j<dim; j++){
         si += A[i][j];
      }
      for(Int_t j=0; j<dim; j++){
         if( si!=0. )
            prob[i][j] = A[i][j]/si;
         else
            prob[i][j] = 0.;
      }

      if (reco_mcN[i] != 0.0 && (b[i] - (*soustr_)[i]) > 0.0 /*&&((*b)[i]>0.)*/) {
         Double_t ef = Probability( fabs(b[i]-(*soustr_)[i]-reco_mcN[i]), sqrt( pow(errb[i],2) /* + Nkd/Nmc*fabs((*reco_mcN)[i]) */ ), lambda );
         for(Int_t j=0; j<dim; j++){
            (*unf)[j] += ef * prob[i][j]*(b[i]-(*soustr_)[i]-reco_mcN[i]);
         }
         (*unf)[i] += (1-ef) * (b[i]-(*soustr_)[i]-reco_mcN[i]);
      } else {
         (*unf)[i] += b[i]-(*soustr_)[i]-reco_mcN[i];
      }
   }

}

void ComputeSoustrTrue( const TMatrixD *A, const TVectorD *unfres, const TVectorD *unfresErr, Int_t N, TVectorD *soustr_, Double_t lambdaS )
{

   TVectorD *true_mcT = new TVectorD(N), *active = new TVectorD(N);
   Double_t estNkd = 0., Nmc=0., NkUR=0.;
   for(Int_t j=0; j<N; j++){

      (*active)[j] = 1.;

      (*true_mcT)[j] = 0.;
      for(Int_t i=0; i<N; i++){
         (*true_mcT)[j] += ((*A)[i][j]);
      }
      if((*unfres)[j] - (*soustr_)[j] >= 0.){
         Nmc += (*true_mcT)[j];
         estNkd += (*unfres)[j] - (*soustr_)[j];
      }

      if( ((*true_mcT)[j]) < 0.){
         std::cout << "found problematic (*true_mcT)[j] " << (*true_mcT)[j] << " " << j << " " << (*unfres)[j] << std::endl;
         exit(1);
      }
   }

   for(Int_t k=0; k<2; k++){
      NkUR = MCnormalizationCoeffIter( unfres, unfresErr, true_mcT, N, estNkd, Nmc, soustr_ );

      for(Int_t j=0; j<N; j++){
         if( ((*unfres)[j] - (*soustr_)[j]>0.) && ((*true_mcT)[j]!=0.) && ((*active)[j]>0.) ){
            Double_t ef = Probability(fabs((*unfres)[j] /*- (*soustr_)[j]*/ -NkUR/Nmc*(*true_mcT)[j]), sqrt(pow((*unfresErr)[j],2) /* +pow(NkUR/Nmc,2)*fabs((*true_mcT)[j]) */ ), lambdaS);
            ((*soustr_)[j]) /*+*/= (1-ef) * ( (*unfres)[j] /*- (*soustr_)[j]*/ - (*true_mcT)[j]/(Nmc/NkUR));
         }
         else{
            ((*soustr_)[j]) /*+*/= (*unfres)[j] /*- (*soustr_)[j]*/ - (*true_mcT)[j]/(Nmc/NkUR);
            (*active)[j] = -1.;
         }
      }
      estNkd = NkUR;
   }

   delete true_mcT; delete active;
}

void ModifyMatrix( TMatrixD *Am, const TMatrixD *A, const TVectorD *unfres, const TVectorD *unfresErr, Int_t N, const Double_t lambdaM_, TVectorD *soustr_, const Double_t lambdaS_ )
{
   ComputeSoustrTrue( A, unfres, unfresErr, N, soustr_, lambdaS_ );

   TVectorD *true_mcT = new TVectorD(N);
   Double_t estNkd = 0., Nmc=0., NkUR=0.;
   for(Int_t j=0; j<N; j++){
      (*true_mcT)[j] = 0.;
      for(Int_t i=0; i<N; i++){
         (*true_mcT)[j] += ((*A)[i][j]);
         ((*Am)[i][j]) = ((*A)[i][j]);
      }
      if((*unfres)[j] - (*soustr_)[j] >= 0.){
         Nmc += (*true_mcT)[j];
         estNkd += (*unfres)[j] - (*soustr_)[j];
      }

      if( ((*true_mcT)[j]) < 0.){
         std::cout << "found problematic (*true_mcT)[j] " << (*true_mcT)[j] << " " << j << " " << (*unfres)[j] << std::endl;
         exit(1);
      }
   }

   NkUR = MCnormalizationCoeffIter( unfres, unfresErr, true_mcT, N, estNkd, Nmc, soustr_ );

   for(Int_t j=0; j<N; j++){
      if( (*unfres)[j] - (*soustr_)[j]>0. && (*true_mcT)[j]!=0. ){
         Double_t ef = Probability(fabs((*unfres)[j] - (*soustr_)[j] -NkUR/Nmc*(*true_mcT)[j]), sqrt(pow((*unfresErr)[j],2) /* +pow(NkUR/Nmc,2)*fabs((*true_mcT)[j]) */ ), lambdaM_);
         for(Int_t i=0; i<N; i++){
            ((*Am)[i][j]) += ef * ( ((*unfres)[j] - (*soustr_)[j])*(Nmc/NkUR) - (*true_mcT)[j]) * ((*A)[i][j])/((*true_mcT)[j]);
         }
      }
   }

   delete true_mcT;
}

void PerformIterations(const TVectorD &data, const TVectorD &dataErr, const TMatrixD &A_, const Int_t &N_, const Double_t lambdaL_, const Int_t NstepsOptMin_, const Double_t lambdaU_, const Double_t lambdaM_, const Double_t lambdaS_, TVectorD* unfres1IDS_, TVectorD* unfres2IDS_)
{
   TVectorD soustr(N_);
   for (Int_t i = 0; i < N_; i++) soustr[i] = 0.;

   Unfold(data, dataErr, A_, N_, lambdaL_, &soustr, unfres1IDS_); // 1 step
   
   for (Int_t i = 0; i < N_; i++) (*unfres2IDS_)[i] = (*unfres1IDS_)[i];

   TMatrixD Am_(N_, N_);
   for (Int_t k = 0; k < NstepsOptMin_; k++) {
      ModifyMatrix(&Am_, &A_, unfres2IDS_, &dataErr, N_, lambdaM_, &soustr, lambdaS_);

      // UNFOLDING
      Unfold(data, dataErr, Am_, N_, lambdaU_, &soustr, unfres2IDS_); // full iterations
   }
}

TMatrixD* GetSqrtMatrix( const TMatrixD& covMat )
{
   // production of square-root matrix (required for correlated random number generation)
   Double_t sum = 0;
   const Int_t size = covMat.GetNrows();;
   TMatrixD* sqrtMat = new TMatrixD( size, size );

   for (Int_t i=0; i<size; i++) {

      sum = 0;
      for (Int_t j=0;j< i; j++) sum += (*sqrtMat)(i,j) * (*sqrtMat)(i,j);

      (*sqrtMat)(i,i) = TMath::Sqrt(TMath::Abs(covMat(i,i) - sum));
      if ((*sqrtMat)(i,i) <= 0) {
         std::cout << "<GetSqrtMatrix> Covariance matrix has incomplete rang" << std::endl;
         exit(1);
      }

      for (Int_t k=i+1 ;k<size; k++) {

         sum = 0;
         for (Int_t l=0; l<i; l++) sum += (*sqrtMat)(k,l) * (*sqrtMat)(i,l);

         (*sqrtMat)(k,i) = (covMat(k,i) - sum) / (*sqrtMat)(i,i);

      }
   }
   return sqrtMat;
}

void GenGaussRnd( TArrayD& v, const TMatrixD& sqrtMat, TRandom3& R )
{
   // generate vector of correlated Gaussian-distributed random numbers
   // sanity check
   const Int_t size = sqrtMat.GetNrows();
   if (size != v.GetSize()) {
      std::cout << "<GenGaussRnd1> Too short input vector: " << size << " " << v.GetSize() << std::endl;
      exit(1);
   }

   Double_t* tmpVec = new Double_t[size];

   for (Int_t i=0; i<size; i++) {
      Double_t x, y, z;
      y = R.Rndm();
      z = R.Rndm();
      x = 2*TMath::Pi()*z;
      tmpVec[i] = TMath::Sin(x) * TMath::Sqrt(-2.0*TMath::Log(y));
   }

   for (Int_t i=0; i<size; i++) {
      v[i] = 0;
      for (Int_t j=0; j<=i; j++) v[i] += sqrtMat(i,j) * tmpVec[j];
   }

   delete [] tmpVec;
}

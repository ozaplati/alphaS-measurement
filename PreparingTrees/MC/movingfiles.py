#!/usr/bin/python

import os, sys

param=sys.argv

list = [
    "JZ0",
    "JZ1",
    "JZ2",
    "JZ3",
    "JZ4",
    "JZ5",
    "JZ6",
    "JZ7",
    #"JZ8",
    "JZ9",
    "JZ10",
    "JZ11",
    "JZ12",
]
# setup the absolut path of rucio download xADs:
pathInput = "/eos/home-o/ozaplati/yAnalysis/JETM1_vectorWeights/" 

# setup the absolut path of SM-IJXS-Dijets directory:
pathSM = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/"

# setup user:
user = "ozaplati"

##################################################################################
## DO NOT MODIFY
##################################################################################


print "Moving all JZX outputs"
for i in list:

    #precommand1 = "mkdir " + pathInput + "/" + i + "_tree"
    command1 = "mv " + pathInput + "/" + "user."+user+".*" + i + "_tree.root   " + pathInput + "/" + i + "_tree"
    #precommand2 = "mkdir " + pathInput + "/" + i + "_cutflow"
    command2 = "mv " + pathInput + "/" + "user."+user+".*" + i + "_cutflow.root   " + pathInput + "/" + i + "_cutflow"
    #precommand3 = "mkdir " + pathInput + "/" + i + "_metadata"
    command3 = "mv " + pathInput + "/" + "user." + user+".*" + i + "_metadata.root   " + pathInput + "/" + i + "_metadata"
    #precommand4 = "mkdir " + pathInput + "/" + i + "_duplicates_tree"
    command4 = "mv " + pathInput + "/" + "user."+user+".*" + i + "_duplicates_tree.root   " + pathInput + "/" + i + "_duplicates_tree"
    
    #print precommand1
    print command1 + "\n"
    #print precommand2
    print command2 + "\n"
    #print precommand3
    print command3 + "\n"
    #print precommand4
    print command4 + "\n"
    
    print (" \n ")
    #os.system(precommand1)
    os.system(command1)
    #os.system(precommand2)
    os.system(command2)
    #os.system(precommand3)
    os.system(command3)
    #os.system(precommand4)
    os.system(command4)

print("\nDONE\n")


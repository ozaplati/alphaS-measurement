#!/usr/bin/python

# This python merge into one sample all the JZ samples with the correct weights

import os, sys

param=sys.argv

list = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    #"8",
    "9",
    "10",
    "11",
    "12",
]

pathInput = "/eos/home-o/ozaplati/yAnalysis/JETM1_vectorWeights/"

##################################################################################
## DO NOT MODIFY
##################################################################################

print "Merging all JZX outputs"

command = ""

for i in list:
    print("\n\n////////////////////////////\n////\n////")
    print("////     Working on JZ" + i + " slice")
    print("////\n")
    
    # syntax:
    # hadd PATH/merge.root PATH/*
    command += "hadd " + pathInput + "/" + "JZ" + i + "_tree/JZ" + i + "_tree.root  " + pathInput + "/" + "JZ" + i + "_tree/* && " + "\\ \n"
    command += "hadd " + pathInput + "/" + "JZ" + i + "_cutflow/JZ" + i + "_cutflow.root " + pathInput + "/" + "JZ" + i + "_cutflow/* && " + "\\ \n"
    command += "hadd " + pathInput + "/" + "JZ" + i + "_metadata/JZ" + i + "_metadata.root  " + pathInput + "/" + "JZ" + i + "_metadata/* && " + "\\ \n"
    command += "hadd " + pathInput + "/" + "JZ" + i + "_duplicates_tree/JZ" + i + "_duplicates_tree.root  " + pathInput + "/" + "JZ" + i + "_duplicates_tree/* && "

command = command[:-3]
command += " &"
print command + "\n\n"
os.system(command)



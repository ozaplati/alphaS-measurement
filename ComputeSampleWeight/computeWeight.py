from ROOT import *
import os,sys
from array import *

######################################################################
## Edit
######################################################################

pyAMIPATH_SAMPLE = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/pyAMI/Outputs/JETM1_allSlices.txt"
Sample = "JETM1_allSlices"

PATH = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/"  # Path to trees

JZXsamples = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  #"8",
  "9",
  "10",
  "11",
  "12",
]

TDirectoryFileName = "treeAlgo"

TTreeName = "nominal"

######################################################################
# DO NOT MODIFY
######################################################################

# Get AMI values
file  = open(pyAMIPATH_SAMPLE, "r")
AMIvalues  = file.readlines()

nSlices = len(AMIvalues)
print(nSlices)

# Save info in arrays
channelNumbers = []
xss            = []
effs           = []
for i in range(0,nSlices):
  channelNumbers.append(AMIvalues[i].split(" ")[0])
  xss.append(AMIvalues[i].split(" ")[1])
  effs.append(AMIvalues[i].split(" ")[2].replace("\n",""))

# Get list of folders
Folders = []
for folder in os.walk(PATH).next()[1]:
  Folders.append(folder)
  #print(folder)
  
# Loop over JZX samples
for JZX in JZXsamples:
  print "Computing sample weight for JZ"+JZX+":"
  # Find metadata and tree
  metadataPATH = PATH+"/"
  treePATH     = PATH+"/"
  for folder in Folders:
    #    if "JZ"+str(JZX)+"_metadata" in folder:
    if "JZ"+str(JZX)+"_metadata" == folder:
      metadataPATH += folder+"/"
      print("\n\nMETADATA: " + metadataPATH)

    #    if "JZ"+str(JZX)+"_tree" in folder:
    elif "JZ"+str(JZX)+"_tree" == folder:
      treePATH += folder+"/"
      print("\n\ntreePATH: " + treePATH)
    else:
      continue


  
    # Open TFile with Metadata
  print "Openning "+metadataPATH+"JZ"+JZX+"_metadata.root"
  metadatafile = TFile.Open(metadataPATH+"JZ"+JZX+"_metadata.root","READ")
  # Get Total Number of events or Sum Of Weights (as appropriate)
  cutflowHistName = "MetaData_EventCount";
  cutflowHist     = metadatafile.Get(cutflowHistName)
  if not cutflowHist:
    print "Metadata Histogram not found, exiting"	  
    sys.exit(0)
  totalEvents     = cutflowHist.GetBinContent(1)
  sumWeights      = cutflowHist.GetBinContent(3)
  #print "TotalEvents: "+str(totalEvents)
  #print "SumWeights: "+str(sumWeights)
  metadatafile.Close()

  Denominator = totalEvents
  if "Powheg" in Sample:
    Denominator = sumWeights

  # Loop over files
  for filetree in os.walk(treePATH).next()[2]:
    #ota added I want only JZx_tree.root file
    if filetree != "JZ"+str(JZX)+"_tree.root":
      print("LOOP OVER FILES: CONTINUES   " + filetree)
      continue
    print("FINALY JZX_tree.root")
    # Open TFile with TTree
    print "Openning "+treePATH+filetree
    
  
    treefile = TFile.Open(treePATH+filetree,"UPDATE")
    if not treefile:
      print "File("+treePATH+filetree+") not found, exiting"
      sys.exit(0)	      
    # Get TTree
    tree = TTree()
    if TDirectoryFileName != "":
      TDir = treefile.GetDirectory(TDirectoryFileName)	    
      tree = TDir.Get(TTreeName)
    else:
      tree = treefile.Get(TTreeName)	    
    w = array( 'd', [ 0 ] )
    newBranch = tree.Branch( 'weight', w, 'weight/D' )
    # Get mcChannelNumber of the file
    mcChannelNumber = 0
    for event in tree:
      mcChannelNumber = tree.mcChannelNumber
      break
    count = 0
    for event in tree:
      # Show status
      if count % 1000000 == 0:
        print str(count)+" of "+str(tree.GetEntries())+" events processed"
      # Read mcEventWeight
      mcEventWeight   = tree.mcEventWeight
      # Calculate sample weight
      xs  = 1
      eff = 1
      iChannel = 0
      for channel in channelNumbers:
        if int(channel) == mcChannelNumber:
          xs  = float(xss[iChannel])
	  eff = float(effs[iChannel])
	iChannel += 1
      if xs == 1:
        print "mcChannelNumber("+str(mcChannelNumber)+") not recognised, exiting"
        sys.exit(0)
      w[0] = xs*eff*mcEventWeight/Denominator
      # Fill tree with sample weight
      newBranch.Fill()
      count += 1
    # Write TTree
    if TDirectoryFileName != "":
      treefile.cd(TDirectoryFileName)
    tree.Write("",TObject.kOverwrite)
    treefile.Close()

print ">>> DONE <<<"
  


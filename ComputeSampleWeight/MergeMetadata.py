from ROOT import *
import os,sys
import glob

######################################################################
## Edit
######################################################################

PATH = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/RunArea/New3_STDM11Kernel1/"  # Path to trees

JZXsamples = [
  #"0",
  #"1",
  "2",
  #"3",
  #"4",
  #"5",
  #"6",
  #"7",
  #"8",
  #"9",
  #"10",
  #"11",
  #"12",
]

######################################################################
# DO NOT MODIFY
######################################################################

command = ""

# Get list of folders
Folders = []
for folder in os.walk(PATH).next()[1]:
  Folders.append(folder)

# Loop over JZX samples
for JZX in JZXsamples:
  # Find metadata
  metadataPATH = PATH+"/"
  for folder in Folders:
    print ("metadata Path: " + metadataPATH)
    # Ota change 
    # in origin version :     if "JZ"+str(JZX)+"_metadata" in folder:
    if "JZ"+str(JZX)+"_metadata" == folder:
      metadataPATH += folder+"/"
      print ("IF .... metadata Path: " + metadataPATH)


  # Loop over 
  command += "hadd "
  command += metadataPATH
  command += "JZ"
  command += JZX
  command += "_metadata.root "
  for metadata in os.walk(metadataPATH).next()[2]:
    command += metadataPATH+metadata+" "
  command += " > log_JZ"
  command += JZX
  command += " 2> err_JZ"
  command += JZX
  command += " && "
command = command[:-2]
print command
os.system(command)


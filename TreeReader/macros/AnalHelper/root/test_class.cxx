#include "../AnalHelper/HIST.h"
#include "HIST.cxx"

#include "../AnalHelper/VEC_HIST.h"
#include "VEC_HIST.cxx"

#include "../AnalHelper/TH1D_JZX.h"
#include "TH1D_JZX.cxx"

#include "../AnalHelper/VecTH1D_JZX.h"
#include "VecTH1D_JZX.cxx"

#include "../AnalHelper/MultiCanvas.h"
#include "MultiCanvas.cxx"

#include "../AnalHelper/AnalException.h"

using namespace std;


void coutConfigEnv(int iJZ){
	TString strIJZ =  std::to_string(iJZ); //strIJZ; //TString::Itoa(iJZ); //std::to_string(iJZ);
	cout << "\n\n" << endl;
	cout << "JZ" << strIJZ << "_color: " 		<< strIJZ << endl;
	cout << "JZ" << strIJZ << "_marker: " 		<< strIJZ << endl;
	cout << "JZ" << strIJZ << "_linestyle: " 	<< strIJZ << endl; 
}


void prepareConfigEnvFile(){
	for (int i =0 ; i<13;  i++) coutConfigEnv(i);
}

TString getStringFromEnv(TString var, TEnv * config, const TString subStr)
{
	stringstream stream(config->GetValue(subStr, ""));
	stream >> var;
	return(var);
}
//////////////////////////////////////////////
//////////////////////////////////////////////
//////////////////////////////////////////////
void PrepareToPlotTest(HIST * h)
{
	//////////////////////////////////////////
	// READ SETUP FROM ENV FILE
	const TString _config = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/TreeReader/config/JZX_compare.env";
	TEnv *config = new TEnv(_config);

	int color, markerstyle, linestyle;
	TString strColor, strMarkerStyle, strLineStyle;

	//////////////////////////////////////////
	// stringstream to conver string to int
	TString JZX_name = h->Get_key();

	stringstream geek_c(config->GetValue(JZX_name + "_color", ""));
	geek_c >> color;

    stringstream geek_m(config->GetValue(JZX_name + "_marker", ""));
    geek_m >> markerstyle;
  
    stringstream geek_l(config->GetValue(JZX_name + "_linestyle", ""));
    geek_l >> linestyle;
  
  
	//cout << "JZX_name: " << JZX_name << endl;
	//cout << "color: " << color << endl;
	//cout << "strMarkerStyle: " << markerstyle << endl;
	//cout << "strLineStyle: " << linestyle << endl;
	//cout << "/////////////" << endl;
	 
    //////////////////////////////////////////
    // APPLY THE SETUP
    //h->UseCurrentStyle(); // important for imported histograms
    h->SetLineColor(color);
    h->SetMarkerColor(color);
    h->SetMarkerSize(1.2);
    h->SetMarkerStyle(markerstyle);
    h->SetLineStyle(linestyle);
    
    //else if(nameSmaple.Contains("301322")) {fillStyle=3000; lineColor=kMagenta+3; fillColor=kMagenta+2;}
	//h->SetFillColor(kGreen+2);
	//->SetLineColor(kGreen+3);
	h->SetLineWidth(2); 
	h->SetLineStyle(1);
	h->SetFillStyle(3000);
	h->GetYaxis()->SetTitle("User Y axis");
	h->GetXaxis()->SetTitle("User X axis");
}

int main(int nArg, char **arg) {
	try{
		if (nArg!=5){
		cout << "Error: Wrong number of input parameters!" << endl;
		cout << nArg << " were used" << endl;
		throw AnalException("Error: Wrong number of input parameters!"); 
		}

         ////////////////////////////
         // set input arguments
         //
         TString path = arg[1];
         TString histName =  arg[2];
         TString pathInTFile = ""; 			//arg[3];
         TString OUTPUT_PATH = arg[3];
         TString config = arg[4]; 			//"/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/TreeReader/config/JZX_compare.env";
        
         
         //vector<TH1D_JZX*> vTH1D_JZX;
         vector<HIST*> vTH1D_JZX;

         vector<double_t> vYmax;
         for (int iJZ=0; iJZ < 13; iJZ++){
			 //JZ8 is missing
			 //if (!(iJZ == 0 || iJZ == 11 || iJZ == 10 || iJZ == 12 )) continue ;
			if(iJZ == 8) continue;

			cout << "working with " << iJZ << " slice" << endl;
			 TString JZX_name = "JZ" + std::to_string(iJZ);

             TFile *tree = new TFile(path + "/" + "JZ" + std::to_string(iJZ) + "_tree.root", "READ");
             TH1D *hist1 = (TH1D*) tree->Get(pathInTFile + histName);

             //TH1D_JZX *hist = new TH1D_JZX(hist1, iJZ, "nTuple_name", path, pathInTFile);
             //vTH1D_JZX.push_back(hist);
             //cout << "push" << endl;
             
             TString key = "key";
             key += std::to_string(iJZ);
             HIST *hist = new HIST(hist1, iJZ, key, "leg descrivtion", "hist describtion", "data describtion", false, false);
             vTH1D_JZX.push_back(hist);
             cout << "push" << endl;
             hist->Show();
             hist->CheckHist();
             
             
             //TH1D_JZX * hNominal = new (TH1D_JZX) hist->Clone();

             
         }
         
         bool bRatio = true;
         //TH1D_JZX * hNominal = (TH1D_JZX*) vTH1D_JZX.at(6)->Clone();
         //TH1D_JZX * hNominal = (TH1D_JZX*) vTH1D_JZX.at(0)->Clone();
         HIST * hNominal = (HIST*) vTH1D_JZX.at(0)->Clone();

         cout << "Inicialization: " << endl;
         //VecTH1D_JZX *vec = new VecTH1D_JZX(vTH1D_JZX, "nTuple_name", "", bRatio);
         
         VEC_HIST *vec = new VEC_HIST(vTH1D_JZX, "some description", bRatio);

         cout << "NOMINAL" << hNominal->GetMaximum() << endl;
         vec->Set_histNominal(hNominal);
         cout << "User Graphic: " << endl;
         cout << "SHOW VECTOR:" << endl;
         vec->ShowVector(vTH1D_JZX);
         
         vec->Set_UserGraphic(PrepareToPlotTest);
         vec->Plot(OUTPUT_PATH);
         
         
		//////////////////////////////////////////
		//		TEST OF MULTI-CANVAS CLASS		//
		//////////////////////////////////////////
		
		vector<TCanvas *> vec_Canvas;
		int n = 3;
		for (int i=0; i < n*n; i++)  vec_Canvas.push_back(vec->Get_canvas());
		
		TString outPut = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/TreeReader/plots/pdf/JZX_compare/myClassMutiCanvas.pdf";
		MultiCanvas myMultiCanvas(vec_Canvas, n,n, outPut);
		//myMultiCanvas.Show();
		cout << "THE END" << endl;
		
	}
	catch(AnalException l){
		l.printMessage();
		cout << "Exiting now!" << endl;
		return (-1);
	}
}

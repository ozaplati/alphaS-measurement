#include "../AnalHelper/VEC_HIST.h"

using namespace std;

/////////////////////////////////////////
//*************************************//
//         VEC_HIST CLASS           //
//*************************************//
/////////////////////////////////////////


/////////////////////////////////
//    Inicialization modules   //
/////////////////////////////////

// Inicialization 
// using ratio requirement
void VEC_HIST::Inicialize(const TString _describtion, bool _ratio, HIST * _hNominal)
{
	cout << "Set up" << endl;
	this->Set_describtion(_describtion);
	this->Set_ratio(_ratio);
	cout << "set up nominal" << endl;
	this->Set_histNominal(_hNominal);
	this->Set_vectorRatios(this->Get_vector(), this->Get_histNominal());
	
	
	cout << "Preparing" << endl;
	this->PrepareCanvas();
	this->PrepareCanvasLog();
	this->PrepareCanvasLogLog();

	this->PreparePad();
	this->PrepareLeg();
}

// Implicit inicialization 
// no ratio requirement
void VEC_HIST::Inicialize(const TString _description = "UnSetUp", bool _ratio = false)
{
	cout << "Set up" << endl;
	this->Set_describtion(_description);
	this->Set_ratio(_ratio);
	
	cout << "Preparing" << endl;
	this->PrepareCanvas();
	this->PrepareCanvasLog();
	this->PrepareCanvasLogLog();
	
	this->PreparePad();
	this->PrepareLeg();
}


////////////////////////////////////
//         CONSTRUCTORS           //
////////////////////////////////////

VEC_HIST::VEC_HIST()
{
	vector<HIST*> _v;
	this->v = _v;
	this->Inicialize();
}


VEC_HIST::VEC_HIST(const vector<HIST*> &_v)
{
	this->v = _v;
	this->Inicialize();
}

VEC_HIST::VEC_HIST(const vector<HIST*> &_v, TString _description, bool _ratio)
{
	this->v = _v;
	cout << "Call Inicialization()" << endl;
	this->Inicialize(_description, _ratio);
}

VEC_HIST::VEC_HIST(const vector<HIST*> &_v, TString _description, bool _ratio, HIST * _hNominal)
{
	this->v = _v;
	cout << "Call Inicialization()" << endl;
	this->Inicialize(_description, _ratio, _hNominal);
}


VEC_HIST::VEC_HIST(const VEC_HIST & _v)
{
	this->v = _v.Get_vector();
	this->Inicialize(_v.Get_description());
}


/////////////////////////////
//       Destrunctor       //
/////////////////////////////


VEC_HIST::~VEC_HIST()
{
	///cout << "DESTRUCTOR VEC_HIST" << endl;
	delete m_c;
	delete m_pad1;
	delete m_pad2;
}



////////////////////////////////////
//         SET INSTANCES          //
////////////////////////////////////



bool VEC_HIST::Set_describtion(const TString _describtion)
{
    this->m_describtion = _describtion;
    return(true);
}

bool VEC_HIST::Set_config(const TString _config)
{
	/*********************************************************
	 *  Set-up configuration-file with setting of histograms *
	 *********************************************************/
	this->m_envConfig = _config;
	return(true);
}

bool VEC_HIST::Set_ratio(const bool _ratio = false)
{
	this->m_ratio = _ratio;
	if (_ratio == false)	
	{
		this->m_subDirRatio = "no_ratio";
	}
	else
	{
		this->m_subDirRatio = "ratio";
	}
	return(true);
}

bool VEC_HIST::Set_yLogScale(const bool _log)
{
	this->m_yLogScale = _log;
	return(true);
}

bool VEC_HIST::Set_xLogScale(const bool _log)
{
	this ->m_xLogScale = _log;
	return(true);
}

bool VEC_HIST::Set_histNominal(HIST * _histNominal)
{
	/****************************************************************
	 * Set-up nominal histogram                                     *
	 * nominal histogram is user for ratio calculation as denominar *
	 ***************************************************************/
	 
	this->m_hNominal = _histNominal;
	this->m_hNominalTH1D = _histNominal->Get_histogram();
	this->Set_vectorRatios(this->Get_vector(), this->Get_histNominal());
	return(true);
}

bool VEC_HIST::Set_vectorRatios(vector<HIST *> _v, HIST * _hNominal)
{
	/*****************************************************
	 *      Calculate and store the vector of ratia      *
	 *****************************************************/
	 
	vector <HIST *> vec_ratios;
	for(int i=0; i < _v.size(); i++){
		HIST * h = _v.at(i);
		TH1D* h_ratio = (TH1D*) _v.at(i)->Get_histogram()->Clone();
		TH1D* h_nominal = (TH1D*) _hNominal->Get_histogram()->Clone();
		int n = 10;
		double val, err;
		double val_n, err_n;
		for(int i =1; i < h->GetNbinsX(); i++)
		{
			cout << "TH1D i: " << i << endl;
			val = h->GetBinContent(i);
			err = h->GetBinError(i);
			val_n = _hNominal->GetBinContent(i);
			err_n = _hNominal->GetBinError(i);
			
			h_ratio->SetBinContent(i, val);
			h_ratio->SetBinError(i, err);
			
			h_nominal->SetBinContent(i, val_n);
			h_nominal->SetBinError(i, err_n);			
		}
		cout << "divide" << endl;
		h_ratio->Divide(h_ratio, h_nominal);
		
		/*
		TH1D* h_th1 = _v.at(i)->Get_histogram();
		TH1D* h_nominal = _hNominal->Get_histogram();
		cout << "h_nominal 5th : " << h_nominal->GetBinContent(2) << endl;

		TH1D* h_th1_r = (TH1D*) h_th1->Clone("h_th1_r");
		cout << "h_th1 5th : " << h_th1->GetBinContent(2) << endl;
		cout << "h_th1_r 5th before : " << h_th1_r->GetBinContent(2) << endl;
		
		h_th1_r->Divide(h_th1_r, _hNominal->Get_histogram());
		
		
		cout << "h_th1_r 5th after : " << h_th1_r->GetBinContent(2) << endl;
		cout << "5th" << endl;
		*/
		cout << "HIST*" << endl;
		HIST * ratio = new HIST(h_ratio, h->Get_index(), h->Get_key(), h->Get_leg_description(), h->Get_data_description(), h->Get_logScale(), h->Get_ratio() );
		//h_ratio = &hhh;
		//h_ratio->Copy(_v.at(i));	//->Clone("h_ratio")
		h_ratio->Divide(_v.at(i), _hNominal);
		cout << "vector 5th bin value: " << _v.at(i)->GetBinContent(2) << endl;
		cout << "hist 5th bin value: " << h->GetBinContent(2) << endl;
		cout << "h_nominal 5th bin value: "  << _hNominal->GetBinContent(2) << endl;
		cout << "h_ratio 5th bin value: " << h_ratio->GetBinContent(2) << endl;
		cout << "5th" << endl;
		cout << "5th" << endl;
		cout << "5th" << endl;

		//cout << "h_ratio->GetMaximum(): " << h_ratio->GetMaximum() << endl; 
		//auto h_ratio = new TRatioPlot(h, _hNominal);
		vec_ratios.push_back(ratio);
	}
	// Store the vector a ratio
	this->v_ratios = vec_ratios;
	
	// control couts
	// _hNominal->Show();
	// _hNominal->CheckHist();
	// this->ShowVector(_v);	
	// this->ShowVector(this->Get_vectorRatios());
	return(true);
}

bool VEC_HIST::Set_padYmax(const double_t _padYmax)
{
	this->m_padYmax = _padYmax;
	return(true);
}

bool VEC_HIST::Set_pad2Ymax(const double_t _pad2Ymax)
{
	this->m_pad2Ymax = _pad2Ymax;
	return(true);
}

bool VEC_HIST::Set_padYmin(const double_t _padYmin)
{
	this->m_padYmin = _padYmin;
	return(true);
}

bool VEC_HIST::Set_pad2Ymin(const double_t _pad2Ymin)
{
	this->m_pad2Ymin = _pad2Ymin;
	return(true);
}


bool VEC_HIST::Set_padYmax_log(const double_t _padYmax_log)
{
	this->m_padYmax_log = _padYmax_log;
	return(true);
}

bool VEC_HIST::Set_padYmin_log(const double_t _padYmin_log)
{
	this->m_padYmin_log = _padYmin_log;
	return(true);
}

bool VEC_HIST::Set_yMaxInPad1(const double_t	_yMaxInPad1)
{
	this->m_yMaxInPad1 = _yMaxInPad1;
	return(true);
}


bool VEC_HIST::Set_yMaxInPad2(const double_t	_yMaxInPad2)
{
	this->m_yMaxInPad2 = _yMaxInPad2;
	return(true);
}


bool VEC_HIST::Set_yMinInPad1(const double_t _yMinInPad1)
{
	this->m_yMinInPad1 = _yMinInPad1;
	return(true);
}


bool VEC_HIST::Set_yMinInPad2(const double_t _yMinInPad2)
{
	this->m_yMinInPad2 = _yMinInPad2;
	return(true);
}


bool VEC_HIST::Set_UserGraphic(void (*pf) (HIST*))
{
	/*********************************************
	 *       Set-up user graphic function        *
	 *********************************************/
	 
	this->m_UserGraphic = pf;
	return(true);
}

bool VEC_HIST::Set_UserGraphic_ratio(void (*pf) (HIST*))
{
	/*********************************************
	 *       Set-up user graphic function        *
	 *********************************************/
	 
	this->m_UserGraphic_ratio = pf;
	return(true);
}



void VEC_HIST::Check_YAxisMaximum_pad1()
{
	/***********************************************************
	 * Find the maximum value of all histograms stored vector  *
	 ***********************************************************/
	
	vector<double_t> y_max;
	for (auto & h : v) {
		y_max.push_back(h->GetMaximum());
	}
	double_t yMax = *max_element(std::begin(y_max), std::end(y_max));
	this->Set_yMaxInPad1(yMax);
}


void VEC_HIST::Check_YAxisMaximum_pad2()
{
	/***********************************************************
	 * Find the maximum value of all histograms stored vector  *
	 ***********************************************************/
	
	vector<double_t> y_max;
	for (auto & h : v_ratios) {
		y_max.push_back(h->GetMaximum());
	}
	double_t yMax = *max_element(std::begin(y_max), std::end(y_max));
	this->Set_yMaxInPad2(yMax);
}


void VEC_HIST::Check_YAxisMinimum_pad1()
{
	/**********************************************************
	* Find the minimum value of all histograms stored vector  *
	***********************************************************/
	 
	vector<double_t> y_min;
	for (auto & h : v) {
		y_min.push_back(h->GetMinimum());
	}
	double_t yMin = *min_element(std::begin(y_min), std::end(y_min));
	this->Set_yMinInPad1(yMin);
}

void VEC_HIST::Check_YAxisMinimum_pad2()
{
	/**********************************************************
	* Find the minimum value of all histograms stored vector  *
	***********************************************************/
	 
	vector<double_t> y_min;
	for (auto & h : v_ratios) {
		y_min.push_back(h->GetMinimum());
	}
	double_t yMin = *min_element(std::begin(y_min), std::end(y_min));
	this->Set_yMinInPad2(yMin);
}


void VEC_HIST::Set_yAxisFinalRanges(const bool _logScale)
{
	/***********************************************************************************
	 * Find maximum and minimum at y-axis of histogram,                                *
	 * evaluate the maximum and minimum of y-axis in the pad                           *
	 * there are two options - linear and logaritmic scale based on:                   *
	 * const bool _logScale                                                            *
	 *                                                                                 *
	 * const bool _logScale           ..... scale of pad .... linear or logaritmic     *
	 * const vector <HIST *> &_v  ..... vector of histogram                        *
	 **********************************************************************************/
	
	this->Check_YAxisMinimum_pad1();
	this->Check_YAxisMaximum_pad1();
	if (this->Get_ratio() == true)
	{
		this->Check_YAxisMinimum_pad2();
		this->Check_YAxisMaximum_pad2();	
	}
	double_t c_max = 1.3, c_min=0.7;
	
	double_t y_min_pad1, y_max_pad1, y_min_pad2, y_max_pad2;
	// Get minimal value at y-axis from all considered histograms
	// Get maximal value at y-axis from all considered histograms
	y_min_pad1 = this->Get_yMinInPad1();
	y_max_pad1 = this->Get_yMaxInPad1();
	
	y_min_pad2 = this->Get_yMinInPad2();
	y_max_pad2 = this->Get_yMaxInPad2();

	
	//cout << "Set_yAxisFinalRanges" << endl;
	//cout << "PAD Get_yMinInPad1(), PAD Get_yMaxInPad1(): " << this->Get_yMinInPad1() << "\t" << this->Get_yMaxInPad1() << endl;
	//cout << "PAD YMIN, PAD YMAX: " << this->Get_padYmin() << "\t" << this->Get_padYmax() << endl;
	if (_logScale == false){
		// setup y-range in canvas
		// linear scale
		y_min_pad1 = c_min * y_min_pad1;
		y_max_pad1 = c_max * y_max_pad1;
		
		//cout << "y_max: " << y_max << endl;
		//cout << "y_min: " << y_min << endl;
		this->Set_padYmax(y_max_pad1);
		this->Set_padYmin(y_min_pad1);
		if (this->Get_ratio() == true)
		{
			y_min_pad2 = c_min * y_min_pad2;
			y_max_pad2 = c_max * y_max_pad2;
		
			this->Set_pad2Ymax(y_max_pad2);
			this->Set_pad2Ymin(y_min_pad2);
		}
		
	}
	else{
		// setup y-range in canvas
		// logaritmic scale
		if (y_min_pad1 < 1E-10) y_min_pad1 = 1;
		if (y_max_pad1 < 1E-10) y_max_pad1 = 10;

		y_max_pad1 = pow(10, log10(y_min_pad1) + (log10(y_max_pad1) - log10(y_min_pad1))*c_max);
		y_min_pad1 = pow(10, log10(y_min_pad1) - (log10(y_max_pad1) - log10(y_min_pad1))*c_min);
		y_min_pad1 = 1/10.0;
		//cout << "y_max: " << y_max << endl;
		//cout << "y_min: " << y_min << endl;
		this->Set_padYmax_log(y_max_pad1);
		this->Set_padYmin_log(y_min_pad1);
		
		if (this->Get_ratio() == true)
		{
			y_min_pad2 = c_min * y_min_pad2;
			y_max_pad2 = c_max * y_max_pad2;
		
			this->Set_pad2Ymax(y_max_pad2);
			this->Set_pad2Ymin(y_min_pad2);
		}
	}

	
	//cout << "/////////////" << endl;
	//cout << "this->Get_padYmax(): " << this->Get_padYmax() << endl;
	//cout << "this->Get_padYmin(): " << this->Get_padYmin() << endl;
	//cout << "/////////////\n\n" << endl;
	
}

/////////////////////////////////
//       GET INSTANCES         //
/////////////////////////////////

TString VEC_HIST::Get_description() const
{
	return(this->m_describtion);
}

TString VEC_HIST::Get_config() const
{
	/****************************************************
	* Get path of the confi-file for histogram settings *
	*****************************************************/
	
	return(this->m_envConfig);
}

bool VEC_HIST::Get_ratio() const
{
	return(this->m_ratio);
}

TString VEC_HIST::Get_subDirRatio() const
{
	return(this->m_subDirRatio);
}

bool VEC_HIST::Get_yLogScale() const
{
	return(this->m_yLogScale);
}

bool VEC_HIST::Get_xLogScale() const
{
	return(this->m_xLogScale);
}

HIST * VEC_HIST::Get_histNominal() const
{
	/*************************************************
	* Get the nominal histogram for ratio evaluation *
	**************************************************/
	 
	return(this->m_hNominal);
}


myHIST_TYPE* VEC_HIST::Get_histNominalTH1D() const
{
	return(this->m_hNominalTH1D);
}
vector<HIST*> VEC_HIST::Get_vectorRatios() const
{
	/*********************************
	* Get vector of ratio histograms *
	**********************************/
	
	return(this->v_ratios);
}


TLegend* VEC_HIST::Get_Legend() const
{
	return(m_leg);
}

auto VEC_HIST::Get_UserGraphic() const -> void (*)(HIST*)
{
	/***************************************************
	 * Get poiter on the user-defined graphic function *
	 ***************************************************/
	 
	return(this->m_UserGraphic);
}

TCanvas * VEC_HIST::Get_canvas()
{
	/*
	this->Check_YAxisMinimum_pad1();
	this->Check_YAxisMaximum_pad1();
	
	double_t c_max = 1.0, c_min=1.0;
	
	double_t y_min, y_max;
	// Get minimal value at y-axis from all considered histograms
	// Get maximal value at y-axis from all considered histograms
	y_min = this->Get_yMinInPad1();
	y_max = this->Get_yMaxInPad1();
	
	y_min = c_min * y_min;
	y_max = c_max * y_max;
		
	this->m_pad1->cd();
	
	cout << "PAD1_set_minimum,_maximum: " << y_min << "\t" << y_max << endl;

			
	if (y_max < 1E6)
	{
		for (auto h: v)
		{
			h->SetMinimum(y_min);
			h->SetMaximum(y_max);
			h->Draw("same");
			//if (this->Get_yLogScale() == false)
			//{
				// << "PAD1_set_minimum,_maximum: " << y_min_pad1 << "\t" << y_max_pad1 << endl;
			//}
		}
	}		
	//v.back()->SetMinimum(this->Get_yMinInPad1());
	//v.back()->SetMaximum(this->Get_yMaxInPad1());
	//v.back()->Draw("same");
	
	m_pad1->Update();
	m_c->Modified();
	m_c->Update();
	*/
	
	
	
	/*
	this->Set_xLogScale(false);
	this->Set_yLogScale(false);
	this->PrepareCanvas();
	
	this->PreparePad();
	this->PrepareToPlot();
	*/
	return(this->m_c);
}

TCanvas * VEC_HIST::Get_canvas_log()
{
	//this->Set_xLogScale(false);
	//this->Set_yLogScale(true);
	//this->PrepareCanvasLog();
	//this->PreparePad();
	//this->PrepareToPlot();
	return(this->m_c_log);
}

TCanvas * VEC_HIST::Get_canvas_loglog()
{
	//this->Set_xLogScale(true);
	//this->Set_yLogScale(true);
	//this->PrepareCanvasLogLog();
	//this->PreparePad();
	//this->PrepareToPlot();
	
	return(this->m_c_loglog);
}

double_t VEC_HIST::Get_yMaxInPad1() const
{
	return(this->m_yMaxInPad1);
}

double_t VEC_HIST::Get_yMaxInPad2() const
{
	return(this->m_yMaxInPad2);
}

double_t VEC_HIST::Get_yMinInPad1() const
{
	return(this->m_yMinInPad1);
}


double_t VEC_HIST::Get_yMinInPad2() const
{
	return(this->m_yMinInPad2);
}


double_t VEC_HIST::Get_padYmax() const
{
	return(this->m_padYmax);
}

double_t VEC_HIST::Get_pad2Ymax() const
{
	return(this->m_pad2Ymax);
}

double_t VEC_HIST::Get_padYmin() const
{
	return(this->m_padYmin);
}
double_t VEC_HIST::Get_pad2Ymin() const
{
	return(this->m_pad2Ymin);
}

double_t VEC_HIST::Get_padYmax_log() const
{
	return(this->m_padYmax_log);
}

double_t VEC_HIST::Get_padYmin_log() const
{
	return(this->m_padYmin_log);
}

vector<HIST*> VEC_HIST::Get_vector() const
{
	/****************************
	 * Get vector of histograms *
	 ****************************/
	 
	return(this->v);
}

/////////////////////////////////
//       PREPARE METHODS       //
/////////////////////////////////

void VEC_HIST::PrepareCanvas()
{
	if (this->Get_ratio() == true)	m_c = new TCanvas("c", "canvas",0,0,1200,1200);
	else							m_c = new TCanvas("c", "canvas",0,0,1200,600);

	m_c->cd();
	gStyle->SetOptStat(0);
	gStyle->SetTextFont(72);
	cout << "canvas prepared" <<endl;
}


void VEC_HIST::PrepareCanvasLog()
{
	if (this->Get_ratio() == true)	m_c_log = new TCanvas("c_log", "canvas- log y-axis",0,0,1200,1200);
	else							m_c_log = new TCanvas("c_log", "canvas- log y-axis",0,0,1200,600);

	m_c_log->cd();
	gStyle->SetOptStat(0);
	gStyle->SetTextFont(72);
	cout << "canvas prepared" <<endl;
}

void VEC_HIST::PrepareCanvasLogLog()
{
	if (this->Get_ratio() == true)	m_c_loglog = new TCanvas("c_loglog", "canvas - log x-axis, log y-axis",0,0,1200,1200);
	else							m_c_loglog= new TCanvas("c_loglog", "canvas - log x-axis, log y-axis",0,0,1200,600);

	m_c_loglog->cd();
	gStyle->SetOptStat(0);
	gStyle->SetTextFont(72);
	cout << "canvas prepared" <<endl;
}



void VEC_HIST::PreparePad()
{
	if (this->Get_ratio() == true){
		m_pad1 = new TPad("pad1","pad1_log",0,0.5,1,1,0,0,0);
		m_pad1->SetBottomMargin(0.015); 
		gPad->SetLogy(1);
	}
	else {
		m_pad1 = new TPad("pad1","pad1",0,0.02,1,1,0,0,0);
		m_pad1->SetBottomMargin(0.20); 
	}

	m_pad1->SetRightMargin(0.05);
	m_pad1->SetTopMargin(0.05);
	m_pad1->SetTicks();
	m_pad1->SetBorderMode(0);
	m_pad1->Draw();

	if (this->Get_ratio() == true){
		m_pad2 =	new TPad("pad2","pad2",0,0,1,0.5,0,0,0);
		m_pad2->SetBottomMargin(0.20);
		m_pad2->SetTopMargin(0.025);
		m_pad2->SetRightMargin(0.05);
		m_pad2->SetTicks();
		m_pad2->SetGridy(1);
		m_pad2->Draw();
	}
}

void VEC_HIST::PrepareLeg()
{
	m_leg  = new TLegend (0.5,0.30,0.9,0.86);
	m_leg->SetFillColor(0);
	m_leg->SetFillStyle(0);
	m_leg->SetBorderSize(0); 
	
	cout << "Legend is prepared" << endl;
}

// Prepare directory structure
void VEC_HIST::PrepareOutDIRS(TString _path) const
{
	cout << "make directories" << endl;
	for (auto & iDir : this->m_vDir) {
		TString OUTPUT = _path + "/" + iDir;
		
		system("mkdir -p " + OUTPUT);
		system("mkdir -p " + OUTPUT + "/" + this->m_subDir);
		system("mkdir -p " + OUTPUT + "/" + this->m_subDir + "/" + this->Get_subDirRatio());
		

	}
	cout << "directories are DONE" << endl;
}


void VEC_HIST::SaveCanvas(TString _outputPath)
{
	/******************************************************************
	 * Save the histograms as pdf, eps, png in the prepared directory *
	 * structure                                                      *
	 *                                                                *
	 * TString _outputPat ...... path to prepared directory structure *
	 * ****************************************************************/
	cout << "Save canvas:" << endl;
	this->PrepareOutDIRS(_outputPath);
	for (auto &iDir : this->m_vDir)
	{
		cout << "saving to: " << iDir << endl;
		
		TString fileName = this->Get_vector().at(0)->GetName();
		fileName.ReplaceAll("h_", "");
		fileName += this->Get_description(); 
		
		TString logScale = "";
		if(this->Get_yLogScale() == true) logScale += "_log";
		if(this->Get_xLogScale() == true) logScale += "log";
				
		//cout << "path: " << _outputPath + "/" + iDir + "/" + this->m_subDir + "/" + this->Get_subDirRatio() + "/" + fileName + logScale + "." + iDir <<endl;
		TCanvas *c;
		
		if(this->Get_yLogScale() == false && this->Get_xLogScale() == false)
		{
			c = this->Get_canvas();
		}
		else if(this->Get_yLogScale() == true && this->Get_xLogScale() == false)
		{
			c = this->Get_canvas_log();
		}
		else
		{
			c = this->Get_canvas_loglog();
		}
	
		c->SaveAs(_outputPath + "/" + iDir + "/" + this->m_subDir + "/" + this->Get_subDirRatio() + "/" + fileName + logScale + "." + iDir);
	}
}


void VEC_HIST::Show() const
{
	/****************************************************
	 *                                                  *
	 * Show the instances of HIST and some values   *
	 * of histogram as TH1D like Integram, maximum, ....*
	 *                                                  *
	 * Use for control of loaded histogram              *
	 *                                                  *
	 ****************************************************/
	 
	cout << "In Show method:" << endl;
	for (auto & hist : this->v){
		hist->Show();
		hist->CheckHist();
	}
}



void VEC_HIST::PrepareToPlot()
{
	/**********************************************************
	 *              Basic method of the class                 *
	 * enter to the pad, call graphic methods and Draw to pad *
	 **********************************************************/
	 //delete m_c; 
	 //this->PrepareCanvas();
	// vector of pads 
	//this->Get_canvas()->Clear();
	//m_c->Clear();
	// Chose canvas
	TCanvas *c;
	if(this->Get_yLogScale() == false && this->Get_xLogScale() == false)
	{
		cout << "\n\n SET NO LOG CANVAS" << endl;
		c = m_c; //this->Get_canvas();
	}
	else if(this->Get_yLogScale() == true && this->Get_xLogScale() == false)
	{
		cout << "\n\n SET LOG CANVAS" << endl;
		c = m_c_log;	//this->Get_canvas_log();
	}
	else
	{
		cout << "\n\n SET LOG LOG CANVAS" << endl;
		c = m_c_loglog;	//this->Get_canvas_loglog();
	}
	c->cd();
	
	vector<TPad *> v_pad;
	vector<int> v_int;
	int dimArrPad = 0;
	if(this->Get_ratio() == true)
	{
		//cout << "GET_RATIO: true" << endl; 
		//v_pad.push_back(this->m_pad1);
		//v_pad.push_back(this->m_pad2);
		//v_int.push_back(1);
		//v_int.push_back(2);
		dimArrPad = 2;
		cout << "GET_RATIO: dimArrPad: " << dimArrPad << endl;
	}
	else
	{
		dimArrPad = 1;
		cout << "GET_RATIO: false" << endl;
		//v_pad.push_back(this->m_pad1);
		//v_int.push_back(1);

		cout << "GET_RATIO: dimArrPad : " << dimArrPad << endl;
	}
	TPad **arrPad = new TPad*[dimArrPad];
	arrPad[0] = m_pad1;
	if (this->Get_ratio() == true) arrPad[1] = m_pad2;
	
	// loop over pads
	for(int i = 0; i < dimArrPad; i++)
	{
		TPad * pad = arrPad[i];
		// enter to the pad
		c->cd();
		c->Draw();
		pad->Draw();
		pad->cd();
		pad->Draw();
		
		// Set up lox scales
		if(pad == this->m_pad1 && this->Get_xLogScale() == true)	gPad->SetLogx(1);
		if(pad == this->m_pad1 && this->Get_yLogScale() == true)	gPad->SetLogy(1);
		
		// NOT WORKING CORRECTLY
		if(pad == this->m_pad2 && this->Get_xLogScale() == true)	gPad->SetLogx(1);
		
		
		// load the vector of appropriate histograms
		// there are two choice:
		// pad1 - absolut distribution 
		// or
		// pad2 - ratio distribution

		
		// calculate y-maximum and y-minimum 
		// and evaluate y_min, y_max in pad 
		this->Set_yAxisFinalRanges(this->Get_yLogScale());
		double_t y_min_pad1, y_max_pad1, y_min_pad2, y_max_pad2;
		

		if (this->Get_yLogScale() == false)
		{
			y_min_pad1 = this->Get_padYmin();
			y_max_pad1 = this->Get_padYmax();
			
			y_min_pad2 = this->Get_pad2Ymin();
			y_max_pad2 = this->Get_pad2Ymax();
			
		}
		else
		{
			y_min_pad1 = this->Get_padYmin_log();
			y_max_pad1 = this->Get_padYmax_log();
			
			y_min_pad2 = this->Get_pad2Ymin();
			y_max_pad2 = this->Get_pad2Ymax();
			
			if (pad == m_pad2)
			{
				y_min_pad2 = 0.01;
				y_max_pad2 = 2.00;
			}
		}
		
		//if (this->Get_ratio() == true)
		//{
		//	y_min = 0.0;
		//	y_max = 2.0;
		//}
		
		//if (pad == this->m_pad2)
		//{
		//	y_min = 0.0;
		//	y_max = 2.0;
		//}
		//cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
		//cout << "y_min_pad, y_max: " << y_min << " \t" << y_max << endl;

		vector<HIST*> histos;
		if (i==0)
		{
			histos = this->Get_vector();
		}
		else if (i ==1)
		{
			 histos = this->Get_vectorRatios();
		}
		// loop over all considered distributions
		for(auto h: histos){
			
			h->UseCurrentStyle();	// reset all graphic settings
									// important for imported histograms

			// basic graphic setting
			TString config = this->Get_config();
			h->PrepareToPlotBases(config);
			
			// set up min and maximum at y-axis
			// y_min and y_max are evaluate in Set_yAxisFinalRanges method
			// the values are stored as intrinsic instances
			//if (histos == v)
			//{
			if (pad == m_pad1)
			{
				h->SetMinimum(y_min_pad1);
				h->SetMaximum(y_max_pad1);
				if (this->Get_yLogScale() == false)
				{
				cout << "\t\tPAD1_set_minimum,_maximum: " << y_min_pad1 << "\t" << y_max_pad1 << endl;
				}
			}
			else if (pad == m_pad2)
			{
				h->SetMinimum(y_min_pad2);
				h->SetMaximum(y_max_pad2);
			}
			//}
			//else
			//{
			//	h->SetMinimum(1.05);	//y_min_pad2);
			//	h->SetMaximum(0.95);	//y_max_pad2);
			// user graphic setting -- 	user graphic setting is defined in function,
			//							the user-function is loaded as class instance

			if (this->m_UserGraphic != NULL){
				this->m_UserGraphic(h);
			}
			if (pad == m_pad2 && this->m_UserGraphic_ratio != NULL)
			{
				//cout << "USER_GRAPHIC_RATIO" << endl;
				this->m_UserGraphic_ratio(h);
			
			}

			//if (pad != m_pad1)
			//{
			//	 h->SetMaximum(2.0);
			//		 h->SetMinimum(0.01);
			//}
			h->Draw("SAME PLC PMC");
			
			if(m_bAddHistPad1 == true && h == this->Get_vector().back())
			{
				m_addHistPad1->SetLineColor(kRed);
				m_addHistPad1->Draw("SAME");
			}
			
			TString legName = h->Get_leg_description();
			
			// Do not forget on the legend
			//if(pad == this->m_pad1){
				if(h == this->Get_vector().at(0))
				{
					/// && this->Get_yLogScale() == true){
					PlotTText();
				}
				
				if (this->Get_yLogScale() == false && pad == this->m_pad1)
				{
					this->Get_Legend()->AddEntry(h, legName, "pf");
					
					if(m_bAddHistPad1 == true && h == this->Get_vector().back())
					{
						this->Get_Legend()->AddEntry(m_addHistPad1, "all slices added", "pf");
					}
				}
			}
			
				// Updates
			pad->Update();
			c->Modified();
			c->Update();
		//}

		// Draw legend only in the pad1
		if (pad == this->m_pad1)	m_leg->Draw();

		// Updates
		pad->Update();
		c->Modified();
		c->Update();
		//c->SaveAs(this->Get_description() + "_logX" + std::to_string(this->Get_xLogScale()) + "_logY" + std::to_string(this->Get_yLogScale()) + ".pdf");
	}
}






void VEC_HIST::Plot(TString path)
{
	// vector of linear/logartimic scale settings
	//vector<TString> vecLogScale = {"Lin", "Log", "LogLog"};
	//vector<TString> vecLogScale = {"Lin"};
	vector<TString> vecLogScale = {"Lin", "Log", "LogLog"}; //, "LogLog"};

	vector<bool> vecRatio = {false, true};
	
	// loop over logaritmic settings
	for (auto const &scale: vecLogScale)
	{
		if(scale == "Lin")
		{
			this->Set_xLogScale(false);
			this->Set_yLogScale(false);
			this->PrepareCanvas();
		}
		else if (scale == "Log")
		{
			this->Set_xLogScale(false);
			this->Set_yLogScale(true);
			this->PrepareCanvasLog();
		}
		else if (scale == "LogLog")
		{
			this->Set_xLogScale(true);
			this->Set_yLogScale(true);
			this->PrepareCanvasLogLog();
		}
		//for (auto r: vecRatio){
		//	this->Set_ratio(r);

			this->PreparePad();
			this->PrepareToPlot();
			this->SaveCanvas(path);
		//}
	}

	
	cout << "DONE!" << endl;
}


void VEC_HIST::ShowVector(vector<HIST *> _v) const
{
	/****************************************************
	 *    Show basic propertis of histogram in vector   *
	 ****************************************************/
	
	cout << "\n\n\n\n\n\nSHOW VECTOR" << endl;
	for (auto h: _v){
		h->CheckHist();
		h->Show();
	}
}


void VEC_HIST::PlotTText()
{
	/*
	title.ReplaceAll("#", " #");
	title.ReplaceAll(",\\}", "} ");
	cout << "title in canvas: " << title << endl;
	*/
	
	double_t max = Get_yMaxInPad1();
	double_t min = Get_yMinInPad1();
	double_t delta_text = 0;


	cout << "max: " << max << "\t min: "<< min << endl;
	//if(gPad->GetLogy() == false)	
	delta_text = abs(max - min)/10;
	//else
	//{
	//	delta_text = abs(max - min)/10
	//}
	
	float y_position;
	int ind=0;
	TString title = this->Get_vector().at(0)->Get_data_description();
	
	cout << "TITLE BEGIN" << title << endl;
	TString text;
	while(title.Contains("{"))
	{
		ind +=1;
		cout << "\n\nindex: " << ind << endl;
		if (this->Get_yLogScale() == false)
		{
			y_position = max - ind * delta_text;
		}
		else
		{
			if (max == 0) max = 1;
			if (min == 0) min = 1;
			
			cout << "log(100)" << log10(100) << endl;
			float ylog_delta = (log10(max) - log10(min))/7;
			float ylog_position = log10(max) - ind * ylog_delta;
			y_position = pow(10, ylog_position);
			cout << "LOG SCALE: " << "\t ylog_delta: " << ylog_delta << "\tylog_position: " << ylog_position << "\t y_position: " <<  y_position << endl;
		}
		text = title(title.Index("{") + 1, title.Index("}") -1);
		title.ReplaceAll("{" + text + "}", "");
		
		cout << "title: " << title <<endl;
		cout << "text: " << text << endl;
		
		double x_position=100.0;
		if (this->Get_xLogScale() == true) x_position = 40.0;
		
		TText *t = new TText(x_position, y_position, text);
		t->SetTextAlign(11); 
		t->SetTextSize(0.04);
		t->Draw();
		//cout << "ind in canvas: " << id << endl;
	}
	
	
}

void VEC_HIST::ResetRatia(vector<HIST*> _v)
{
	v_ratios.clear();
	v_ratios.assign(_v.begin(), _v.end());
	this->Set_ratio(true);
}

void VEC_HIST::CreateAddHist()
{
	m_addHistPad1 = new TH1D ("m_addHistPad1", "All HISTogram in pad 1 are added", 15, 75, 2000);//GetNbinsX(), Double_t xlow, Double_t xup)
	if (m_bAddHistPad1 == true)
	{
		cout << "\n\nHISTOGRAMS ARE ALREADY MERGE!!!\n\n" << endl;
	}
	else
	{
		for (int i=0; i< this->Get_vector().size(); i++)
		{
			m_addHistPad1->Add(this->Get_vector().at(i));
		}
		m_bAddHistPad1 = true;
	}
}

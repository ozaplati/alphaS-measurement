#include <iostream>
#include "TString.h"
#include "TFile.h"

#include "../AnalHelper/AnalException.h"

using namespace std;

void PrintTFile(TString rootFile, TString pathInRootFile)
{
	TFile * f = new TFile(rootFile, "READ");
	TFile * fn = (TFile *) f->Get(pathInRootFile);
	fn->Print();
}


int main(int nArg, char **arg)
{
	try
	{
		if(nArg !=3)
		{
			cout << "Error: Wrong number of input parameters!" << endl;
			cout << nArg << " were used" << endl;
			throw AnalException("Error: Wrong number of input parameters!"); 
		}
		
		TString rootFile = arg[1];
		TString pathInRootFile = arg[2];
		
		
		PrintTFile(rootFile, pathInRootFile);
		return(0);
	}
		catch(AnalException l){
		l.printMessage();
		cout << "Exiting now!" << endl;
		return (-1);
	}
}

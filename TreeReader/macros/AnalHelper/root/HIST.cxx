#include "../AnalHelper/HIST.h"

using namespace std;

//////////////////////////////////////
//**********************************//
//         HIST CLASS           //
//**********************************//
//////////////////////////////////////


//////////////////////////////////////
//           SET INSTANCES          //
//////////////////////////////////////
bool HIST::Set_index(const int _index)
{
	this->m_index = _index;
	return(true);
};

bool HIST::Set_key(const TString _key)
{
	this->m_key = _key;
	return(true);
}

bool HIST::Set_hist_description(const TString _str)
{
	this->m_hist_describe = _str;
	return(true);
}

bool HIST::Set_data_description(const TString _str)
{
	this->m_data_describe = _str;
	return(true);
}


bool HIST::Set_leg_description(const TString _str)
{
	this->m_leg_describe = _str;
	return(true);
}


bool HIST::Set_ratio(const bool _ratio)
{
	this->m_ratio = _ratio;
	return(true);
}

bool HIST::Set_logScale(const bool _log)
{
	this->m_logScale = _log;
	return(true);
}


bool HIST::Set_color(const int _color)
{
	this->m_color = _color;
	return(true);
}


bool HIST::Set_markerStyle(const int _markerStyle)
{
	this->m_markerStyle = _markerStyle;
	return(true);
}

bool HIST::Set_lineStyle(const int _lineStyle)
{
	this->m_lineStyle = _lineStyle;
	return(true);
}


bool HIST::Set_histogram(myHIST_TYPE * _h)
{
	this->m_HIST = _h;
	return(true);
}

////////////////////////////////////
//         GET INSTANCES          //
////////////////////////////////////

myHIST_TYPE * HIST::Get_histogram() const
{
	return(this->m_HIST);
}

int HIST::Get_index() const
{
	return(this->m_index);
}

TString HIST::Get_key() const
{
	return(this->m_key);
}

TString HIST::Get_hist_description() const
{
	return(this->m_hist_describe);
}

TString HIST::Get_data_description() const
{
	return(this->m_data_describe);
}

TString HIST::Get_leg_description() const
{
	return(this->m_leg_describe);
}

bool HIST::Get_ratio() const
{
	return(this->m_ratio);
}

bool HIST::Get_logScale() const
{
	return(this->m_logScale);
}

int HIST::Get_color() const
{
	return(this->m_color);
}

int HIST::Get_lineStyle() const
{
	return(this->m_lineStyle);
}

int HIST::Get_markerStyle() const
{
	return(this->m_markerStyle);
}

void HIST::Inicialize(myHIST_TYPE * _h, const int _index, const TString _key, const TString _leg_description, const TString _hist_description, const TString _data_description, const bool _log, const bool _ratio)
{
	this->Set_histogram(_h);
	this->Set_index(_index);
	this->Set_key(_key);
	this->Set_hist_description(_hist_description);
	this->Set_data_description(_data_description);
	this->Set_leg_description(_leg_description);
	this->Set_ratio(_ratio);
	this->Set_logScale(_log);
}

void HIST::Inicialize(myHIST_TYPE * _h, const int _index, const TString _key, const TString _leg_description, const TString _hist_description, const bool _log, const bool _ratio)
{
	this->Set_histogram(_h);
	this->Set_index(_index);
	this->Set_key(_key);
	this->Set_hist_description(_hist_description);
	this->Set_data_description("");
	this->Set_leg_description(_leg_description);
	this->Set_ratio(_ratio);
	this->Set_logScale(_log);
}

///////////////////////////////////
//         CONSTRUCTORS          //
///////////////////////////////////
HIST::HIST()
	:myHIST_TYPE()
{
	myHIST_TYPE* h;
	this->Inicialize(h, -1, "", "", "", "", false, false);
}

HIST::HIST(myHIST_TYPE*_h, const int _index, const TString _key, const TString _leg_description, const TString _hist_desctibtion, const bool _log, const bool _ratio)
	:myHIST_TYPE(*_h)
{
	this->Inicialize(_h, _index, _key,  _leg_description, _hist_desctibtion, _log, _ratio);

}

HIST::HIST(myHIST_TYPE* _h, const int _index, const TString _key, const TString _leg_description, const TString _hist_description, const TString _data_description, const bool _log, const bool _ratio)
	: myHIST_TYPE(*_h)
{
	//this->h = (myHIST_TYPE *) _h->Clone();
	this->Inicialize(_h, _index, _key, _leg_description, _hist_description, _data_description, _log, _ratio);
}

HIST::HIST(const HIST & _HIST) 
{
	myHIST_TYPE h;
	myHIST_TYPE *pH = new myHIST_TYPE;
	pH = (myHIST_TYPE*) _HIST.Clone();
	h = *pH;
	myHIST_TYPE * ph = &h;
	int h_index = _HIST.Get_index();
	TString h_key = _HIST.Get_key();
	TString h_hist_description = _HIST.Get_hist_description();
	TString h_data_description = _HIST.Get_data_description();
	TString h_leg_description = _HIST.Get_leg_description();
	bool h_ratioHist = _HIST.Get_ratio();
	bool h_log = _HIST.Get_logScale();
	
	
	this->Inicialize(ph, h_index, h_key, h_leg_description, h_hist_description, h_data_description, h_log, h_ratioHist);
}


HIST::~HIST()
{
	///cout << "Call destructor" << endl;
}

//////////////////////////
//    OPERATORS
/*
HIST HIST::operator=(const HIST &_jzxH)
{
	//this->h = (myHIST_TYPE *) _jzxH.Clone();
	myHIST_TYPE h = (myHIST_TYPE) _jzxH.Clone();
	myHIST_TYPE * ph = &h;
	int h_iJZX = _jzxH.GetIJZX();
	TString h_nTuple_name = _jzxH.GetJZX_nTuple_name();
	TString h_rootFile_path = _jzxH.GetJZX_rootFile_path();
	TString h_inRootFile_path = _jzxH.GetJZX_inRootFile_path();
	bool h_ratioHist = _jzxH.Get_ratio();
	bool h_log = _jzxH.Get_logScale();
	
	
	this->Inicialize(ph, h_iJZX, h_nTuple_name, h_rootFile_path, h_inRootFile_path, h_log, h_ratioHist);
}
*/
HIST* HIST::operator=(const HIST* _HIST)
{
	//this->h = (myHIST_TYPE *) _jzxH.Clone();
	myHIST_TYPE h;
	myHIST_TYPE *pH = new (myHIST_TYPE);
	pH = (myHIST_TYPE*) _HIST->Clone();
	h = *pH;
	myHIST_TYPE * ph = &h;
	int h_index = _HIST->Get_index();
	TString h_key = _HIST->Get_key();
	TString h_hist_description = _HIST->Get_hist_description();
	TString h_data_description = _HIST->Get_data_description();
	TString h_leg_description = _HIST->Get_leg_description();
	bool h_ratioHist = _HIST->Get_ratio();
	bool h_log = _HIST->Get_logScale();
	
	
	this->Inicialize(ph, h_index, h_key, h_leg_description, h_hist_description, h_data_description, h_log, h_ratioHist);
}

//friend HIST operator=(const HIST* _h1, const HIST *_h2);
/*
HIST HIST::operator=(HIST _h)
{
	myHIST_TYPE *h = (myHIST_TYPE*) _h.Clone();
	HIST newH = HIST(h, _h.GetIJZX(), _h.GetJZX_nTuple_name(), _h.GetJZX_rootFile_path(), _h.GetJZX_inRootFile_path(), _h.Get_logScale(), _h.Get_ratio());
	//newH->PrepareToPlotBases(config)
	cout << "OPERATOR org: " << _h.GetIJZX() << endl;
	cout << "OPERATOR new: " << newH.GetIJZX() << endl;
	return(newH);
}
*/

/*
HIST* HIST::Copy(const HIST *_h)
{
	myHIST_TYPE *h = (myHIST_TYPE*) _h->Get_histogram()->Clone();
	cout << "COLONE" << endl;
	HIST newH = HIST(h, _h->Get_index(), _h->Get_key(), _h->Get_leg_description(), _h->Get_hist_description(), _h->Get_data_description(), _h->Get_logScale(), _h->Get_ratio());
	//HIST *p = &newH;
	//this = newH;
	cout << "NEWH" << endl;
	return(&newH);
}
**/


HIST* HIST::Copy(const HIST * _h)
{
	/**********************************
	 * RETURN COPY OF INPUT HIST* _h  *
	 **********************************/
	  
	TH1D* h = (TH1D *)_h->Get_histogram()->Clone();
	int n = 10;
	double val, err;
	double val_n, err_n;
	for(int i =1; i < _h->GetNbinsX(); i++)
	{
		cout << "TH1D i: " << i << endl;
		val = _h->GetBinContent(i);
		err = _h->GetBinError(i);

		h->SetBinContent(i, val);
		h->SetBinError(i, err);
	}
	
	cout << "HIST*" << endl;
	HIST * hist_copy = new HIST(h, _h->Get_index(), _h->Get_key(), _h->Get_leg_description(), _h->Get_data_description(), _h->Get_logScale(), _h->Get_ratio() );
	return(hist_copy);
}

//////////////////////////
// Print Methods

std::ostream & operator<<(std::ostream & os, const HIST* h)
{
	os  << "hist name: \t\t" << h->GetName() << "\n";
	os  << "index: \t\t" << h->Get_index() << "\n";
	os  << "leg description: \t\t" << h->Get_leg_description() << "\n";
	os  << "hist description: \t\t" << h->Get_hist_description() << "\n";
	os  << "data description: \t\t" << h->Get_data_description() << "\n";
	return(os);
}

void HIST::Show() const
{
	cout << "//////////////////////////////" << endl;
	cout << "hist name: \t\t\t" << this->GetName() << "\n";
	cout << "index: \t\t\t" << this->Get_index() << "\n";
	cout << "leg description: \t\t" << this->Get_leg_description() << "\n";
	cout << "hist description: \t\t" << this->Get_hist_description() << "\n";
	cout << "data description: \t\t" << this->Get_data_description() << "\n";
	cout << "//////////////////////////////" << endl;
}

void HIST::CheckHist() const
{
	cout << "//////////////////////////////" << endl;
	cout << "hist name: " << this->GetName() << endl;
	cout << "hist integral: " << this->Integral() << endl;
	cout << "hist y-max value: " << this->GetMaximum() << endl;
	cout << "///////////////////////////////" << endl;
	cout << "\n" << endl;
}


/////////////////////////////
// Prepare to Plot Methods
void HIST::PrepareToPlotBases(const TString _config)
{
	///
	/// TString _config ... path to TEnv config file
	
	//void setupHistStyle(myHIST_TYPE* hist, TString iJZ, TString pathToConfigFile){
    //////////////////////////////////////////
    // READ SETUP FROM ENV FILE
    TEnv *config = new TEnv(_config);
   
    int color = 2;
    int markerstyle = 2;
    int linestyle = 2;
    
    //////////////////////////////////////////
    // stringstream to conver string to int
    TString config_key = this->Get_key();
    
    stringstream geek_c(config->GetValue(config_key + "_color", ""));
    geek_c >> color;
    
    stringstream geek_m(config->GetValue(config_key + "_marker", ""));
    geek_m >> markerstyle;
    
    stringstream geek_l(config->GetValue(config_key + "_linestyle", ""));
    geek_l >> linestyle;
    
    //////////////////////////////////////////
    // APPLY THE SETUP
    this->UseCurrentStyle(); // important for imported histograms

    //this->SetLineColor(color);
    //this->SetMarkerColor(color);
    this->SetMarkerSize(1.2);
    this->SetMarkerStyle(markerstyle);
    this->SetLineStyle(linestyle);
    
    //this->SetFillColor(kRed+2);
	//this->SetLineColor(kBlue+3);
	this->SetLineWidth(2); 
	this->SetLineStyle(2);
	this->SetFillStyle(3000);
	this->GetYaxis()->SetTitle("In PrepareToPlotBasas");
	this->GetXaxis()->SetTitle("In PrepareToPlotBasas");
    
    TString h_name = this->GetName();
    //if(h_name.Contains(config_key))	h_name += config_key;
    //h_name += " slice" ;
    this->SetName(h_name);
    //cout << "function h-name:  " << this->GetName() << endl;
    ///
    ///
    ///
    
	if(this->Get_ratio() == false){
		this->GetYaxis()->SetTitleOffset(0.6);
		this->GetXaxis()->SetLabelSize(0.065);
		
		this->GetXaxis()->SetTitleSize(0.07);

		this->GetXaxis()->SetLabelOffset(0.025);
		this->GetXaxis()->SetTitleOffset(1.2);

		this->GetYaxis()->SetLabelSize(0.065);
		this->GetYaxis()->SetTitleSize(0.07);
		
		this->GetYaxis()->SetTitle("I am in Get_ratio()");
		this->Draw("SAME PLC PMC");
		
		cout << "HIST CLASS - Draw()" << endl;
		int oo;
		cin >> oo;
		

		//cout << "/////" << endl;
		//cout << "/////" << endl;
		//cout << "I am in Get_ratio() == true" << endl;
		//cout << "/////" << endl;
		//cout << "/////" << endl;
	}
	else
	{
		this->GetYaxis()->SetLabelSize(0.025);
	}
	
	if(this->Get_ratio() == true){
		//h_ratio->Divide(h_ratio, h_signal_for_ratio);
		this->GetXaxis()->SetLabelSize(0.065);
		this->GetXaxis()->SetTitleSize(0.09);
		this->GetXaxis()->SetLabelOffset(0.015);
		this->GetXaxis()->SetTitleOffset(1.0);
		this->GetYaxis()->SetLabelSize(0.045);
		this->GetYaxis()->SetTitleSize(0.065);
		this->GetYaxis()->SetTitleOffset(0.7);
		//this->GetYaxis()->SetTitle("Z' sample/signal      ");
		//this->GetXaxis()->SetTitle(x_title);
		this->SetMaximum(10.0);		
	}
	//cout << "END OF PREPARE TO PLOT BASE\n\n\n" << endl;

}

void HIST::ResetTText()
{
	m_key = "";		// key string in config file
	m_hist_describe = "";
	m_data_describe = "";
	m_leg_describe = "";
	//m_ratio = true;
}

/////                                       /////
/////                                       /////
/////////////////////////////////////////////////
/////////////////////////////////////////////////

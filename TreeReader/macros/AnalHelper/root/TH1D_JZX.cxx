#include "../AnalHelper/TH1D_JZX.h"

using namespace std;

//////////////////////////////////////
//**********************************//
//         TH1D_JZX CLASS           //
//**********************************//
//////////////////////////////////////


//////////////////////////////////////
//           SET INSTANCES          //
//////////////////////////////////////
bool TH1D_JZX::SetIJZX(const int _iJZX)
{
	this->m_iJZX = _iJZX;
	return(true);
};

bool TH1D_JZX::SetRootFile_path(const TString _rootFile_path)
{
    this->m_JZX_rootFile_path = _rootFile_path;
    return(true);
}

bool TH1D_JZX::SetJZX_nTuple_name(const TString _nTuple_name)
{
    this->m_JZX_nTuple_name = _nTuple_name;
    return(true);
}

bool TH1D_JZX::SetJZX_name ()
{
    this->m_JZX_name = "JZ";
    this->m_JZX_name += std::to_string(this->m_iJZX);
    return(true);
}

bool TH1D_JZX::SetJZX_inRootFile_path(const TString _inRootFile_path)
{
    this->m_JZX_inRootFile_path = _inRootFile_path;
    return(true);
}

bool TH1D_JZX::Set_ratio(const bool _ratio)
{
	this->m_ratio = _ratio;
	return(true);
}

bool TH1D_JZX::Set_logScale(const bool _log)
{
	this->m_logScale = _log;
	return(true);
}


bool TH1D_JZX::Set_color(const int _color)
{
	this->m_color = _color;
	return(true);
}


bool TH1D_JZX::Set_markerStyle(const int _markerStyle)
{
	this->m_markerStyle = _markerStyle;
	return(true);
}

bool TH1D_JZX::Set_lineStyle(const int _lineStyle)
{
	this->m_lineStyle = _lineStyle;
	return(true);
}


bool TH1D_JZX::Set_TH1D(TH1D * _h)
{
	this->m_TH1D = _h;
	return(true);
}

////////////////////////////////////
//         GET INSTANCES          //
////////////////////////////////////

TH1D * TH1D_JZX::Get_TH1D() const
{
	return(this->m_TH1D);
}

int TH1D_JZX::GetIJZX() const
{
	return(this->m_iJZX);
}

TString TH1D_JZX::GetJZX_name() const
{
	return(this->m_JZX_name);
}
TString TH1D_JZX::GetJZX_nTuple_name() const
{
	return(this->m_JZX_nTuple_name);
}
TString TH1D_JZX::GetJZX_rootFile_path() const
{
	return(this->m_JZX_rootFile_path);
}
TString TH1D_JZX::GetJZX_inRootFile_path() const
{
	return(this->m_JZX_inRootFile_path);	
}

bool TH1D_JZX::Get_ratio() const
{
	return(this->m_ratio);
}

bool TH1D_JZX::Get_logScale() const
{
	return(this->m_logScale);
}

int TH1D_JZX::Get_color() const
{
	return(this->m_color);
}

int TH1D_JZX::Get_lineStyle() const
{
	return(this->m_lineStyle);
}

int TH1D_JZX::Get_markerStyle() const
{
	return(this->m_markerStyle);
}

void TH1D_JZX::Inicialize(TH1D * _h, const int _iJZX, const TString _nTuple_name, const TString _rootFile_path, const TString _inRootFile_path, const bool _log, const bool _ratio)
{
	this->Set_TH1D(_h);
	this->SetIJZX(_iJZX);
	this->SetJZX_name();
	this->SetRootFile_path(_rootFile_path);
	this->SetJZX_nTuple_name(_nTuple_name);
	this->SetJZX_inRootFile_path(_inRootFile_path);
	this->Set_ratio(_ratio);
	this->Set_logScale(_log);
}

void TH1D_JZX::Inicialize(TH1D * _h, const int _iJZX, const TString _nTuple_name, const bool _log, const bool _ratio)
{
	this->Set_TH1D(_h);
	this->SetIJZX(_iJZX);
	this->SetJZX_name();
	this->SetRootFile_path("");
	this->SetJZX_nTuple_name(_nTuple_name);
	this->SetJZX_inRootFile_path("");
	this->Set_ratio(_ratio);
	this->Set_logScale(_log);
}

///////////////////////////////////
//         CONSTRUCTORS          //
///////////////////////////////////
TH1D_JZX::TH1D_JZX()
	:TH1D()
{
	TH1D* h;
	this->Inicialize(h, -1, "UnSetup", "UnSetUp", "UnSetUp", false, false);
}

TH1D_JZX::TH1D_JZX(TH1D*_h, const int _iJZX = -1, const TString _description = "UnSetup", const bool _log = false, const bool _ratio = false)
	:TH1D(*_h)
{
	this->Inicialize(_h, _iJZX, _description, _log, _ratio);

}

TH1D_JZX::TH1D_JZX(TH1D* _h, const int _iJZX, const TString _nTuple_name, const TString _rootFile_path, const TString _inRootFile_path, const bool _log, const bool _ratio)
	: TH1D(*_h)
{
	//this->h = (TH1D *) _h->Clone();
	this->Inicialize(_h, _iJZX, _nTuple_name, _rootFile_path, _inRootFile_path, _log, _ratio);
}

TH1D_JZX::TH1D_JZX(const TH1D_JZX & _jzxH) //: TH1D
{
	//this->h = (TH1D *) _jzxH.Clone();
	TH1D h;
	TH1D *pH = new TH1D;
	pH = (TH1D*) _jzxH.Clone();
	h = *pH;
	TH1D * ph = &h;
	int h_iJZX = _jzxH.GetIJZX();
	TString h_nTuple_name = _jzxH.GetJZX_nTuple_name();
	TString h_rootFile_path = _jzxH.GetJZX_rootFile_path();
	TString h_inRootFile_path = _jzxH.GetJZX_inRootFile_path();
	bool h_ratioHist = _jzxH.Get_ratio();
	bool h_log = _jzxH.Get_logScale();
	
	
	this->Inicialize(ph, h_iJZX, h_nTuple_name, h_rootFile_path, h_inRootFile_path, h_log, h_ratioHist);
}


TH1D_JZX::~TH1D_JZX()
{
	cout << "Call destructor" << endl;
}

//////////////////////////
//    OPERATORS
/*
TH1D_JZX TH1D_JZX::operator=(const TH1D_JZX &_jzxH)
{
	//this->h = (TH1D *) _jzxH.Clone();
	TH1D h = (TH1D) _jzxH.Clone();
	TH1D * ph = &h;
	int h_iJZX = _jzxH.GetIJZX();
	TString h_nTuple_name = _jzxH.GetJZX_nTuple_name();
	TString h_rootFile_path = _jzxH.GetJZX_rootFile_path();
	TString h_inRootFile_path = _jzxH.GetJZX_inRootFile_path();
	bool h_ratioHist = _jzxH.Get_ratio();
	bool h_log = _jzxH.Get_logScale();
	
	
	this->Inicialize(ph, h_iJZX, h_nTuple_name, h_rootFile_path, h_inRootFile_path, h_log, h_ratioHist);
}
*/
TH1D_JZX* TH1D_JZX::operator=(const TH1D_JZX* _jzxH)
{
	//this->h = (TH1D *) _jzxH.Clone();
	TH1D h;
	TH1D *pH = new (TH1D);
	pH = (TH1D*) _jzxH->Clone();
	h = *pH;
	TH1D * ph = &h;
	int h_iJZX = _jzxH->GetIJZX();
	TString h_nTuple_name = _jzxH->GetJZX_nTuple_name();
	TString h_rootFile_path = _jzxH->GetJZX_rootFile_path();
	TString h_inRootFile_path = _jzxH->GetJZX_inRootFile_path();
	bool h_ratioHist = _jzxH->Get_ratio();
	bool h_log = _jzxH->Get_logScale();
	
	
	this->Inicialize(ph, h_iJZX, h_nTuple_name, h_rootFile_path, h_inRootFile_path, h_log, h_ratioHist);
}

//friend TH1D_JZX operator=(const TH1D_JZX* _h1, const TH1D_JZX *_h2);
/*
TH1D_JZX TH1D_JZX::operator=(TH1D_JZX _h)
{
	TH1D *h = (TH1D*) _h.Clone();
	TH1D_JZX newH = TH1D_JZX(h, _h.GetIJZX(), _h.GetJZX_nTuple_name(), _h.GetJZX_rootFile_path(), _h.GetJZX_inRootFile_path(), _h.Get_logScale(), _h.Get_ratio());
	//newH->PrepareToPlotBases(config)
	cout << "OPERATOR org: " << _h.GetIJZX() << endl;
	cout << "OPERATOR new: " << newH.GetIJZX() << endl;
	return(newH);
}
*/

TH1D_JZX* TH1D_JZX::Copy(const TH1D_JZX *_h)
{
	TH1D *h = (TH1D*) _h->Get_TH1D()->Clone();
	cout << "COLONE" << endl;
	TH1D_JZX newH = TH1D_JZX(h, _h->GetIJZX(), _h->GetJZX_nTuple_name(), _h->GetJZX_rootFile_path(), _h->GetJZX_inRootFile_path(), _h->Get_logScale(), _h->Get_ratio());
	TH1D_JZX *p = &newH;
	//this = newH;
	cout << "NEWH" << endl;
	return(p);
}


//////////////////////////
// Print Methods

std::ostream & operator<<(std::ostream & os, const TH1D_JZX* h)
{
	os  << "hist name: \t\t" << h->GetName() << "\n";
	os  << "nTuple name: \t\t" << h->GetJZX_nTuple_name() << "\n";
	os  << "rootFile path: \t\t" << h->GetJZX_rootFile_path() << "\n";
	os  << "path in rootFile: \t" << h->GetJZX_inRootFile_path() << "\n";
	return(os);
}

void TH1D_JZX::Show() const
{
	cout << "//////////////////////////////" << endl;
	cout  << "hist name: \t\t" << this->GetName() << "\n";
	cout  << "nTuple name: \t\t" << this->GetJZX_nTuple_name() << "\n";
	cout  << "rootFile path: \t\t" << this->GetJZX_rootFile_path() << "\n";
	cout  << "path in rootFile: \t" << this->GetJZX_inRootFile_path() << "\n";
	cout << "//////////////////////////////" << endl;
}

void TH1D_JZX::CheckHist() const
{
	cout << "//////////////////////////////" << endl;
	cout << "hist name: " << this->GetName() << endl;
	cout << "hist integral: " << this->Integral() << endl;
	cout << "hist y-max value: " << this->GetMaximum() << endl;
	cout << "///////////////////////////////" << endl;
	cout << "\n" << endl;
}


/////////////////////////////
// Prepare to Plot Methods
void TH1D_JZX::PrepareToPlotBases(const TString _config)
{
	///
	/// TString _config ... path to TEnv config file
	
	//void setupHistStyle(TH1D* hist, TString iJZ, TString pathToConfigFile){
    //////////////////////////////////////////
    // READ SETUP FROM ENV FILE
    TEnv *config = new TEnv(_config);
   
    int color, markerstyle, linestyle;
    
    //////////////////////////////////////////
    // stringstream to conver string to int
    TString JZX_name = this->GetJZX_name();
    
    stringstream geek_c(config->GetValue(JZX_name + "_color", ""));
    geek_c >> color;
  
    stringstream geek_m(config->GetValue(JZX_name + "_marker", ""));
    geek_m >> markerstyle;
  
    stringstream geek_l(config->GetValue(JZX_name + "_linestyle", ""));
    geek_l >> linestyle;
  
    //////////////////////////////////////////
    // APPLY THE SETUP
    this->UseCurrentStyle(); // important for imported histograms

    this->SetLineColor(color);
    this->SetMarkerColor(color);
    this->SetMarkerSize(1.2);
    this->SetMarkerStyle(markerstyle);
    this->SetLineStyle(linestyle);
    
    this->SetFillColor(kRed+2);
	this->SetLineColor(kBlue+3);
	this->SetLineWidth(2); 
	this->SetLineStyle(2);
	this->SetFillStyle(3000);
	this->GetYaxis()->SetTitle("In PrepareToPlotBasas");
	this->GetXaxis()->SetTitle("In PrepareToPlotBasas");
    
    TString h_name = this->GetName();
    if(h_name.Contains(JZX_name))	h_name += JZX_name;
    //h_name += " slice" ;
    this->SetName(h_name);
    //cout << "function h-name:  " << this->GetName() << endl;
    ///
    ///
    ///
    
	if(this->Get_ratio() == false){
		this->GetYaxis()->SetTitleOffset(0.6);
		this->GetXaxis()->SetLabelSize(0.065);
		
		this->GetXaxis()->SetTitleSize(0.07);

		this->GetXaxis()->SetLabelOffset(0.025);
		this->GetXaxis()->SetTitleOffset(1.2);

		this->GetYaxis()->SetLabelSize(0.065);
		this->GetYaxis()->SetTitleSize(0.07);
		
		this->GetYaxis()->SetTitle("I am in Get_ratio()");
		this->Draw("same");
		//cout << "/////" << endl;
		//cout << "/////" << endl;
		//cout << "I am in Get_ratio() == true" << endl;
		//cout << "/////" << endl;
		//cout << "/////" << endl;
	}
	else
	{
		this->GetYaxis()->SetLabelSize(0.025);
	}
	
	if(this->Get_ratio() == true){
		//h_ratio->Divide(h_ratio, h_signal_for_ratio);
		this->GetXaxis()->SetLabelSize(0.065);
		this->GetXaxis()->SetTitleSize(0.09);
		this->GetXaxis()->SetLabelOffset(0.015);
		this->GetXaxis()->SetTitleOffset(1.0);
		this->GetYaxis()->SetLabelSize(0.045);
		this->GetYaxis()->SetTitleSize(0.065);
		this->GetYaxis()->SetTitleOffset(0.7);
		//this->GetYaxis()->SetTitle("Z' sample/signal      ");
		//this->GetXaxis()->SetTitle(x_title);
		this->SetMaximum(10.0);		
	}
	//cout << "END OF PREPARE TO PLOT BASE\n\n\n" << endl;

}

/////                                       /////
/////                                       /////
/////////////////////////////////////////////////
/////////////////////////////////////////////////

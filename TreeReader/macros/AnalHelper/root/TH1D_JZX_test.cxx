#include "../AnalHelper/TH1D_JZX.h"
#include "TH1D_JZX.cxx"


using namespace std;




void checkTheHist(TH1D* h){
    cout << "hist name: " << h->GetName() << endl;
    cout << "hist integral: " << h->Integral() << endl;
    cout << "hist y-max value: " << h->GetMaximum() << endl;
    cout << "\n" << endl;	
}



void coutConfigEnv(int iJZ){
	//String strIJZ = std::to_string(iJZ);
    TString strIJZ =  std::to_string(iJZ); //strIJZ; //TString::Itoa(iJZ); //std::to_string(iJZ);
    cout << "\n\n" << "JZ" << strIJZ << "_color: " << strIJZ << endl;
	cout << "JZ" << strIJZ << "_marker: " << strIJZ << endl;
    cout << "JZ" << strIJZ << "_linestyle: " << strIJZ << endl;	
}

void prepareConfigEnvFile(){
    for (int i =0 ; i<13;  i++) coutConfigEnv(i);
}

void setupHistStyle(TH1D* hist, TString iJZ, TString pathToConfigFile){
    //////////////////////////////////////////
    // READ SETUP FROM ENV FILE
    TEnv *config = new TEnv(pathToConfigFile);
 
  
    int color, markerstyle, linestyle;
    //////////////////////////////////////////
    // stringstream to conver string to int
    stringstream geek_c(config->GetValue("JZ" + iJZ + "_color", ""));
    geek_c >> color;
  
    stringstream geek_m(config->GetValue("JZ" + iJZ + "_marker", ""));
    geek_m >> markerstyle;
  
    stringstream geek_l(config->GetValue("JZ" + iJZ + "_linestyle", ""));
    geek_l >> linestyle;
  
    //////////////////////////////////////////
    // APPLY THE SETUP
    hist->SetLineColor(color);
    hist->SetMarkerColor(color);
    hist->SetMarkerSize(1.2);
    hist->SetMarkerStyle(markerstyle);
    hist->SetLineStyle(linestyle);
    
    TString h_name = hist->GetName();
    h_name += " JZ";
    h_name += iJZ;
    h_name += " slice" ;
    hist->SetName(h_name);
    cout << "function h-name:  " << hist->GetName() << endl;
}



int main(int nArg, char **arg) {
        if (nArg!=5){
            cout << "Error: Wrong number of input parameters!" << endl;
            cout << nArg << " were used" << endl;
            //throw
             cout << "Error: Wrong number of input parameters!" << endl; 
         }
     
         ////////////////////////////
         // set input arguments
         //
         TString path = arg[1];
         TString histName =  arg[2];
         TString pathInTFile = ""; //arg[3];
         TString OUTPUT_PATH = arg[3];
         TString config = arg[4]; //"/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/TreeReader/config/JZX_compare.env";
        
         vector<TH1D_JZX*> vTH1D_JZX;
         vector<double_t> vYmax;
         for (int iJZ=0; iJZ < 13; iJZ++){
			 if (iJZ != 1) continue;
			
			 TString JZX_name = "JZ" + std::to_string(iJZ);
			 cout << "Working on " << JZX_name << "slice" << endl;
			 
             TFile *tree = new TFile(path + "/" + "JZ" + std::to_string(iJZ) + "_tree.root", "READ");
             TH1D *hist1 = (TH1D*) tree->Get(pathInTFile + histName);
   //TH1D_JZX(TH1D* _h, const TString _JZX_name, const TString _nTuple_name, const TString _path, const TString _inRootFile_path);

             TH1D_JZX *hist = new TH1D_JZX(hist1, iJZ, "nTuple name", path, pathInTFile);
             cout << "////////////////" << endl;
             cout << hist << endl;
             cout << "/////////////////" << endl;
             hist->Show();
             hist->CheckHist();
             
             checkTheHist(hist);
             setupHistStyle(hist, std::to_string(iJZ), config);
             vTH1D_JZX.push_back(hist);
             cout << "after push_back" << endl;
             
         }
         
         
         //prepareConfigEnvFile();
         
         
        
         cout << "canvas " << endl;
         //////////////////
         // setup canvas		
         TCanvas *c = new TCanvas("c", "canvas",0,0,1200,1200);
         c->cd();
         gStyle->SetOptStat(0);
         gStyle->SetTextFont(72);
         bool bool_log = true;
         if(bool_log==true)	gPad->SetLogy(1);
         
         
        TPad *pad1 = 	new TPad("pad1","pad1",0,0.5,1,1,0,0,0);
		//Pad *pad2 =	new TPad("pad2","pad2",0,0,1,0.5,0,0,0);
  	    //if(bool_normalized==true){
		pad1->SetRightMargin(0.05);
		pad1->SetTicks();
		pad1->SetBottomMargin(0.015); 
		pad1->SetBorderMode(0);
		pad1->Draw();
		
         
         cout << "leg" << endl;
         ////////////////////
         //set the legend
         //TLegend *leg  = new TLegend (0.5,0.45,0.9,0.86);
         TLegend *leg  = new TLegend (0.5,0.30,0.9,0.86);
         leg->SetFillColor(0);
         leg->SetFillStyle(0);
         leg->SetBorderSize(0); 
         
         //double_t largest_element  = *max_element(vYmax.begin(),vYmax.end());
         cout << "loop again" << endl;
         for(int i=0; i < vTH1D_JZX.size(); i++){
			 cout << "before TH1D*" << endl;
             pad1->cd();

             TH1D * h = vTH1D_JZX.at(i);
             
             h->UseCurrentStyle(); // important for imported histograms
             h->Draw("same");
             
             TString legName = h->GetName(); // "JZ" + std::to_string(iJZ);
             legName.ReplaceAll(histName, "");
             leg->AddEntry(h, legName, "pf");
             c->Update();
             cout << "seconf for end" << endl;
             
         }
         leg->Draw("same");


         
         
         
         //cout << "largest element: " << largest_element << endl;
         //mkdir("c:/myfolder");
         cout << "make directories" << endl;
         TString OUTPUT_PDF = OUTPUT_PATH + "/pdf";
         system("mkdir -p " + OUTPUT_PDF);
         system("mkdir -p " + OUTPUT_PDF + "/JZX_compare");
         cout << "save canvas" << endl;
         c->SaveAs(OUTPUT_PATH + "/pdf/JZX_compare/" + histName +".pdf");
         
    //}
    //catch (int e){
    //    cout << "An exception occurred. Exception Nr. " << e << '\n';
    //}

        
	//int stop;
	//cin >> stop;
	//return(0);
}

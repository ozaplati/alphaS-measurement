#include "../AnalHelper/VecTH1D_JZX.h"

using namespace std;

/////////////////////////////////////////
//*************************************//
//         VecTH1D_JZX CLASS           //
//*************************************//
/////////////////////////////////////////


/////////////////////////////////
//    Inicialization modules   //
/////////////////////////////////

// Inicialization 
// using ratio requirement
void VecTH1D_JZX::Inicialize(const TString _nTuple_name, const TString _inRootFile_path, bool _ratio, TH1D_JZX * _hNominal)
{
	cout << "Set up" << endl;
	this->Set_nTuple_name(_nTuple_name);
	this->Set_inRootFile_path(_inRootFile_path);
	this->Set_ratio(_ratio);
	cout << "set up nominal" << endl;
	this->Set_histNominal(_hNominal);
	this->Set_vectorRatios(this->Get_vector(), this->Get_histNominal());
	
	
	cout << "Preparing" << endl;
	this->PrepareCanvas();
	this->PreparePad();
	this->PrepareLeg();
}

// Implicit inicialization 
// no ratio requirement
void VecTH1D_JZX::Inicialize(const TString _nTuple_name = "Un Setup", const TString _inRootFile_path = "Un Setup", bool _ratio = false)
{
	cout << "Set up" << endl;
	this->Set_nTuple_name(_nTuple_name);
	this->Set_inRootFile_path(_inRootFile_path);
	this->Set_ratio(_ratio);
	
	cout << "Preparing" << endl;
	this->PrepareCanvas();
	this->PreparePad();
	this->PrepareLeg();
}


////////////////////////////////////
//         CONSTRUCTORS           //
////////////////////////////////////

VecTH1D_JZX::VecTH1D_JZX()
{
	vector<TH1D_JZX*> _v;
	this->v = _v;
	this->Inicialize();
}


VecTH1D_JZX::VecTH1D_JZX(const vector<TH1D_JZX*> &_v)
{
	this->v = _v;
	this->Inicialize();
}

VecTH1D_JZX::VecTH1D_JZX(const vector<TH1D_JZX*> &_v, TString _nTuple_name, TString _inRootFilePath, bool _ratio)
{
	this->v = _v;
	cout << "Call Inicialization()" << endl;
	this->Inicialize(_nTuple_name, _inRootFilePath, _ratio);
}

VecTH1D_JZX::VecTH1D_JZX(const vector<TH1D_JZX*> &_v, TString _nTuple_name, TString _inRootFilePath, bool _ratio, TH1D_JZX * _hNominal)
{
	this->v = _v;
	cout << "Call Inicialization()" << endl;
	this->Inicialize(_nTuple_name, _inRootFilePath, _ratio, _hNominal);
}


VecTH1D_JZX::VecTH1D_JZX(const VecTH1D_JZX & _v)
{
	this->v = _v.Get_vector();
	this->Inicialize(_v.Get_nTuple_name(), _v.Get_inRootFile_path());
}


/////////////////////////////
//       Destrunctor       //
/////////////////////////////


VecTH1D_JZX::~VecTH1D_JZX()
{
	cout << "DESTRUCTOR VecTH1D_JZX" << endl;
	delete m_c;
	delete m_pad1;
	delete m_pad2;
}



////////////////////////////////////
//         SET INSTANCES          //
////////////////////////////////////

bool VecTH1D_JZX::Set_nTuple_name(const TString _nTuple_name)
{
    this->m_nTuple_name = _nTuple_name;
    return(true);
}

bool VecTH1D_JZX::Set_inRootFile_path(const TString _inRootFile_path)
{
    this->m_inRootFile_path = _inRootFile_path;
    return(true);
}

bool VecTH1D_JZX::Set_config(const TString _config)
{
	/*********************************************************
	 *  Set-up configuration-file with setting of histograms *
	 *********************************************************/
	this->m_envConfig = _config;
	return(true);
}

bool VecTH1D_JZX::Set_ratio(const bool _ratio = false)
{
	this->m_ratio = _ratio;
	if (_ratio == false)	
	{
		this->m_subDirRatio = "no_ratio";
	}
	else
	{
		this->m_subDirRatio = "ratio";
	}
	return(true);
}

bool VecTH1D_JZX::Set_yLogScale(const bool _log)
{
	this->m_yLogScale = _log;
	return(true);
}

bool VecTH1D_JZX::Set_xLogScale(const bool _log)
{
	this ->m_xLogScale = _log;
	return(true);
}

bool VecTH1D_JZX::Set_histNominal(TH1D_JZX * _histNominal)
{
	/****************************************************************
	 * Set-up nominal histogram                                     *
	 * nominal histogram is user for ratio calculation as denominar *
	 ***************************************************************/
	 
	this->m_hNominal = _histNominal;
	this->m_hNominalTH1D = _histNominal->Get_TH1D();
	this->Set_vectorRatios(this->Get_vector(), this->Get_histNominal());
	return(true);
}

bool VecTH1D_JZX::Set_vectorRatios(vector<TH1D_JZX *> _v, TH1D_JZX * _hNominal)
{
	/*****************************************************
	 *      Calculate and store the vector of ratia      *
	 *****************************************************/
	 
	vector <TH1D_JZX *> vec_ratios;
	for(int i=0; i < _v.size(); i++){
		TH1D_JZX * h = (TH1D_JZX *) _v.at(i);
		TH1D_JZX * h_ratio = new TH1D_JZX(*h);
		h_ratio->Divide(h_ratio, _hNominal);
		cout << "h_ratio->GetMaximum(): " << h_ratio->GetMaximum() << endl; 
		vec_ratios.push_back(h_ratio);
	}
	// Store the vector a ratio
	this->v_ratios = vec_ratios;
	
	// control couts
	// _hNominal->Show();
	// _hNominal->CheckHist();
	// this->ShowVector(_v);	
	// this->ShowVector(this->Get_vectorRatios());
	return(true);
}

bool VecTH1D_JZX::Set_padYmax(const double_t _padYmax)
{
	this->m_padYmax = _padYmax;
	return(true);
}

bool VecTH1D_JZX::Set_padYmin(const double_t _padYmin)
{
	this->m_padYmin = _padYmin;
	return(true);
}

bool VecTH1D_JZX::Set_yMaxInPad1(const double_t	_yMaxInPad1)
{
	this->m_yMaxInPad1 = _yMaxInPad1;
	return(true);
}

bool VecTH1D_JZX::Set_yMinInPad1(const double_t _yMinInPad1)
{
	this->m_yMinInPad1 = _yMinInPad1;
	return(true);
}

bool VecTH1D_JZX::Set_UserGraphic(void (*pf) (TH1D_JZX*))
{
	/*********************************************
	 *       Set-up user graphic function        *
	 *********************************************/
	 
	this->m_UserGraphic = pf;
	return(true);
}

void VecTH1D_JZX::Check_YAxisMaximum(const vector<TH1D_JZX*> &_v)
{
	/***********************************************************
	 * Find the maximum value of all histograms stored vector  *
	 ***********************************************************/
	
	vector<double_t> y_max;
	for (auto & h : _v) {
		y_max.push_back(h->GetMaximum());
	}
	double_t yMax = *max_element(std::begin(y_max), std::end(y_max));
	this->Set_yMaxInPad1(yMax);
}

void VecTH1D_JZX::Check_YAxisMinimum(const vector<TH1D_JZX*> &_v)
{
	/**********************************************************
	* Find the minimum value of all histograms stored vector  *
	***********************************************************/
	 
	vector<double_t> y_min;
	for (auto & h : _v) {
		y_min.push_back(h->GetMinimum());
	}
	double_t yMin = *min_element(std::begin(y_min), std::end(y_min));
	this->Set_yMinInPad1(yMin);
}


void VecTH1D_JZX::Set_yAxisFinalRanges(const vector<TH1D_JZX*> &_v, const bool _logScale)
{
	/***********************************************************************************
	 * Find maximum and minimum at y-axis of histogram,                                *
	 * evaluate the maximum and minimum of y-axis in the pad                           *
	 * there are two options - linear and logaritmic scale based on:                   *
	 * const bool _logScale                                                            *
	 *                                                                                 *
	 * const bool _logScale           ..... scale of pad .... linear or logaritmic     *
	 * const vector <TH1D_JZX *> &_v  ..... vector of histogram                        *
	 **********************************************************************************/
	
	this->Check_YAxisMinimum(_v);
	this->Check_YAxisMaximum(_v);
	double_t c_max = 1.3, c_min=0.7;
	
	double_t y_min, y_max;
	// Get minimal value at y-axis from all considered histograms
	// Get maximal value at y-axis from all considered histograms
	y_min = this->Get_yMinInPad1();
	y_max = this->Get_yMaxInPad1();
	
	cout << "Set_yAxisFinalRanges" << endl;
	cout << "PAD Get_yMinInPad1(), PAD Get_yMaxInPad1(): " << this->Get_yMinInPad1() << "\t" << this->Get_yMaxInPad1() << endl;
	cout << "PAD YMIN, PAD YMAX: " << this->Get_padYmin() << "\t" << this->Get_padYmax() << endl;
	if (_logScale == false){
		// setup y-range in canvas
		// linear scale
		y_min = c_min * y_min;
		y_max = c_max * y_max;
	}
	else{
		// setup y-range in canvas
		// logaritmic scale
		if (y_min < 1E-10) y_min = 1;
		if (y_max < 1E-10) y_max = 10;

		y_max = pow(10, log10(y_min) + (log10(y_max) - log10(y_min))*c_max);
		y_min = pow(10, log10(y_min) - (log10(y_max) - log10(y_min))*c_min);
	}
	cout << "y_max: " << y_max << endl;
	cout << "y_min: " << y_min << endl;
	this->Set_padYmax(y_max);
	this->Set_padYmin(y_min);
	
	cout << "/////////////" << endl;
	cout << "this->Get_padYmax(): " << this->Get_padYmax() << endl;
	cout << "this->Get_padYmin(): " << this->Get_padYmin() << endl;
	cout << "/////////////\n\n" << endl;
	
}

/////////////////////////////////
//       GET INSTANCES         //
/////////////////////////////////

TString VecTH1D_JZX::Get_nTuple_name() const
{
	return(this->m_nTuple_name);
}

TString VecTH1D_JZX::Get_inRootFile_path() const
{
	/*********************************************************************
	* Get path of the histogram in the rootFile structure of directories *
	**********************************************************************/
	return(this->m_inRootFile_path);
}

TString VecTH1D_JZX::Get_config() const
{
	/****************************************************
	* Get path of the confi-file for histogram settings *
	*****************************************************/
	
	return(this->m_envConfig);
}

bool VecTH1D_JZX::Get_ratio() const
{
	return(this->m_ratio);
}

TString VecTH1D_JZX::Get_subDirRatio() const
{
	return(this->m_subDirRatio);
}

bool VecTH1D_JZX::Get_yLogScale() const
{
	return(this->m_yLogScale);
}

bool VecTH1D_JZX::Get_xLogScale() const
{
	return(this->m_xLogScale);
}

TH1D_JZX * VecTH1D_JZX::Get_histNominal() const
{
	/*************************************************
	* Get the nominal histogram for ratio evaluation *
	**************************************************/
	 
	return(this->m_hNominal);
}


TH1D* VecTH1D_JZX::Get_histNominalTH1D() const
{
	return(this->m_hNominalTH1D);
}
vector<TH1D_JZX*> VecTH1D_JZX::Get_vectorRatios() const
{
	/*********************************
	* Get vector of ratio histograms *
	**********************************/
	
	return(this->v_ratios);
}


TLegend* VecTH1D_JZX::Get_Legend() const
{
	return(m_leg);
}

auto VecTH1D_JZX::Get_UserGraphic() const -> void (*)(TH1D_JZX*)
{
	/***************************************************
	 * Get poiter on the user-defined graphic function *
	 ***************************************************/
	 
	return(this->m_UserGraphic);
}

TCanvas * VecTH1D_JZX::Get_canvas() const
{
	return(this->m_c);
}

double_t VecTH1D_JZX::Get_yMaxInPad1() const
{
	return(this->m_yMaxInPad1);
}

double_t VecTH1D_JZX::Get_yMinInPad1() const
{
	return(this->m_yMinInPad1);
}



double_t VecTH1D_JZX::Get_padYmax() const
{
	return(this->m_padYmax);
}

double_t VecTH1D_JZX::Get_padYmin() const
{
	return(this->m_padYmin);
}



vector<TH1D_JZX*> VecTH1D_JZX::Get_vector() const
{
	/****************************
	 * Get vector of histograms *
	 ****************************/
	 
	return(this->v);
}

/////////////////////////////////
//       PREPARE METHODS       //
/////////////////////////////////

void VecTH1D_JZX::PrepareCanvas()
{
	if (this->Get_ratio() == true)	m_c = new TCanvas("c", "canvas",0,0,1200,1200);
	else							m_c = new TCanvas("c", "canvas",0,0,1200,600);

	m_c->cd();
	gStyle->SetOptStat(0);
	gStyle->SetTextFont(72);
	cout << "canvas prepared" <<endl;
}


void VecTH1D_JZX::PreparePad()
{
	if (this->Get_ratio() == true){
		m_pad1 = new TPad("pad1","pad1",0,0.5,1,1,0,0,0);
		m_pad1->SetBottomMargin(0.015); 
		gPad->SetLogy(1);
	}
	else {
		m_pad1 = new TPad("pad1","pad1",0,0.02,1,1,0,0,0);
		m_pad1->SetBottomMargin(0.20); 
	}

	m_pad1->SetRightMargin(0.05);
	m_pad1->SetTopMargin(0.05);
	m_pad1->SetTicks();
	m_pad1->SetBorderMode(0);
	m_pad1->Draw();

	if (this->Get_ratio() == true){
		m_pad2 =	new TPad("pad2","pad2",0,0,1,0.5,0,0,0);
		m_pad2->SetBottomMargin(0.20);
		m_pad2->SetTopMargin(0.025);
		m_pad2->SetRightMargin(0.05);
		m_pad2->SetTicks();
		m_pad2->SetGridy(1);
		m_pad2->Draw();
	}
}

void VecTH1D_JZX::PrepareLeg()
{
	m_leg  = new TLegend (0.5,0.30,0.9,0.86);
	m_leg->SetFillColor(0);
	m_leg->SetFillStyle(0);
	m_leg->SetBorderSize(0); 
	
	cout << "Legend is prepared" << endl;
}

// Prepare directory structure
void VecTH1D_JZX::PrepareOutDIRS(TString _path) const
{
	cout << "make directories" << endl;
	for (auto & iDir : this->m_vDir) {
		TString OUTPUT = _path + "/" + iDir;
		
		system("mkdir -p " + OUTPUT);
		system("mkdir -p " + OUTPUT + "/" + this->m_subDir);
		system("mkdir -p " + OUTPUT + "/" + this->m_subDir + "/" + this->Get_subDirRatio());
		

	}
	cout << "directories are DONE" << endl;
}


void VecTH1D_JZX::SaveCanvas(TString _outputPath) const
{
	/******************************************************************
	 * Save the histograms as pdf, eps, png in the prepared directory *
	 * structure                                                      *
	 *                                                                *
	 * TString _outputPat ...... path to prepared directory structure *
	 * ****************************************************************/
	cout << "Save canvas:" << endl;
	this->PrepareOutDIRS(_outputPath);
	for (auto &iDir : this->m_vDir)
	{
		cout << "saving to: " << iDir << endl;
		
		TString fileName = this->Get_vector().at(0)->GetName();
		fileName.ReplaceAll("h_", "");
		//fileName += this->Get_nTuple_name(); 
		
		TString logScale = "";
		if(this->Get_yLogScale() == true) logScale += "_log";
		if(this->Get_xLogScale() == true) logScale += "log";
				
		//cout << "path: " << _outputPath + "/" + iDir + "/" + this->m_subDir + "/" + this->Get_subDirRatio() + "/" + fileName + logScale + "." + iDir <<endl;
		
		m_c->SaveAs(_outputPath + "/" + iDir + "/" + this->m_subDir + "/" + this->Get_subDirRatio() + "/" + fileName + logScale + "." + iDir);
	}
}


void VecTH1D_JZX::Show() const
{
	/****************************************************
	 *                                                  *
	 * Show the instances of TH1D_JZX and some values   *
	 * of histogram as TH1D like Integram, maximum, ....*
	 *                                                  *
	 * Use for control of loaded histogram              *
	 *                                                  *
	 ****************************************************/
	 
	cout << "In Show method:" << endl;
	for (auto & hist : this->v){
		hist->Show();
		hist->CheckHist();
	}
}



void VecTH1D_JZX::PrepareToPlot()
{
	/**********************************************************
	 *              Basic method of the class                 *
	 * enter to the pad, call graphic methods and Draw to pad *
	 **********************************************************/
	 //delete m_c; 
	 //this->PrepareCanvas();
	// vector of pads 
	vector<TPad *> v_pad;
	if(this->Get_ratio() == true)
	{
		v_pad.push_back(this->m_pad1);
		v_pad.push_back(this->m_pad2);
	}
	else
	{
		v_pad.push_back(this->m_pad1);
	}
	
	// loop over pads
	for(auto pad: v_pad)
	{
		// enter to the pad
		pad->cd();
		
		// Set up lox scales
		if(pad == this->m_pad1 && this->Get_xLogScale() == true)	gPad->SetLogx(1);
		if(pad == this->m_pad1 && this->Get_yLogScale() == true)	gPad->SetLogy(1);
		
		// NOT WORKING CORRECTLY
		//if(pad == this->m_pad2 && this->Get_xLogScale() == true)	gPad->SetLogx(1);
		
		
		// load the vector of appropriate histograms
		// there are two choice:
		// pad1 - absolut distribution 
		// or
		// pad2 - ratio distribution
		vector<TH1D_JZX*> histos;
		if (pad == this->m_pad1)
		{
			histos = this->Get_vector();
		}
		else
		{
			 histos = this->Get_vectorRatios();
		}
		
		// calculate y-maximum and y-minimum 
		// and evaluate y_min, y_max in pad 
		this->Set_yAxisFinalRanges(histos, this->Get_yLogScale());
		double_t y_min, y_max;
		y_min = this->Get_padYmin();
		y_max = this->Get_padYmax();


		cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
		cout << "y_min, y_max: " << y_min << " \t" << y_max << endl;


		// loop over all considered distributions
		for(auto h: histos){
			
			h->UseCurrentStyle();	// reset all graphic settings
									// important for imported histograms

			// basic graphic setting
			TString config = this->Get_config();
			h->PrepareToPlotBases(config);
			
			// set up min and maximum at y-axis
			// y_min and y_max are evaluate in Set_yAxisFinalRanges method
			// the values are stored as intrinsic instances
			h->SetMinimum(y_min);
			h->SetMaximum(y_max);

			// user graphic setting -- 	user graphic setting is defined in function,
			//							the user-function is loaded as class instance

			if (this->m_UserGraphic != NULL){
				this->m_UserGraphic(h);
			}

			h->Draw("same");
		
			TString legName = h->GetJZX_name();
			legName += " ";
			legName += this->Get_nTuple_name();
			
			// Do not forget on the legend
			if(pad == this->m_pad1){
				if (this->Get_yLogScale() == false){
					this->Get_Legend()->AddEntry(h, legName, "pf");
				}
			}
			
			// Updates
			pad->Update();
			m_c->Modified();
			m_c->Update();
		}

		// Draw legend only in the pad1
		if (pad == this->m_pad1) m_leg->Draw();
		
	}
}





void VecTH1D_JZX::Plot(TString path)
{
	// vector of linear/logartimic scale settings
	vector<TString> vecLogScale = {"Lin", "Log", "LogLog"};
	vector<bool> vecRatio = {false, true};
	
	// loop over logaritmic settings
	for (auto const &scale: vecLogScale)
	{
		if(scale == "Lin")
		{
			this->Set_xLogScale(false);
			this->Set_yLogScale(false);
		}
		else if (scale == "Log")
		{
			this->Set_xLogScale(false);
			this->Set_yLogScale(true);
		}
		else if (scale == "LogLog")
		{
			this->Set_xLogScale(true);
			this->Set_yLogScale(true);
		}
		//for (auto r: vecRatio){
		//	this->Set_ratio(r);
			
			this->PrepareToPlot();
			this->SaveCanvas(path);
		//}
	}

	
	cout << "DONE!" << endl;
}


void VecTH1D_JZX::ShowVector(vector<TH1D_JZX *> _v) const
{
	/****************************************************
	 *    Show basic propertis of histogram in vector   *
	 ****************************************************/
	
	cout << "\n\n\n\n\n\nSHOW VECTOR" << endl;
	for (auto h: _v){
		h->CheckHist();
		h->Show();
	}
}

#include "../AnalHelper/MultiCanvas.h"

using namespace std;

////////////////////////////
//      SET METHODS
////////////////////////////
bool MultiCanvas::Set_lines(const unsigned int _lines)
{
	this->m_lines = _lines;
	return(true);
}

bool MultiCanvas::Set_rows(const unsigned int _rows)
{
	this->m_rows = _rows;
	return(true);
}

bool MultiCanvas::Set_output(const TString _outPut)
{
	this->m_outputFile = _outPut;
	return(true);
}

bool MultiCanvas::Set_vecCanvas(const vector<TCanvas *> _vecCanvas)
{
	this->m_vecCanvas = _vecCanvas;
}

////////////////////////////
//      GET METHODS
////////////////////////////

unsigned int MultiCanvas::Get_lines() const
{
	return(this->m_lines);
}

unsigned int MultiCanvas::Get_rows() const
{
	return(this->m_rows);
}

TString MultiCanvas::Get_outPut() const
{
	return(this->m_outputFile);
}

/////////////////
// CHECK IF WORKING CORRECTLY
vector <TCanvas *> MultiCanvas::Get_vecCanvas() const
{
	return(this->m_vecCanvas);
}

TCanvas * MultiCanvas::Get_multiCanvas() const
{
	return(this->m_multiCanvas);
}
//
/////////////////



////////////////////////////
//      CONSTRUCTOR
////////////////////////////

MultiCanvas::MultiCanvas(vector<TCanvas *> _vecCanvas, const unsigned int _lines, const unsigned int _rows, const TString _outPut)
{
	// Set up input parameters
	this->Set_lines(_lines);
	this->Set_rows(_rows);
	this->Set_output(_outPut);
	this->Set_vecCanvas(_vecCanvas);
	
	// Evaluate pixesl of wight and high of multiCanvas
	unsigned int pixels_x = _vecCanvas.at(1)->GetWw() * this->Get_rows();
	unsigned int pixels_y = _vecCanvas.at(1)->GetWh() * this->Get_lines();
	
	// Create multiCanvas
	TCanvas * m_multiCanvas = new TCanvas("multi_canvas", "multi_canvas", pixels_x, pixels_y);
	m_multiCanvas->Divide(_lines, _rows);
	cout << "MULTICANVAS IN CONSTRUCTOR" <<endl;

	/////////////////////////
	// loops over TCanvases
	
	// loop over rows
	for(int iRow=1; iRow <= this->Get_rows(); iRow++)
	{
		cout << "iRow: " << iRow << endl;
		//if(index_canvas >= m_vecCanvas.size() +1) break;
		// loop over lines
		for(int iLine = 1;  iLine <= this->Get_lines(); iLine++)
		{
			int index_canvas = (iLine-1)*this->Get_rows() + iRow;
			if(index_canvas >= m_vecCanvas.size() +1) break;
			cout << "\n\n\tindex_canvas: " << index_canvas << endl;
			// Enter to subCanvas
			m_multiCanvas->cd(index_canvas);
			// Draw to subCanvas
			cout << "\t enter to canvas" << endl;
			_vecCanvas.at(index_canvas-1)->DrawClonePad();
			cout << "\t DeawClon " << endl;
		}
		
	}
	m_multiCanvas->Update();
	// Save multiCanvas
    m_multiCanvas->SaveAs(this->Get_outPut());

}

////////////////////////////
//      DESTRUCTOR
////////////////////////////

MultiCanvas::~MultiCanvas()
{
	//delete m_multiCanvas;
}


////////////////////////////
//      OTHERS METHODS
////////////////////////////

void MultiCanvas::Show() const
{
	cout << "\n\nSHOW() METHOD" << endl;
	cout << "lines: " << this->Get_lines() << endl;
	cout << "rows: "  << this->Get_rows() << endl;
	cout << "number of TCanvas: " << this->Get_vecCanvas().size() << endl;
	cout << "output: " << this->Get_outPut();
	cout << "\n\n" << endl;
	
}

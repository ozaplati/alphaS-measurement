#pragma once

#include "TH1D_JZX.h"
#include "AnalException.h"
//#include "TH1D_JZX.cxx"

#include <iostream>
#include "TFile.h"
#include "TString.h"
#include <vector>
#include "TEnv.h"
#include "TH1.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TLegend.h"
#include <TROOT.h>
#include <TStyle.h>
#include <unistd.h>
#include <string> 
#include <sstream> 
#include "TPad.h"
#include<algorithm>


#ifndef VECTHD1_JZX_H
#define VECTHD1_JZX_H

using namespace std;
class VecTH1D_JZX
{
	protected:
		
		//////////////////////////////
		// Data containers
		vector<TH1D_JZX*> v;
		vector<TH1D_JZX*> v_ratios;
		
		//////////////////////////////
		// nominal histogram
		TH1D_JZX * m_hNominal = NULL;
		TH1D * m_hNominalTH1D = NULL;
		
		//////////////////////////////
		// TObjects
		TCanvas * m_c;
		TPad    * m_pad1;
		TPad    * m_pad2;
		TLegend * m_leg;
		
		TLegend *Get_Legend() const;
		
		//////////////////////////////
		// Log scale setup variable
		bool m_yLogScale;
		bool m_xLogScale;
		
		//////////////////////////////
		// user-graphic function
		void (*m_UserGraphic) (TH1D_JZX *) = NULL;
		
		const vector<TString> m_vDir = { "pdf", "png", "eps"};
		
	private:
		
		TString m_nTuple_name;
		TString m_inRootFile_path;
		TString m_envConfig;
		TString m_subDirRatio;
		const TString m_subDir = "JZX_compare";
		
		bool m_ratio = false;
		
		//////////////////////////////
		// Instances y-axis - range
		double_t m_yMaxInPad1;
		double_t m_yMinInPad1;
		double_t m_padYmax;
		double_t m_padYmin;
		
		/////////////////////////////
		// Set ranges at y-axis
		bool Set_yMaxInPad1(const double_t _yMaxInPad1);
		bool Set_yMinInPad1(const double_t _yMinInPad1);
		bool Set_padYmax(const double_t	_padYmax);
		bool Set_padYmin(const double_t _padYmin);

		void Inicialize(const TString _nTuple_name, const TString _inRootFile_path, const bool _ratio);
		void Inicialize(const TString _nTuple_name, const TString _inRootFile_path, const bool _ratio, TH1D_JZX * _hNominal);
		
		/////////////////////////////
		// Prepare methods
		void PrepareCanvas();
		void PreparePad();
		void PrepareLeg();
		
		/////////////////////////////
		// Check ranges
		double_t Get_yMaxInPad1() const;
		double_t Get_yMinInPad1() const;

		double_t Get_padYmax() const;
		double_t Get_padYmin() const;
		void Check_YAxisMaximum(const vector<TH1D_JZX *> &_v);
		void Check_YAxisMinimum(const vector<TH1D_JZX *> &_v);
		
		////////////////////////////
		// Plot methods
		void PrepareToPlot();
		void Show()const;
		
	public:
		////////////////////////////
		// Constructors
		VecTH1D_JZX();
		VecTH1D_JZX(const vector<TH1D_JZX*> &_v);
		VecTH1D_JZX(const vector<TH1D_JZX*> &_v, TString _nTuple_name, TString _inRootFilePath, bool _ratio);
		VecTH1D_JZX(const vector<TH1D_JZX*> &_v, TString _nTuple_name, TString _inRootFilePath, bool _ratio, TH1D_JZX * _hNominal);

		VecTH1D_JZX(const VecTH1D_JZX & _v);
		
		////////////////////////////
		// Destructor
		virtual ~VecTH1D_JZX();
		
		////////////////////////////
		// Set instances
		bool Set_nTuple_name(const TString _nTuple_name);
		bool Set_inRootFile_path(const TString _inRootFile_path);
		bool Set_config(const TString _config);
		bool Set_ratio(const bool _ratio);
		bool Set_histNominal(TH1D_JZX * _histNominal);
		bool Set_vectorRatios(vector<TH1D_JZX *> _v, TH1D_JZX * _hNominal);
		
		bool Set_UserGraphic(void (*pf) (TH1D_JZX*));
		bool Set_UserGraphic(void (*pf) (TH1D*));
		
		bool Set_yLogScale(const bool _log);
		bool Set_xLogScale(const bool _log);
		
		//////////////////////////////
		// Get instances
		TString Get_nTuple_name() const;
		TString Get_inRootFile_path() const;
		TString Get_config() const;
		TString Get_subDirRatio() const;
		
		bool Get_ratio() const;
		bool Get_yLogScale() const;
		bool Get_xLogScale() const;
		
		TH1D_JZX * Get_histNominal() const;
		TH1D * Get_histNominalTH1D() const;
		
		vector<TH1D_JZX*> Get_vector() const;
		vector<TH1D_JZX*> Get_vectorRatios() const;
		
		TCanvas *Get_canvas() const;
		
		void Set_yAxisFinalRanges(const vector<TH1D_JZX*> &_v, const bool _logScale);
		
		////////////////////////////////
		// Get user graphic function
		auto Get_UserGraphic() const -> void (*)(TH1D_JZX*);
		
		////////////////////////////////
		// Save modules
		void SaveCanvas(TString _outputPath) const;
		void PrepareOutDIRS(TString _path) const;
		
		////////////////////////////////
		// Plot modules
		void Plot(TString path);
		
		////////////////////////////
		// control modules
		void ShowVector(vector<TH1D_JZX *> _v) const;
};


#endif 

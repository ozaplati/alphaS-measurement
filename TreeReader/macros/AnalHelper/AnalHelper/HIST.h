#pragma once
#include <iostream>
#include "TFile.h"
#include "TString.h"
#include <vector>
#include "TEnv.h"
#include "TH1.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TLegend.h"
#include <TROOT.h>
#include <TStyle.h>
#include <unistd.h>
#include <string> 
#include <sstream> 
#include "TPad.h"
#include<algorithm>

#ifndef HIST_H 
#define HIST_H

using namespace std;


typedef TH1D myHIST_TYPE;

class HIST:
	public myHIST_TYPE
{
	private:
		myHIST_TYPE* m_HIST = 0;
		
		int m_index = 0;
		TString m_key = "";		// key string in config file
		TString m_hist_describe = "";
		TString m_data_describe = "";
		TString m_leg_describe = "";
		
		//////////////////////////////////////
		// Instance aloaded from configFile
		int m_color;
		int m_lineStyle;
		int m_markerStyle;
		
		//bool m_onlyPad1 = true;
		bool m_ratio = false;
		bool m_logScale;
		
	public:
		HIST();
		HIST(myHIST_TYPE*_h, const int _index, const TString _key,  const TString _leg_description, const TString _hist_description, const TString _data_description, const bool _log, const bool _ration);
		
		HIST(myHIST_TYPE* _h, const int _index, const TString _key,  const TString _leg_description, const TString _hist_description, const bool _log = false, const bool _ratio = false);
		HIST(const HIST & _HIST);
		virtual ~HIST();
		
		bool Set_histogram(myHIST_TYPE * _h);
		bool Set_index(const int _i);
		bool Set_key(const TString _key);
		bool Set_hist_description(const TString _str);
		bool Set_data_description(const TString _str);
		bool Set_leg_description(const TString _str);
		
		bool Set_ratio(const bool _ratio);
		bool Set_logScale(const bool _log);
		
		bool Set_color(const int);
		bool Set_lineStyle(const int);
		bool Set_markerStyle(const int);
		
		void Inicialize(myHIST_TYPE * _h, const int _index, const TString _key, const TString _leg_description, const TString _hist_description, const TString _data_description, const bool _log, const bool _ratio);
		void Inicialize(myHIST_TYPE * _h, const int _index, const TString _key, const TString _leg_description, const TString _hist_description, const bool _log, const bool _ratio);
		//void Inicialize(myHIST_TYPE * _h, const int _index, const TString _hist_description);
		myHIST_TYPE * Get_histogram() const;
		
		int Get_index() const;
		TString Get_key() const;
		TString Get_hist_description() const;
		TString Get_data_description() const;
		TString Get_leg_description() const;
		
		bool Get_ratio() const;
		bool Get_logScale() const; 
		
		int Get_color() const;
		int Get_markerStyle() const;
		int Get_lineStyle() const;
		
		void Show() const;
		friend std::ostream & operator<<(std::ostream & os, const HIST & _jzxH);

		void CheckHist() const;
		
		void PrepareToPlotBases(const TString _config);
		void PlotPDF(bool _logScale, TString _leg_name, TString _output);	//TO DO
		
		HIST* Copy(const HIST *_h);
		void ResetTText();
		
		//HIST* HClone();
		
		//friend HIST operator=(const HIST* _h1, const HIST *_h2);
		HIST* operator=(const HIST* _HIST);
		HIST operator=(const HIST &_HIST);

};

#endif 

#pragma once

#ifndef ANALEXCEPTION_H
#define ANALEXCEPTIO_H
#include <exception>
#include <iostream>
#include "TString.h"

class AnalException: public std::exception{
    public:
        AnalException(TString s):message(s){}
        ~AnalException() throw() {}
        void printMessage(){std::cout << message << std::endl;}
    private:
        TString message;
};
#endif

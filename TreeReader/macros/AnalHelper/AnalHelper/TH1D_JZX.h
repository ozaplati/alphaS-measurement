#pragma once
#include <iostream>
#include "TFile.h"
#include "TString.h"
#include <vector>
#include "TEnv.h"
#include "TH1.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TLegend.h"
#include <TROOT.h>
#include <TStyle.h>
#include <unistd.h>
#include <string> 
#include <sstream> 
#include "TPad.h"
#include<algorithm>

#ifndef COMPARE_JZX_HIST_H 
#define COMPARE_JZX_HIST_H

using namespace std;

class TH1D_JZX:
	public TH1D
{
	private:
		TH1D* m_TH1D;
		
		int m_iJZX;
		TString m_JZX_name;
		TString m_JZX_nTuple_name;
		TString m_JZX_rootFile_path;
		TString m_JZX_inRootFile_path;
		
		//////////////////////////////////////
		// Instance aloaded from configFile
		int m_color;
		int m_lineStyle;
		int m_markerStyle;
		
		//bool m_onlyPad1 = true;
		bool m_ratio = false;
		bool m_logScale;
		
	public:
		TH1D_JZX();	//TO DO
		TH1D_JZX(TH1D*_h, const int iJZX, const TString _description, const bool _log, const bool _ration);
 //TO DO
		TH1D_JZX(TH1D* _h, const int _JZX, const TString _nTuple_name, const TString _path, const TString _inRootFile_path, const bool _log = false, const bool _ratio = false);
		TH1D_JZX(const TH1D_JZX & _jzxH);	//TO DO
		virtual ~TH1D_JZX();
		
		bool Set_TH1D(TH1D * _h);
		bool SetIJZX(const int _JZX);
		bool SetJZX_name();
		bool SetRootFile_path(const TString _rootFile_path);
		bool SetJZX_nTuple_name(const TString _nTuple_name);
		bool SetJZX_inRootFile_path(const TString _inRootFile_path);
		//bool Set_onlyPad1(const bool _onlyPad1);
		bool Set_ratio(const bool _ratio);
		bool Set_logScale(const bool _log);
		
		bool Set_color(const int);
		bool Set_lineStyle(const int);
		bool Set_markerStyle(const int);
		
		void Inicialize(TH1D * _h, const int _iJZX, const TString _nTuple_name, const TString _rootFile_path, const TString _inRootFile_path, const bool _log, const bool _ratio);
		void Inicialize(TH1D * _h, const int _iJZX, const TString _description, const bool _log, const bool _ratio);

		TH1D * Get_TH1D() const;
		
		int GetIJZX() const;
		TString GetJZX_name() const;
		TString GetJZX_nTuple_name() const;
		TString GetJZX_rootFile_path() const;
		TString GetJZX_inRootFile_path() const;
		//bool Get_onlyPad1() const;
		bool Get_ratio() const;
		bool Get_logScale() const; 
		
		int Get_color() const;
		int Get_markerStyle() const;
		int Get_lineStyle() const;
		
		void Show() const;
		friend std::ostream & operator<<(std::ostream & os, const TH1D_JZX & _jzxH);

		void CheckHist() const;
		
		void PrepareToPlotBases(const TString _config);
		void PlotPDF(bool _logScale, TString _leg_name, TString _output);	//TO DO
		
		TH1D_JZX* Copy(const TH1D_JZX *_h);
		
		//friend TH1D_JZX operator=(const TH1D_JZX* _h1, const TH1D_JZX *_h2);
		TH1D_JZX* operator=(const TH1D_JZX* _h);
		TH1D_JZX operator=(const TH1D_JZX &_jzxH);

};

#endif 

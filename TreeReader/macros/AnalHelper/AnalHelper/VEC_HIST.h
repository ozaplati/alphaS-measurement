#pragma once

#include "HIST.h"
#include "AnalException.h"

#include <iostream>
#include <math.h>  

#include "TFile.h"
#include "TString.h"
#include <vector>
#include "TEnv.h"
#include "TH1.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TLegend.h"
#include <TROOT.h>
#include <TStyle.h>
#include <unistd.h>
#include <string> 
#include <sstream> 
#include "TPad.h"
#include "TText.h"

#include<algorithm>


#ifndef VEC_HIST_H
#define VEC_HIST_H

using namespace std;
class VEC_HIST
{
	protected:
		
		//////////////////////////////
		// Data containers
		vector<HIST*> v;
		vector<HIST*> v_ratios;
		
		//////////////////////////////
		// nominal histogram
		HIST * m_hNominal = NULL;
		myHIST_TYPE * m_hNominalTH1D = NULL;
		
		TH1D* m_addHistPad1;
		bool m_bAddHistPad1 = false;
		
		//////////////////////////////
		// TObjects
		TCanvas * m_c = new TCanvas;
		TCanvas * m_c_log = new TCanvas;
		TCanvas * m_c_loglog = new TCanvas;

		TPad    * m_pad1 = new TPad;
		TPad    * m_pad2 = new TPad;
		
		//TPad    * m_pad1_log;
		//TPad    * m_pad2_log;
		///
		//TPad    * m_pad1_loglog;
		//TPad    * m_pad2_loglog;
		
		TLegend * m_leg;
		
		TLegend *Get_Legend() const;
		
		//////////////////////////////
		// Log scale setup variable
		bool m_yLogScale;
		bool m_xLogScale;
		
		//////////////////////////////
		// user-graphic function
		void (*m_UserGraphic) (HIST *) = NULL;
		void (*m_UserGraphic_ratio) (HIST *) = NULL;

		
		const vector<TString> m_vDir = { "pdf", "png", "eps"};
		
	private:
		
		//TString m_nTuple_name;
		//TString m_inRootFile_path;
		TString m_envConfig;
		TString m_subDirRatio;
		const TString m_subDir = "";
		
		///NEW
		///
		TString m_describtion = "";
		
		bool m_ratio = false;
		
		//////////////////////////////
		// Instances y-axis - range
		double_t m_yMaxInPad1;
		double_t m_yMinInPad1;
		
		double_t m_yMaxInPad2;
		double_t m_yMinInPad2;
		
		double_t m_padYmax;
		double_t m_padYmin;
		
		double_t m_padYmax_log;
		double_t m_padYmin_log;
		
		double_t m_pad2Ymax;
		double_t m_pad2Ymin;
		
		
		
		/////////////////////////////
		// Set ranges at y-axis
		bool Set_yMaxInPad1(const double_t _yMaxInPad1);
		bool Set_yMinInPad1(const double_t _yMinInPad1);
		
		bool Set_yMaxInPad2(const double_t _yMaxInPad2);
		bool Set_yMinInPad2(const double_t _yMinInPad2);
		
		bool Set_padYmax(const double_t	_padYmax);
		bool Set_padYmin(const double_t _padYmin);
		
		bool Set_pad2Ymax(const double_t _pad2Ymax);
		bool Set_pad2Ymin(const double_t _pad2Ymin);
		
		bool Set_padYmax_log(const double_t _padYmax);
		bool Set_padYmin_log(const double_t _padYmin);

		
		//void Inicialize(const TString _nTuple_name, const TString _inRootFile_path, const bool _ratio);
		//void Inicialize(const TString _nTuple_name, const TString _inRootFile_path, const bool _ratio, HIST * _hNominal);
		
		
		void Inicialize(const TString _description, const bool _ratio);
		void Inicialize(const TString _description, const bool _ratio, HIST * _hNominal);
		
		/////////////////////////////
		// Prepare methods
		void PrepareCanvas();
		void PrepareCanvasLog();
		void PrepareCanvasLogLog();
		
		//void PrepareToGetCanvas(TString logmode);
		
		void PreparePad();
		void PrepareLeg();
		
		/////////////////////////////
		// Check ranges
		double_t Get_yMaxInPad1() const;
		double_t Get_yMinInPad1() const;
		
		double_t Get_yMaxInPad2() const;
		double_t Get_yMinInPad2() const;		

		double_t Get_padYmax() const;
		double_t Get_padYmin() const;
		
		double_t Get_pad2Ymax() const;
		double_t Get_pad2Ymin() const;
		
		double_t Get_padYmax_log() const;
		double_t Get_padYmin_log() const;
		
		void Check_YAxisMaximum_pad1();
		void Check_YAxisMinimum_pad1();
		
		void Check_YAxisMaximum_pad2();
		void Check_YAxisMinimum_pad2();
		
		
		////////////////////////////
		// Plot methods
		void PrepareToPlot();
		void Show()const;
		
	public:
		////////////////////////////
		// Constructors
		VEC_HIST();
		VEC_HIST(const vector<HIST*> &_v);
		VEC_HIST(const vector<HIST*> &_v, TString _description, bool _ratio);
		VEC_HIST(const vector<HIST*> &_v, TString _description, bool _ratio, HIST * _hNominal);

		VEC_HIST(const VEC_HIST & _v);
		
		////////////////////////////
		// Destructor
		virtual ~VEC_HIST();
		
		////////////////////////////
		// Set instances
		//bool Set_nTuple_name(const TString _nTuple_name);
		//bool Set_inRootFile_path(const TString _inRootFile_path);
		bool Set_describtion(const TString _describtion);
		bool Set_config(const TString _config);
		bool Set_ratio(const bool _ratio);
		bool Set_histNominal(HIST * _histNominal);
		bool Set_vectorRatios(vector<HIST *> _v, HIST * _hNominal);
		
		bool Set_UserGraphic(void (*pf) (HIST*));
		bool Set_UserGraphic_ratio(void (*pf) (HIST*));

		bool Set_UserGraphic(void (*pf) (myHIST_TYPE*));
		bool Set_UserGraphic_ratio(void (*pf) (myHIST_TYPE*));
		
		bool Set_yLogScale(const bool _log);
		bool Set_xLogScale(const bool _log);
		
		//////////////////////////////
		// Get instances
		//TString Get_nTuple_name() const;
		//TString Get_inRootFile_path() const;
		TString Get_description() const;
		TString Get_config() const;
		TString Get_subDirRatio() const;
		
		bool Get_ratio() const;
		bool Get_yLogScale() const;
		bool Get_xLogScale() const;
		
		HIST * Get_histNominal() const;
		myHIST_TYPE * Get_histNominalTH1D() const;
		
		vector<HIST*> Get_vector() const;
		vector<HIST*> Get_vectorRatios() const;
		
		TCanvas *Get_canvas();
		TCanvas *Get_canvas_log();
		TCanvas *Get_canvas_loglog();
		
		//TCanvas *Get_canvas(const TString mode);
		
		void Set_yAxisFinalRanges(const bool _logScale);
		
		////////////////////////////////
		// Get user graphic function
		auto Get_UserGraphic() const -> void (*)(HIST*);
		auto Get_UserGraphic_ratio() const -> void (*)(HIST*);
		
		////////////////////////////////
		// Save modules
		void SaveCanvas(TString _outputPath);
		void PrepareOutDIRS(TString _path) const;
		
		////////////////////////////////
		// Plot modules
		void Plot(TString path);
		void PlotTText();
		
		////////////////////////////
		// control modules
		void ShowVector(vector<HIST *> _v) const;
		
		////////////////////////////
		// RESER RATIO HISTORAMS
		void ResetRatia(vector<HIST*> _v);
		
		void CreateAddHist();
};


#endif 

#pragma once

#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H
#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>


 #include <iostream>
 #include <fstream>
 #include <sstream>
 #include <iterator>     // std::iterator
 
 #include <vector>
 #include <string> 
 
 #include "TFile.h"
 #include "TTree.h"
 #include "TCanvas.h"
 #include "TText.h"
 #include "TLatex.h"
 #include "TPaveText.h"
 #include "TPaveLabel.h"
 #include "TPad.h"
 #include "TFrame.h"
 #include "TH1F.h"
 #include "TBenchmark.h"
 #include "TRandom.h"
 #include "TSystem.h"
 
#include "MultiCanvas.h"
//#include "../root/MultiCanvas.cxx" 

#include "HIST.h"
//#include "../root/HIST.cxx" 


#include "VEC_HIST.h"
//#include "../root/VEC_HIST.cxx"

#include "AnalException.h"



void weight_pyAMY(HIST* _h, TString _pyAMY_config)
{
	//HIST * h;
	//h->Copy(_h);

	TEnv * config = new TEnv(_pyAMY_config);
	TString JZX = "JZ";
	JZX += std::to_string(_h->Get_index());
	Double_t mcevt_weight = 1.0;
	Double_t luminosity = 147 * 10E-15; // 147 fb^-1
	Double_t  crossSection = config->GetValue(JZX + "_crossScetion", 1.0);
	Double_t  eff = config->GetValue(JZX + "_genFiltEff", 1.0);
	Double_t  nEvents = 10000;
	
	float total_weight = mcevt_weight * eff * crossSection * luminosity / nEvents;
	cout << "file: " << _pyAMY_config << endl;
	cout << "JZX + _crossScetion: " << JZX + "_crossScetion" << endl;
	cout << "JZX + _genFiltEff: " << JZX + "_genFiltEff" << endl;
	
	cout << "weight_pyAMY: " << endl;
	cout << "crossSction: " << crossSection << endl;
	cout << "eff: " << eff << endl;
	cout << "nEvents: " << nEvents << endl;
	cout << "total_weight: " << total_weight << endl;
	_h->Scale(total_weight);
	//return(h);

	
}


//////////////////////////////////////////////////
// 		LOAD CONFIGURATION FROM CONFIG FILE 	//
//////////////////////////////////////////////////

TString load_fromConfig(TString _key, TString _configFile)
{
	/****************************************************************
	* Function load _key variable from the config-file _configFile	*
	* using TEnv class												*
	* TString _key													*
	* TString _configFile											*
	* return TString												*
	****************************************************************/
	 
	TEnv *config = new TEnv(_configFile);
	TString s = config->GetValue(_key, "none");
	return(s);
}




vector<TString> load_ROOT_FileNames(TString _configFile)
{
	/************************************************************************************************
	 * function load paths to ROOT-Files using special key from the configuration-file _configFile	*
	 * key TString has in this case look like JZX_rootFile, 										*
	 * where X is intoger number 0,1,2,....,12														*
	 * all paths are return as vector of TStrings:  vector<TString>									*
	 ************************************************************************************************/ 
	
	vector<TString> v;
	TString key,s;
	int i_max = 5;
	for (int i = 0; i< i_max; i++)
	{
		if (i == 3) continue;
		if (i == 2) continue;
		if (i == 1) continue;


		key = "JZ"; key += std::to_string(i); key += "_rootFile";
		s = load_fromConfig(key, _configFile);
		///DEBUG
		//cout << "load_ROOT_FileNames - JZ" << i << ": " << s << endl;
		//cout << "key " << key << endl;  
		//cout << "_configFile: " << _configFile << endl;
		v.push_back(s);
	}
	return(v);
}



//////////////////////////////////////
//////////////////////////////////////

vector<TString> SplitMultiString(TString input, TString splitString)
{
	/****************************************************************************
	 * HelpFunction - split long string to multiple smaller substrings			*
	 *																			*
	 * substrings are divided in two if is it contans the splitString as a key	*
	 * in my case, it is used something like this:								*
	 * "[1,2,3,4][5,6,7][8,9,10]" as input										*
	 * using splitingString = "][" and removing the remaing "[", "]"			*
	 * you can get "1,2,3,4"		"5,6,7"		"8,9,10"						*
	 ****************************************************************************/
	
	int dim = input.CountChar('[');
	
	vector<TString> vStr;
	TString s_temp1 = "";
	TString s_temp = input;
	input.ReplaceAll("\n", "");
	int index = 0;
	while (s_temp.Contains(splitString))
	{
		int ind = s_temp.Index(splitString);
		s_temp.Replace(ind, 2, "");
		int ind_last = s_temp.Length();
		s_temp1 = s_temp(0, ind);
		s_temp1.ReplaceAll("[", "");
		s_temp1.ReplaceAll("]", "");
		
		vStr.push_back(s_temp1);
		
		s_temp.Replace(0, ind, "");
	
	}
	s_temp.ReplaceAll("[", "");
	s_temp.ReplaceAll("]", "");
	
	vStr.push_back(s_temp);
	
	return(vStr);
}




vector<int> convertStrtoArr(TString str) 
{
	/****************************************************************************
	 * Fucntion to convert a string to vector of integers 						*
	 * the input string have to contains integer number separeted by comma ","	*
	 * 																			*
	 * This funcion is written to use in sequence with above  mentioned			*
	 * SplitMultiString function												*
	 ****************************************************************************/
	
	/// get length of string str 
	str.ReplaceAll(" ", "");
	str.ReplaceAll("[", "");
	str.ReplaceAll("]", "");
	str.Insert(0, ",");
	
	int str_length = str.Length(); 
	
	/// create an array with appropriate size 
	/// size is equel to number of "," + 1 
	int dim_arr = str.CountChar(',') - 1 ;
	
	vector<int> v_arr;
	
	int i_array = 0;						// index of array
	/// DEBUG PRINT
	/// cout << "str: " << str << endl;
	/// cout << "\n\n str: " << str << endl;
	/// cout << "dim_arr: " << dim_arr << endl;
	
	/// for over all char
	for (int i = str.Length() - 1; i >= 0; i = i-1)
	{
		
		char s = str[i];
		int exponent = 0;
		int arr_item = 0;
		if (s == '[') cout << "\n\nHERE WE ARE\n\n" << endl;
		
		if (s == '\0')
		{
			continue;
		}
		else if (s == ','){
			continue;
		}
		else if (s == ' ')
		{
			continue;
		}
		else if (isdigit(s))
		{
			char s_digit = s;    
			while (s_digit != ',' && s_digit != '\0')
			{
				int digit = int(s_digit)-int('0');
				
				arr_item += digit * pow(10, exponent); 
				
				if (i != 0)
				{
					exponent += 1;
					i-=1;
					s_digit = str[i];
				}
				else
				{
					break;
				}
				
				if (s_digit == ',')
				{
					break;
					cout << "break" << endl;
				}
			}
			
			v_arr.push_back(arr_item);
			
		}
	//cout << "end of one inerationm in loop" << endl;
	} 
	cout << "\n\n\n: ALTERNATIVNI MODE" << endl;
	for (int j = 0; j < v_arr.size(); j++)
	{
		cout << "arr[" << j << "]: " << v_arr.at(j) << endl;
		cout << "next print" << endl;
		cout << "size: " << v_arr.size() << "\n\n" << endl;
	}
	cout << "convertStrtoArr - end of function" << endl;
	return(v_arr);
} 




bool sortFunctionLowToUp(HIST* h_i, HIST* h_j) 
{ 
	/********************************************************
	 * Compare histograms HIST with respect to their index 	*
	 * 														*
	 * Useful for std::sort() method for vector<HIST*>		*
	 ********************************************************/
	 
	return (h_i->Get_index() < h_j->Get_index()); 
} 



////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
TString prepare_lines(TString s)
{
	/**********************************************************************************
	 * Heplful fucnction - oredering of a given string to locks of curly brackets.    *
	 * block begins at the begining of the string and ends at the white space:        *
	 * eq "Hallo Word"-> {Hallo}{Word}                                                *
	 **********************************************************************************/
	TString s_return = s;
	int counter = 0;
	while(s_return.Contains(" "))
	{
		int ind_space = s_return.First(" ");
		if (counter == 0) s_return.Insert(0, "{");
		else if (ind_space != -1)
		{
			s_return.Replace(ind_space, 1, "}{");
		}
		counter += 1;
	}
	s_return.Append("}");
	return(s_return);
}




////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

int GetNumberOflines(TString fileName)
{
	/******************************************************
	 * Function return number of lines of a given file    *
	 * something like wc program in bash terminal         *
	 * ***************************************************/
	
	string s;
	int sTotal=0;
	
	ifstream in;
	in.open(fileName);
	
	cout << fileName << endl;
	while(!in.eof()) {
		getline(in, s);
		sTotal ++;	
	}
	cout << sTotal;
	
	in.close();	
	return(sTotal);
}


///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
void coutConfigEnv(int ind){
	/********************************************************************
	 * Print function - check config file with graphic of histograms	*
	 * histograms are associated with index (int ind) 					*
	 * index is usually used for varius index of weights (0,1,...26)	*
	 ********************************************************************/
	 
	TString ind_str =  std::to_string(ind);
	cout << "\n\n" << endl;
	cout << "weight_" << ind_str << "_color: " 		<< ind_str << endl;
	cout << "weight_" << ind_str << "_marker: " 	<< ind_str << endl;
	cout << "weight_" << ind_str << "_linestyle: " 	<< ind_str << endl; 
}


void prepareConfigEnvFile(){
	/*********************************************************************
	 * Prepare configuration file with graphic - cout method in terminal *
	 *********************************************************************/
	for (int i =0 ; i<28;  i++) coutConfigEnv(i);
}



TString* GetArrOfLinesInFile(TString fileName)
{
	/***********************************************************************************
	 * Function return the array of TString*,                                          *
	 * each value in the array contains string at appropriate line in the given file   *
	 ***********************************************************************************/
	
	/// ///////////////////////////////////////
	/// Get number of lines in given file
	int dim = GetNumberOflines(fileName);
	cout << "dim: " << dim << endl;
	
	TString *arr_return = new TString[dim];
	
	/// ////////////////////////////////////////
	/// load the file
	ifstream inputFile(fileName);
    string line;

	/// ///////////////////////////////////////
	/// loop over lines
	int counter = 0;   
	for( std::string line; getline( inputFile, line ); )
	{
		arr_return[counter] = line;
		cout << "arr_return[" << counter << "]: " << arr_return[counter] << endl;
		counter +=1;
	}
	return(arr_return);
}





/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
TCanvas *GetCanvas(HIST * h, int id, TString title, TString legName)
{
	/************************************************************************
	 * Prepare canvas to save                                               * 
	 *		HIST* h ... histogram                                           *
	 *		int id ... index associated with an appropriate weight          *
	 *		TString title ... describtion of data - draw as TText in canvas *
	 *		TString legName ... name of the histogram in the legend         *
	 ************************************************************************/
	 
	TString name = "c_GetCanvas";
	name  += std::to_string(id);
	
	TCanvas *c = new TCanvas(name,"This is the pt canvas with ith weight",200,10,700,500);
	
	h->SetFillColor(kBlue-3);
	h->GetXaxis()->SetTitle("leading jet pt [GeV]");
	h->GetYaxis()->SetTitle("dN/dpt");
	h->Draw();

	//gPad->SetLogy(1);
	gPad->Update();
	gSystem->ProcessEvents();
		
	TLegend *leg  = new TLegend (0.5,0.6,0.93,0.8);
	leg->SetFillColor(0);
	leg->SetFillStyle(0);
	leg->SetBorderSize(0); 
	leg->SetTextSize(0.025);
	title.ReplaceAll("#", " #");
	title.ReplaceAll(",\\}", "} ");
	cout << "title in canvas: " << title << endl;
	
	float max = h->GetMaximum();
	float min = h->GetMinimum();
	float delta_text = 0;

	//if(gPad->GetLogy() == false)	
	delta_text = abs(max - min)/10;
	//else
	//{
	//	delta_text = abs(max - min)/10
	//}
	
	float y_position;
	int ind=0;
	TString text;
	while(title.Contains("{"))
	{
		ind +=1;
		y_position = max - ind * delta_text;
		text = title(title.Index("{") + 1, title.Index("}") -1);
		title.ReplaceAll("{" + text + "}", "");
		
		cout << "title: " << title <<endl;
		cout << "text: " << text << endl;
		TText *t = new TText(100, y_position, text);
		t->SetTextAlign(11); 
		t->SetTextSize(0.04);
		t->Draw();
		cout << "ind in canvas: " << id << endl;
	}
	
	leg->AddEntry(h, legName, "pf");
	leg->Draw();
	
	c->Modified();
	c->Update();
	return(c);
	gSystem->ProcessEvents();
}
 
HIST* FindHIST_with_index(vector<HIST*> _v, int _i)
{
	for(auto h: _v)
	{
		if(h->Get_index() == _i)
		{
			return(h);
		}
		else
		{
			continue;
		}
	}
}

HIST* Copy(const HIST * _h)
{
 
	TH1D* h = (TH1D *)_h->Get_histogram()->Clone();
	int n = 10;
	double val, err;
	double val_n, err_n;
	for(int i =1; i < _h->GetNbinsX(); i++)
	{
		cout << "TH1D i: " << i << endl;
		val = _h->GetBinContent(i);
		err = _h->GetBinError(i);

		h->SetBinContent(i, val);
		h->SetBinError(i, err);
	}
	
	cout << "HIST*" << endl;
	HIST * hist_copy = new HIST(h, _h->Get_index(), _h->Get_key(), _h->Get_leg_description(), _h->Get_data_description(), _h->Get_logScale(), _h->Get_ratio() );
/*
	cout << "vector 5th bin value: " << _v.at(i)->GetBinContent(2) << endl;
	cout << "hist 5th bin value: " << h->GetBinContent(2) << endl;
	cout << "h_nominal 5th bin value: "  << _hNominal->GetBinContent(2) << endl;
	cout << "h_ratio 5th bin value: " << h_ratio->GetBinContent(2) << endl;
	cout << "5th" << endl;
	cout << "5th" << endl;
	cout << "5th" << endl;
	*/
	// control couts
	// _hNominal->Show();
	// _hNominal->CheckHist();
	// this->ShowVector(_v);	
	// this->ShowVector(this->Get_vectorRatios());
	return(hist_copy);
}

#endif

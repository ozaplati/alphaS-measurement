#ifndef MULTICANVAS_H
#define MULTICANVAS_H

#include <iostream>
#include "HIST.h"
#include "TCanvas.h"

//////////////////////
// MULTI-CANVAS CLASS

class MultiCanvas
	: public TCanvas
{
	private:
		unsigned int m_lines;
		unsigned int m_rows;
		vector<TCanvas *> m_vecCanvas;
		
		TCanvas * m_multiCanvas;
		TString m_outputFile;
		
	protected:
		//////////////////
		// SET METHODS
		//
		
		bool Set_lines(const unsigned int _lines);
		bool Set_rows(const unsigned int _rows);
		bool Set_vecCanvas(const vector<TCanvas * > _vecCanvas);
		bool Set_output(const TString);
		
		//////////////////
		// GET METHODS
		//
		unsigned int Get_lines() const;
		unsigned int Get_rows() const;
		TString Get_outPut() const;
		
		vector<TCanvas *> Get_vecCanvas() const;
		
		
	public:
		MultiCanvas(vector<TCanvas *> _vecCanvas, const unsigned int _lines, const unsigned int _rows, const TString _outPut);
		virtual ~MultiCanvas();
		
		TCanvas * Get_multiCanvas() const;
		void Show() const;
		
};
#endif 

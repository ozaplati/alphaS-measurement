#!/usr/bin/python
import os, sys

pathToROOT = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/treeExample/treeReader"
histNAME = "h_njet"
OUTPUT_PATH = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/TreeReader/plots/";
#pathINFILE = "/";
configFILE = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/TreeReader/config/JZX_compare.env";
         
#command = "./compare_JZX_hist " + pathToROOT + "\t " + histNAME + "\t " + pathINFILE + "\t " + OUTPUT_PATH +  "\t " + configFILE
command = "./TH1D_JZX_test " + pathToROOT + "\t " + histNAME + "\t " + OUTPUT_PATH +  "\t " + configFILE

print("command: \n" + command)
os.system(command)


#!/bin/bash
ROOTFLAGS=$(root-config --cflags)
ROOTLD=$(root-config --libs)

PATH_BUILD="../build"
PATH_ROOT="../root"
PATH_EXE="../executable"
mkdir -p ${PATH_BUILD}
mkdir -p ${PATH_EXE}
###########################
# COMPILE CLASS
c++ -o ${PATH_BUILD}/listOfTFile.o -c ${PATH_ROOT}/listOfTFile.cxx -I  $ROOTFLAGS $FJFLAGS                            # compile my class: TH1D_JZX, VecTH1D_JZX

##################################
# CREATE EXECUTABLE FILE OF MAIN
echo "main was compiled"
c++ -o ${PATH_EXE}/listOfTFile ${PATH_BUILD}/listOfTFile.o $ROOTLD -I  $ROOTFLAGS                                 # create executable file


echo "DONE!"

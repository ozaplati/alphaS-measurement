#!/bin/bash
ROOTFLAGS=$(root-config --cflags)
ROOTLD=$(root-config --libs)

PATH_BUILD="../build"
PATH_ROOT="../root"
PATH_EXE="../executable"
mkdir -p ${PATH_BUILD}
mkdir -p ${PATH_EXE}
###########################
# COMPILE CLASS
c++ -o ${PATH_BUILD}/TH1D_JZX.o -c ${PATH_ROOT}/TH1D_JZX.cxx -I  $ROOTFLAGS $FJFLAGS				# compile my class: TH1D_JZX, VecTH1D_JZX
c++ -o ${PATH_BUILD}/VecTH1D_JZX.o -c ${PATH_ROOT}/VecTH1D_JZX.cxx -I  $ROOTFLAGS $FJFLAGS			# compile my class: TH1D_JZX, VecTH1D_JZX
c++ -o ${PATH_BUILD}/MultiCanvas.o -c ${PATH_ROOT}/MultiCanvas.cxx -I $ROOTFLAGS $FJFLAGS			# compile my class: MultiCanvas

c++ -o ${PATH_BUILD}/HIST.o -c ${PATH_ROOT}/HIST.cxx -I  $ROOTFLAGS $FJFLAGS				# compile my class: TH1D_JZX, VecTH1D_JZX


#c++ -o ${PATH_BUILD}/PyUtils.o -c ${PATH_ROOT}/PyUtils.cpp
###########################
# COMPILE MAIN
echo "class was compiled"
c++ -o ${PATH_BUILD}/test_class.o -c ${PATH_ROOT}/test_class.cxx -I  $ROOTFLAGS $FJFLAGS 			# compile main cxx file

##################################
# CREATE EXECUTABLE FILE OF MAIN
echo "main was compiled"
c++ -o ${PATH_EXE}/test_class ${PATH_BUILD}/test_class.o $ROOTLD -I  $ROOTFLAGS					# create executable file


echo "DONE!"

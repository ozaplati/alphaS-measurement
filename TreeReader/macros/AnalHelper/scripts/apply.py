#!/usr/bin/python
import os, sys

#pathToROOT = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/treeExample/treeReader"
pathToROOT = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/treeExample/treeReader_test/"

OUTPUT_PATH = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/plots/";
#pathINFILE = "/";
configFILE = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/config/JZX_compare.env";
#command = "./compare_JZX_hist " + pathToROOT + "\t " + histNAME + "\t " + pathINFILE + "\t " + OUTPUT_PATH +  "\t " + configFILE


#histNAME = "h_njet"
hNames = [
"h_njets",
#"h_wgt"
]

for hist in hNames:
	command = "./../executable/test_class " + pathToROOT + "\t " + hist + "\t " + OUTPUT_PATH +  "\t " + configFILE
	print("command: \n" + command)
	os.system(command)
print("DONE!")

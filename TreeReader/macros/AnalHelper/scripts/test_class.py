#!/usr/bin/python
import os, sys

#pathToROOT = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/treeExample/treeReader"
pathToROOT = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/treeExample/test/"
histNAME = "h_njet"
OUTPUT_PATH = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/plots/VEC_HIST_TEST";
#pathINFILE = "/";
configFILE = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/config/JZX_compare.env";
         
#command = "./compare_JZX_hist " + pathToROOT + "\t " + histNAME + "\t " + pathINFILE + "\t " + OUTPUT_PATH +  "\t " + configFILE
command = "./../executable/test_class " + pathToROOT + "\t " + histNAME + "\t " + OUTPUT_PATH +  "\t " + configFILE

print("command: \n" + command)
os.system(command)

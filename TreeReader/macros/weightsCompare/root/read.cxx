 #include <iostream>
 #include <fstream>
 #include <sstream>
 
 #include <vector>
 #include <string> 
 
 #include "TFile.h"
 #include "TTree.h"
 #include "TCanvas.h"
 #include "TText.h"
 #include "TLatex.h"
 #include "TPaveText.h"
 #include "TPaveLabel.h"
 #include "TPad.h"
 #include "TFrame.h"
 #include "TH1F.h"
 #include "TBenchmark.h"
 #include "TRandom.h"
 #include "TSystem.h"
 
#include "../../AnalHelper/AnalHelper/MultiCanvas.h"
#include "../../AnalHelper/root/MultiCanvas.cxx" 

#include "../../AnalHelper/AnalHelper/TH1D_JZX.h"
#include "../../AnalHelper/root/TH1D_JZX.cxx" 

#include "../../AnalHelper/AnalHelper/VecTH1D_JZX.h"
#include "../../AnalHelper/root/VecTH1D_JZX.cxx"

#include "../../AnalHelper/AnalHelper/AnalException.h"

 
using namespace std;

TString prepare_lines(TString s)
{
	TString s_return = s;
	/////////////////////////////////
	int counter = 0;
	while(s_return.Contains(" "))
	{
		//if (counter >= 5) break;
		
		int ind_space = s_return.First(" ");
		if (counter == 0) s_return.Insert(0, "{");
		else if (ind_space != -1)
		{
			s_return.Replace(ind_space, 1, "}{");
		}
		counter += 1;
		cout << "s_return_i: " << s_return << endl;
	}
	s_return.Append("}");
	cout << "\ns_return: " << s_return << "\n\n\n" << endl;
	return(s_return);
	/////////////////////////////////
}
int GetNumberOflines(TString fileName)
{
	string s;
	int sTotal=0;
	
	ifstream in;
	in.open(fileName);
	
	//cout << "while" << endl;
	cout << fileName << endl;
	while(!in.eof()) {
		//cout << sTotal << endl;
		getline(in, s);
		sTotal ++;	
		//if(sTotal >= 100) break;
	}
	cout << sTotal;
	
	//int i;
	//cin >> i;
	in.close();	
	return(sTotal);
}
 
TString* GetArrOfWeightNames(TString fileName)
{
	cout << "GetArrWeightNames" << endl;
	int dim = GetNumberOflines(fileName);
	cout << "dim: " << dim << endl;
	TString *weights_names = new TString[dim];
	
	ifstream inputFile(fileName);
    string line;

	int counter = 0;
	cout << "\n\n\nread weights names:" << endl;
   
   for( std::string line; getline( inputFile, line ); )
	{
		weights_names[counter] = line;
		cout << "weights_names[" << counter << "]: " << weights_names[counter] << endl;
		counter +=1;
	}
	return(weights_names);
}
struct ext_TH1D
{
	TH1D * hist;				// histogram
	TString weight_index;	// index of weights
	TString weight_name;	// name of weight
	TString x_label;
	TString y_label;
	TString mc_name;
	TString jzx;
};

void Print_ext_TH1D(ext_TH1D *h)
{
	cout << "hist max: " << h->hist->GetMaximum();
	cout << "weight index" << h->weight_index;
	cout << "weight name" << h->weight_name;
}
 
vector<TH1D*> GetVectorOfHistograms(TString rootFileName, TString branchName)
{
 
	cout << "LOAD TFILE" << endl;
    TFile *f = TFile::Open(rootFileName,"READ");
 
	cout << "LOAD TTREE" << endl;
    TTree *t; 
    f->GetObject("treeAlgo/nominal",t);
 
 
   // Create a new canvas.
    TCanvas *c1 = new TCanvas("c1","Dynamic Filling Example - weights",200,10,700,500);

    const Int_t kUPDATE = 1000;
	cout << "SET UP BRANCHES" << endl;
    std::vector<float> *vec_mcWeights = 0;
    TBranch *br_vec_mcWeights = 0;
    t->SetBranchAddress("mcEventWeights",&vec_mcWeights,&br_vec_mcWeights);
 
    std::vector<float> *vec_jet_pt = 0;
    TBranch *br_vec_jet_pt = 0;
    t->SetBranchAddress(branchName, &vec_jet_pt, &br_vec_jet_pt);
 
 
 
    // Create one histograms
    TH1D *h = new TH1D("h","This is the weights distribution",100,-4,4);
    //TH1F *h_pt = new TH1F("h_pt","This is the pt distribution",100, 0, 1000);

    std::vector<TH1D*> vecHist;
    std::vector<ext_TH1D*> vecExtHist;
    
    int n=27; //vec_mcWeights->size();
    
    //cout << "FOR LOOP CREATE TH1D AND CANVAS" << endl;
    for (int i=0; i<n; i++)
    {
		//cout << "CREATE TH1D: " << i << endl;
		TH1D *h_i = new TH1D("h_i"," ",100, 0, 1000);
		h_i->SetFillColor(kBlue-3);
		vecHist.push_back(h_i);
		
		ext_TH1D * h_ext = new ext_TH1D;
		vecExtHist.push_back(h_ext);
	}
	
	//cout << "VECHIST.SIZE(): " << vecHist.size() << endl;

    h->SetFillColor(48);
 
	//cout << "LOOP OVER EVENTS" << endl;
    float weight = 1.0;
    float leadingJetPt = 0.0;
    for (Int_t i = 0; i < t->GetEntries(); i++) {
       //if (i >= 1000) break;
       //cout << "\t EVENT: " << i << endl;
       
       Long64_t tentry = t->LoadTree(i);
       
       br_vec_mcWeights->GetEntry(tentry);
       br_vec_jet_pt->GetEntry(tentry);
       
       //sort vector of jet pt 
       //cout << "sort" << endl;
       std::sort(vec_jet_pt->begin(), vec_jet_pt->end(), greater<float>()); 
       //for(int iii = 0; iii < vec_jet_pt->size(); iii++)
       //{
		   //cout << "\t pt[" << iii << "]: " << vec_jet_pt->at(iii) << endl;
       //}
       //cout << "\t LOOP OVER WEIHTS" << endl;
       for (UInt_t j = 0; j < vec_mcWeights->size(); ++j) {
		   //cout << "\t\t WEIGHT: " << j << endl;
		   
          weight = vec_mcWeights->at(j);
          //cout << "\t\t FILL WEIHT TH1D" << endl;
          h->Fill(weight);

          if (vec_jet_pt->size() > 0){
			 // cout << "\t\t JET EVENT, JET FOUNED: " << vec_jet_pt->size() << endl; 
			  leadingJetPt = vec_jet_pt->at(0);
 			  //cout << "\t\t FILL LEADING JET PT:  " << leadingJetPt << endl; 
			  //cout << "\t\t SIZE OF vecHist: " << vecHist.size() << endl;
              TH1D * hh = vecHist.at(j);
			  hh->Fill(leadingJetPt, weight);
          }
          
       }
        
        
    }
    //cout << "event loop done" << endl;
    return(vecHist);
 }
 
TCanvas *GetCanvas(TH1D * h, int id, TString title, TString legName)
{
	TString name = "c_GetCanvas";
	name  += std::to_string(id);
	
		TCanvas *c = new TCanvas(name,"This is the pt canvas with ith weight",200,10,700,500);
		
		h->SetFillColor(kBlue-3);
		h->GetXaxis()->SetTitle("leading jet pt [GeV]");
		h->GetYaxis()->SetTitle("dN/dpt");
		h->Draw();
		//gPad->SetLogy(1);
		gPad->Update();
		gSystem->ProcessEvents();
		
		TLegend *leg  = new TLegend (0.5,0.6,0.93,0.8);
		leg->SetFillColor(0);
		leg->SetFillStyle(0);
		leg->SetBorderSize(0); 
		leg->SetTextSize(0.025);
		title.ReplaceAll("#", " #");
		title.ReplaceAll(",\\}", "} ");
		cout << "title in canvas: " << title << endl;
		
		float max = h->GetMaximum();
		float min = h->GetMinimum();
		float delta_text = 0;
		//if(gPad->GetLogy() == false)	
		delta_text = abs(max - min)/10;
		//else
		//{
		//	delta_text = abs(max - min)/10
		//}
		
		float y_position;
		int ind=0;
		TString text;
		while(title.Contains("{"))
		{
			ind +=1;
			y_position = max - ind * delta_text;
			text = title(title.Index("{") + 1, title.Index("}") -1);
			title.ReplaceAll("{" + text + "}", "");
			
			cout << "title: " << title <<endl;
			cout << "text: " << text << endl;
			TText *t = new TText(100, y_position, text);
			t->SetTextAlign(11); 
			t->SetTextSize(0.04);
			t->Draw();
		}
		
		leg->AddEntry(h, legName, "pf");
		leg->Draw();
		
		c->Modified();
		c->Update();
		return(c);
		gSystem->ProcessEvents();
}
 

int main(int nArg, char **arg) {
	try{
		if (nArg!=4){
		cout << "Error: Wrong number of input parameters!" << endl;
		cout << nArg << " were used" << endl;
		throw AnalException("Error: Wrong number of input parameters!"); 
		}
	// SET UP
	TString rootFileName = arg[1];	//"/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/scripts/JETM1_test_1305_prepare_to_grid/data-tree/mc16_13TeV.root";
	TString branchName = arg[2];	//"jet_pt";
	TString data_name = arg[3];		//"#splitline{Pythia8, A14NNPDF23LO,}{WithSW, JETM1}";


	// GET DESCRIPTION OF WEIGHTS
	TString *arr_WeightsName = GetArrOfWeightNames("../config/weights_name.txt");
	
	vector<TH1D *> vHistograms = GetVectorOfHistograms(rootFileName, branchName);
	cout << "In main" << endl;

	vector<TCanvas *> vCanvases; 
	
	int ind_h=0;
	cout << "before auto loop" << endl;
	for (auto h: vHistograms)
	{
		cout << "ind_h: " << ind_h << endl;
		
		TString title = "JZX:i ";
		 
		title += "weight-id:";
		title += std::to_string(ind_h);

		title += " ";

		title += arr_WeightsName[ind_h];
		title.ReplaceAll("_", " ");

		title = prepare_lines(title);
		title.ReplaceAll("-", " ");
		
		TCanvas * canvas = new TCanvas;
		
		canvas = GetCanvas(h, ind_h, title, data_name);
		
		TString toSave = "saveAs";
		toSave += std::to_string(ind_h);
		toSave += ".pdf";
		canvas->SaveAs(toSave);
		vCanvases.push_back(canvas);
		cout << "after Get Canvas" << endl;
		ind_h +=1;
	}
	
	cout << "BEFORE MULTICANVAS" << endl;
	
	TString pdf_name = "leadingJetPt_multiCanvas.pdf";
	
	
	MultiCanvas myMultiCanvas(vCanvases, 6,5, pdf_name);
	myMultiCanvas.Show();
	
	cout << "\n\nTHE END" << endl;
	
	return(0);
	}
	catch(AnalException l){
		l.printMessage();
		cout << "Exiting now!" << endl;
		return (-1);
	}
}

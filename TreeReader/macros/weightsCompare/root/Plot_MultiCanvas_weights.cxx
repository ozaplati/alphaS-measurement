#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>


 #include <iostream>
 #include <fstream>
 #include <sstream>
 #include <iterator>     // std::iterator
 
 #include <vector>
 #include <string> 
 
 #include "TFile.h"
 #include "TTree.h"
 #include "TCanvas.h"
 #include "TText.h"
 #include "TLatex.h"
 #include "TPaveText.h"
 #include "TPaveLabel.h"
 #include "TPad.h"
 #include "TFrame.h"
 #include "TH1F.h"
 #include "TBenchmark.h"
 #include "TRandom.h"
 #include "TSystem.h"
 
#include "../../AnalHelper/AnalHelper/MultiCanvas.h"
#include "../../AnalHelper/root/MultiCanvas.cxx" 


#include "../../AnalHelper/AnalHelper/HIST.h"
#include "../../AnalHelper/root/HIST.cxx" 


#include "../../AnalHelper/AnalHelper/VEC_HIST.h"
#include "../../AnalHelper/root/VEC_HIST.cxx"


#include "../../AnalHelper/AnalHelper/AnalException.h"
#include "../../AnalHelper/AnalHelper/HelpFunctions.h"

//#include "../../AnalHelper/AnalHelper/PyUtils.h"

 
using namespace std;




//////////////////////////////////////////////
//////////////////////////////////////////////
//////////////////////////////////////////////
void PrepareToPlotTest(HIST * h)
{
	/****************************************************************************************
	 * Prepare tu plot fucntion - the function is written as an input for VEC_HIST class	*
	 * 																						*
	 * this is only for pad1 histogram														*
	 ****************************************************************************************/
	 
	/// ///////////////////////////////////////
	/// READ SETUP FROM ENV FILE
	//cout << "PREPARE TO PLOT FUNCTION" << endl;
	const TString _config = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/config/weights.env";
	TEnv *config = new TEnv(_config);

	int color = 1, markerstyle = 1, linestyle = 1;
	TString strColor, strMarkerStyle, strLineStyle;

	/// ///////////////////////////////////////
	/// stringstream to conver string to int
	TString key_str = h->Get_key();

	stringstream geek_c(config->GetValue(key_str + "_color", ""));
	geek_c >> color;

    stringstream geek_m(config->GetValue(key_str + "_marker", ""));
    geek_m >> markerstyle;
  
    stringstream geek_l(config->GetValue(key_str + "_linestyle", ""));
    geek_l >> linestyle;
  
	/// DEBUG PRINTS
	///cout << "key_str: " << key_str << endl;
	///cout << "color: " << color << endl;
	///cout << "strMarkerStyle: " << markerstyle << endl;
	///cout << "strLineStyle: " << linestyle << endl;
	///cout << "/////////////" << endl;
	 
    /// ///////////////////////////////////////
    /// APPLY THE SETUP
    //h->UseCurrentStyle(); // important for imported histograms
    h->SetLineColor(color);
    h->SetMarkerColor(color);
    h->SetMarkerSize(1.2);
    h->SetMarkerStyle(markerstyle);
    h->SetLineStyle(linestyle);
    
    //else if(nameSmaple.Contains("301322")) {fillStyle=3000; lineColor=kMagenta+3; fillColor=kMagenta+2;}
	//h->SetFillColor(kGreen+2);
	//->SetLineColor(kGreen+3);
	h->SetLineWidth(2); 
	h->SetLineStyle(1);
	h->SetFillStyle(3000);
	
	/// ///////////////////////////////////////
	/// Set-up TITLES
	h->GetXaxis()->SetTitle("inclusive jet pt [GeV]");
	h->GetYaxis()->SetTitle("dN/dpt");
	
	// No normalization:
	//h->SetMinimum(1E-16);
	//h->SetMaximum(1E16);
	
	// with normaliaztion
	h->SetMinimum(1E-30);
	h->SetMaximum(1E-6);
}
 
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void PrepareToPlotRatioTest(HIST * h)
{
	/****************************************************************************************
	 * Prepare tu plot fucntion - the function is written as an input for VEC_HIST class	*
	 * 																						*
	 * this is only for pad2 ratio histogram												*
	 ****************************************************************************************/
	 
	h->SetMinimum(0.01);
	h->SetMaximum(2.01);
	h->GetYaxis()->SetTitle("weight[i]/weight[0]");
	//h->GetYaxis()->SetTitle("dN/dpt");
}



////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

//std::multimap<int, HIST*> GetMapOfHistograms(TString rootFileName, TString branchName)
vector<HIST*> GetVecOfHistograms(TString slice, TString branchName)
{
	/**********************************************************************************
	 * Return multimap of considered histograms with appropriate indexes.             *
	 * Int index is associated with index of Monte-Carlo weight in TTree              *
	 *                                                                                *
	 * TString rootFileName .... path to root-file                                    *
	 * TString branchName   .... associated branch with variable of interest in TTree *
	 * TString slice        .... slice e.q. JZ0, JZ1, ... JZ12                        *
	 *                                                                                *
	 * currently there are returnt kinematic distributions of leading jets            *
	 *********************************************************************************/
	
	/// //////////////////
	/// Load root-file
	
	TEnv *config_rooFile = new TEnv("/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/macros/weightsCompare/config/config.txt");
	TString key_rootFile = slice + "_rootFile";
	TString rootFileName = config_rooFile->GetValue(key_rootFile, "");
	TFile *f = TFile::Open(rootFileName,"READ");
	
	cout << "slice: " << slice << endl;
	cout << "key_rootFile: " << key_rootFile << endl;
	cout << "rootFileName: " << rootFileName << endl;
	//int zz; cin >> zz;
	
	/// //////////////////
	/// Load tree
	TTree *t; 
	f->GetObject("treeAlgo/nominal",t);
	
	/// ////////////////////////////////////////////////////////////////
	/// Connect brances with working variables - Monte-Carlo weights
	std::vector<float> *vec_mcWeights = 0;
	TBranch *br_vec_mcWeights = 0;
	t->SetBranchAddress("mcEventWeights",&vec_mcWeights,&br_vec_mcWeights);
	
	/// ////////////////////////////////////////////////////////////////
	/// Connect brances with working variables - jet pt
	std::vector<float> *vec_jet_pt = 0;
	TBranch *br_vec_jet_pt = 0;
	t->SetBranchAddress(branchName, &vec_jet_pt, &br_vec_jet_pt);
	
	/// //////////////////////////
	/// Create one histograms  ///
	/// //////////////////////////
	
	/// ////////////////////////////////////
	/// Inicialize multimap of histogram
	Long64_t tent = t->LoadTree(1);
	br_vec_mcWeights->GetEntry(tent);
	int n = vec_mcWeights->size(); //27;
	
	
	//std::multimap<int, HIST*> mapHist;
	vector<HIST*> vecHIST;
	for (int i = 0; i < n; i++)
	{
		//TH1D *h_i = new TH1D("h_i"," ", 3, 50, 150);
		float x_min = 39;
		float x_max = 25;
		int n_bins = 1000;
		/*
		if (slice == "JZ1"){x_min = 25; x_max = 500; n_bins = 20}
		else if (slice == "JZ1"){x_min = 25; x_max = 500; n_bins = 20}
		else if (slice == "JZ1"){x_min = 25; x_max = 500; n_bins = 20}
		else if (slice == "JZ1"){x_min = 25; x_max = 500; n_bins = 20}
		else if (slice == "JZ1"){x_min = 25; x_max = 500; n_bins = 20}
		*/
		//TH1D *h_i = new TH1D("h_i"," ", 15, 25, 400);
		
		TH1D *h_i = new TH1D("h_i"," ", 15, 75, 2000);
		h_i->SetFillColor(kBlue-3);
		
		TString key = "weight_";
		key += std::to_string(i);
		bool log = true;
		bool ratio = true;
		HIST *hh_i = new HIST(h_i, i, key, slice, "data describton", log, ratio);
		
		//PrepareToPlotTest(hh_i);
		//mapHist.insert(std::pair<int, HIST*>(i, hh_i));
		vecHIST.push_back(hh_i);
	}
	/// ///////////////////////////////////////////
	/// Sort vector of histogram according index
	/// function sortFunctionLowToUp is defined in AnalHelper/AnalHelper/HelpFunction.h 
	sort(vecHIST.begin(), vecHIST.end(), sortFunctionLowToUp); 
	
	for(auto h : vecHIST) cout << "\tsorted indexes: " << h->Get_index() << endl;
	
	/// ///////////////////////
	/// LOOP OVER ENVENTS
	float weight = 1.0;
	float jetPt = 0.0;
	for (Int_t i = 0; i < t->GetEntries(); i++) {
		if (i >= 10000) break;
		
		Long64_t tentry = t->LoadTree(i);
		
		br_vec_mcWeights->GetEntry(tentry);
		br_vec_jet_pt->GetEntry(tentry);
		
		/// sort vector of jet pt 
		std::sort(vec_jet_pt->begin(), vec_jet_pt->end(), greater<float>()); 
		
		/// DEBUG PRINT
		/// cout << "std::sort() applied" << endl;
		/// for(int iii = 0; iii < vec_jet_pt->size(); iii++)
		/// {
		/// 	cout << "\t pt[" << iii << "]: " << vec_jet_pt->at(iii) << endl;
		/// }
		///cout << "\t LOOP OVER WEIHTS" << endl;
		
		/// //////////////////////////
		/// LOOP OVER ALL WEIGHTS
		for (UInt_t j = 0; j < vec_mcWeights->size(); ++j) { 
			//if (j>= n)continue;
			
			weight = vec_mcWeights->at(j);
			//h->Fill(weight);
			
			/// I am interestend only with jet events
			if (vec_jet_pt->size() > 0){
				//cout << "\t\t JET EVENT, JET FOUNED: " << vec_jet_pt->size() << endl; 
				/// Get pt of leading jet
				for (int i_jet = 0; i_jet < vec_jet_pt->size(); i_jet++)
				{
					jetPt = vec_jet_pt->at(i_jet);
					
					/// DEBUG PRINT
					/// cout << "\t\t\t FILL JET PT:  " << jetPt << endl; 
					/// cout << "\t\t SIZE OF vecHist: " << mapHist.size() << endl;
					/// cout << "\t\t\t SIZE OF vecHIST: " << vecHIST.size() << endl;
					
					/// ///////////////////////////////////////////////////////////////
					/// Fill pt of leading jet to histogram with appropriate weight
					HIST * hh = vecHIST.at(j);
					hh->Fill(jetPt, weight);
				}
			}
		}
	}
	return(vecHIST);
}
 

int main(int nArg, char **arg) {
	try
	{
		/// /////////////////////////////
		/// CHECK number of arguments
		if (nArg != 5){
		cout << "Error: Wrong number of input parameters!" << endl;
		cout << nArg << " parameters were used" << endl;
		
		/// /////////////////////////////
		/// PRINT all input arguments
		for (int i = 0; i<nArg; i++)
		{
			cout << "arg[" << i << "]: " << arg[i] << endl;
		}
		
		/// ////////////////////
		/// Call an exception
		throw AnalException("Error: Wrong number of input parameters!"); 
		}
		
		/// ///////////////////
		/// SET UP BLOCK
		
		//vector<int> vec_rootFileName = listTupleToVector_Int(arg[1]);
		TString config = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/macros/weightsCompare/config/config.txt";
		cout << arg[1] << endl;
		//int oo; cin >> oo;
		vector<TString> vec_JZX = SplitMultiString(arg[1], "][");//vec_rootFileNames = load_ROOT_FileNames(config);	//SplitMultiString(arg[1], "][");	//"/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/scripts/JETM1_test_1305_prepare_to_grid/data-tree/mc16_13TeV.root";
		//vector<TString> vec_jzx = load_keys(config);
		TString branchName = arg[2];										//"jet_pt";
		TString data_name = arg[3];											//"#splitline{Pythia8, A14NNPDF23LO,}{WithSW, JETM1}";
//vector<TString> vec_jzx = vec_JZX; //SplitMultiString(arg[4], "][");
		TString mode = arg[4];
		
		mode.ToUpper();
		
		TString OUTPUT_PATH = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/plots/TEST";
		
		vector<vector<int>> vMode;		/// includes array of integer, each array is associated with weight id in the canvas, different canvas has different int array
		vector<TString> vTString; 		/// temporary vector, it is used to get vectro if int array
		TString* vModeStr;				/// temporary string
		int num_canvas = 0;				/// number of canvases
		
		/// ////////////////////////////
		/// MODE PROCEDURE
		if (!mode.Contains("ALL"))
		{
			
			/// ////////////////////////////////////////////////
			/// ALTERNATIVE MODE - plot only selected weights

			/// Only chosen weights are considerd
			/// consdider weights are represented by TString 
			/// w.q. mode = "[1,2,3,4][5,6,7][8,9,10,11]"
			///
			/// get array of weights in each canvas
			/// e.q. in 1st canvas will be: 1, 2, 3, 4
			///
			/// first separate weights according canvases
			num_canvas = mode.CountChar('[');
			vModeStr = new TString[num_canvas];
			cout << "Call function SplitMulitiString\n" << endl;
			vector<TString> vSplitMultiStr = SplitMultiString(mode, "][");
			vTString.insert(vTString.end(), vSplitMultiStr.begin(), vSplitMultiStr.end());
			
			// second get int array of weights
			// store it to vector vMode
			for (int i = 0; i< vTString.size(); i++)
			{
				cout << "Call function convertStrtoArr" << endl; 
				vector <int> arr;
				cout << "begore insert" << endl;
				arr = convertStrtoArr(vTString.at(i));
				cout << "converStrArr is OK\n\n" << endl;
				//arr.insert(arr.begin(), convertStrtoArr(vTString.at(i)).begin(), convertStrtoArr(vTString.at(i)).end());
				cout << "push" << endl;
				vMode.push_back(arr);
			}
			
			/// DEBUG
			///for (int i_arr = 0; i_arr < vMode.size(); i_arr++)
			///{
				///int * arr = vMode.back();
				///DEBUG
				///cout << "\t i_arr: " << i_arr << endl;
				///cout << "\t before loop over arr" << endl;
				///cout << "sizeof(array): " << sizeof(arr) << endl;
				///cout << "sizeof(int): " << sizeof(int) << endl;
			///}
		}
		
		/// //////////////////////////////////////
		/// GET VECTOR OF HISTOGRAMS FROM TTREE
		/// 
		/// run Reader for all rootFiles
		/// store histograms to vector<HIST*> for each rootFile
		/// all is stored as vecror < vector<HIST* > >
		std::vector< vector<HIST*> > allReaded;
		for (int iJZX = 0; iJZX < vec_JZX.size(); iJZX++)
		{
			TString JZX = vec_JZX.at(iJZX);
			//String rootFileName = vec_rootFileNames.at(ind_rootFile);
			//TString jzx = vec_jzx.at(ind_rootFile);
			//cout << "rootFilename: " << rootFileName << endl;
			
			//TString slice = "JZ"; slice += jzx;
			cout << "JZX: befor function: " << JZX << endl;
			std::vector<HIST*> vecHist = GetVecOfHistograms(JZX, branchName);
			sort(vecHist.begin(), vecHist.end(), sortFunctionLowToUp);
			
			allReaded.push_back(vecHist);
		
		}
		/// DEBUG
		///for (int i=0; i<allReaded.at(0).size(); i++)
		///{
		///	cout << "Maximum: " << allReaded.at(0).at(i)->GetMaximum() << endl;
		///}
		
		
		/// ////////////////////////
		/// GET JZX DESCRIPTION
		
		TString jzx = "0";
		data_name +="[JZX:";
		data_name += jzx;
		data_name +="]";
		data_name.ReplaceAll("[", "{");
		data_name.ReplaceAll("]", "}");
		
		cout << "data_name: " << data_name << endl;
		
		/// //////////////////////////
		/// SETUP OUTPUT DIRECTORY
		
		TString outputDir = "../pdf";
		system("mkdir -p " + outputDir);
		
		/// /////////////////////////////
		/// GET DESCRIPTION OF WEIGHTS
		
		TString *arr_WeightsName = GetArrOfLinesInFile("../config/weights_name.txt");
		
		/// /////////////////////
		/// SETUP RATIO MODE
		//bool bRatio = true;
		bool bRatio = true;
		
		
		/// /////////////////////////////////////////
		/// ANALHELPER - GET VEC-_HIST from vecHist
		vector<HIST*> vecHist(allReaded.at(0));
		VEC_HIST *myVEC_HIST = new VEC_HIST(vecHist, "Some describtion", bRatio);
		
		
		//myVEC_HIST->Set_histNominal(vecHist.at(0));
		cout << "AFTER VEC_HIST * myCEC_HIST" << endl;
		
		vector<vector<int>> v_id_weights_in_canvas;
		///
		/// ALL MODE
		///
		int n_hist_in_canvas = 0;	// number of histograms in one canvas - considered only for "ALL" mode
		//int num_canvas = 0;
		if(mode.Contains("ALL"))  
		{
			/// ////////////////////////////////////////////////////////
			/// "ALL" mode - Evaluate number of canvases
			/// basicly i want 4 distribution in one canvas
			n_hist_in_canvas = 4;
			if (vecHist.size()%n_hist_in_canvas == 0)
			{
				num_canvas = vecHist.size()/n_hist_in_canvas;
			}
			else
			{
				/// but my number of weights is not divisible by 4
				num_canvas = floor (vecHist.size()/n_hist_in_canvas) + 1;
			}
			
			/// ////////////////////////////////////////////////////////
			/// "ALL" mode - Evaluate vector of weight id array
			for (int i_c = 0; i_c < num_canvas; i_c++)		// loop over numver o canvas
			{
				vector<int> id_arr;
				int dim_id_arr;
				if(i_c < num_canvas -1)
				{
					 dim_id_arr = n_hist_in_canvas;
				}
				else
				{
					dim_id_arr = vecHist.size()%n_hist_in_canvas;
					
				}
				
				for (int i_id = 0; i_id < dim_id_arr; i_id++)
				{
					int id_weight = i_id + n_hist_in_canvas * i_c;
					if (id_weight < vecHist.size()) id_arr.push_back(id_weight);
					
					cout << "ALL MODE: id_arr[" << i_id << "]: " << id_arr.at(i_id) << "\t n_hist_in_canvas: " << n_hist_in_canvas << "\t num_canvas: " << num_canvas << endl;
				}
				cout << "push" << endl;
				v_id_weights_in_canvas.push_back(id_arr);
			}
		}
		else
		{
		///
		/// ALTERNATIVE MODE
		///
			/// IN case of "ALTERNATIVE" mode it can be various number of weights in canvas
			/// evaluet the useging char counter "[" in mode string
			cout << "ALTERNATIVE MODE BLOCK" << endl;
			num_canvas = mode.CountChar('[');
			
			/// copy array of id weights to vecor v_id_weights_in_canvas
			v_id_weights_in_canvas.insert(v_id_weights_in_canvas.begin(), vMode.begin(), vMode.end());
			
			/// DEBUG PRINT
			/// cout << "vMode Print:" << endl;
			/// for (int i=0; i < vMode.size(); i++) cout << "vMode.at(i): " << *vMode.at(i) << endl;
			/// int e;
			/// cin >> e;
			
			///DEBUG
			///cout << "v_id_weights_in_canvas.back()[0]: " << v_id_weights_in_canvas.back()[0] << endl; 
			///cout << "v_id_weights_in_canvas.back()[1]: " << v_id_weights_in_canvas.back()[1] << endl;
		}
		
		
		/// /////////////////////////////
		/// CREATE VECTOR OF CANVASES
		
		vector<TCanvas *> vCanvases(num_canvas, new TCanvas);
		vector<TCanvas *> vCanvases_log(num_canvas, new TCanvas);
		vector<TCanvas *> vCanvases_loglog(num_canvas, new TCanvas);
		
		vector<TCanvas> vCanvasesObj; 
		cout << "CANVAS ARE CREATED" << endl;
		HIST* nominal = new HIST();
		
		
		int i_can = 0;
		int counter = 0;
		cout << "auto loop start" << endl;
		cout << "allReaded: " << allReaded.size() << endl;

		vector <vector<HIST*> > vecHist_all_can;
		vector <vector<HIST*> > vecHistRatios_all_can;
		for (int i=0; i < num_canvas; i++)
		{
			/// pad1 histograms
			vector<HIST*> vecHist_can_i;
			vecHist_all_can.push_back(vecHist_can_i);
			
			/// pad2 histogram - ratia
			vector<HIST*> vecHistRatios_can_i;
			vecHistRatios_all_can.push_back(vecHistRatios_can_i);
		}
		
		/// /////////////////////////////
		/// LOOP OVER ALL ROOTS 
		for (vector<HIST*> vec_OneROOT: allReaded)
		{
			cout << "vec_OneROOT.at(0) maximum: " << vec_OneROOT.at(0)->GetMaximum() << endl; 
			cout << "vec_OneROOT.at(4) maximum: " << vec_OneROOT.at(4)->GetMaximum() << endl; 
			
			/// ///////////////////////////////////////
			/// LOOP OVER ALL HISTOGRAMS IN ONE CANVAS
			for (int iArr=0; iArr < v_id_weights_in_canvas.size(); iArr++)		// loop over arr, arr id a poiter to array, which contains id-weights in appropriate canvas
			{
				vector<int> arr(v_id_weights_in_canvas.at(iArr));
				
				vector<HIST*> vec_allHist_inOneROOT(vec_OneROOT);
				
				vector<HIST*> vecHist_can_i;
				
				//
				// for (int ii=0; ii< vec_allHist_inOneROOT.size(); ii++)
				//{
				//	vector<int> vec_arr(arr);
				//	
				//	if(std::find(vec_arr.begin(), vec_arr.end(), ii) != vec_arr.end())
				//	{
				//		/* v contains x */
				//		cout << "\t\t ind: " << vec_allHist_inOneROOT.at(ii)->Get_index() << endl;
				//		cout << "\t\t legendtitle: " << vec_allHist_inOneROOT.at(ii)->Get_leg_description() << "\n\n" << endl;
				//		
				//	} 
				//	else 
				//	{
				//		/* v does not contain x */
				//		continue;
				//	}
				//}
				
				//int oo;
				//cin >> oo;
				
				/// ///////////////////////////////////////
				/// Get number of weights in one canvas
				///
				/// first for "ALL" mode
				int n=0;
				if (mode.Contains("ALL"))
				{
					cout << "ARR SIZE: " << arr.size() << endl;
					n = arr.size();
				}
				else
				{
					/// ///////////////////////////////////////
					/// Get number of weights in one canvas
					/// second for "ALTERNATIVE" mode
					
					n = arr.size(); //(sizeof(*arr) * sizeof(arr[0]))/sizeof(int);
				}
				
				for (int id = 0; id < n; id++)
				{
					int index = arr[id];
					
					HIST *hh = vec_allHist_inOneROOT.at(index);
					
					int ind_h = hh->Get_index();
					
					TString hist_description, data_description, legend_description;
					
					/// ///////////////////////////
					/// Load data-description
					data_description = data_name;
					data_description = prepare_lines(data_description);
					data_description.ReplaceAll("_", " ");
					
					/// //////////////////////////
					/// Load legend desctiption
					legend_description += hh->Get_leg_description();
					legend_description += " ";
					legend_description += "weight-id:";
					legend_description += std::to_string(hh->Get_index());
					legend_description += " ";
					
					/// key functions for mining of strings
					legend_description += arr_WeightsName[hh->Get_index()];
					legend_description.ReplaceAll("_", " ");
					
					/// key functions for mining of string
					legend_description = prepare_lines(legend_description);
					legend_description.ReplaceAll("-", " ");
					legend_description.ReplaceAll(",\\", "");
					
					cout << "data_descrintion: "<< data_description << endl;
					cout << "legend_descrintion: " << legend_description << endl;
					
					/// //////////////////////////////////
					/// Set up data-desctiption in HIST
					hh->Set_data_description(data_description);  
					
					/// ///////////////////////////////////
					/// Set up legend-description in HIST
					hh->Set_leg_description(legend_description);
					
					/// DEBUG PRINT
					/// cout << "push back hh histogram:" << endl;
					/// cout << "id: " << id << endl;
					/// cout << "Add HIST to VEC_HIST" << endl;
					/// cout << "index: " << index << endl;
					/// cout << "hh->Get_index()" << hh->Get_index() << endl;
					/// int iiii;
					/// cin >> iiii;
					
					vector<int> vec_arr(arr);
					int ii =hh->Get_index();
					/*
					if(std::find(vec_arr.begin(), vec_arr.end(), ii) != vec_arr.end())
					{
						///v contains x
						cout << "\t\t ind: " << vec_allHist_inOneROOT.at(ii)->Get_index() << endl;
						cout << "\t\t legendtitle: " << vec_allHist_inOneROOT.at(ii)->Get_leg_description() << "\n\n" << endl;
						//vecHist_can_i.push_back(hh);
						
						///
						/// WIGHTING USINGCONFIG/pyAMY.txt FILE
						///
						vecHist_all_can.at(iArr).push_back(hh);
						cout << ii << endl << "\n";
						cout << "vecHist_can_i.size(): " << vecHist_all_can.at(0).size() << endl; 
					} 
					else 
					{
						///v does not contain x
						continue;
					}
					*/
					/// //////////////////////////////////////////////
					/// Set up nominal distribution - weight-id = 0
					
					cout << "SETUP NOMINAL HISTOGRAM" << endl;
					//int tttt; cin >> tttt;

						nominal = FindHIST_with_index(vec_allHist_inOneROOT, 0);
					//if (hh->Get_index() == 0)
					//{
					//	cout << "NOMINAL hh WAS FOUND" << endl;
					//}
					//else
					//{
						HIST *ratio;
						cout << "ratio->Copy(hh)" << endl;
						cout << "hh->Get_index   vs   nominal->Get_index(): "<< hh->Get_index() << "\t vs\t" << nominal->Get_index() << endl;
						//cout << 
						ratio = Copy(hh);
						ratio->ResetTText();
						
						cout << "ratio - maximum: " << ratio->GetMaximum();
						cout << "hh -maximum: " << hh->GetMaximum();
						
						ratio->Divide (nominal);
						cout << "divided: ratio - maximum: " << ratio->GetMaximum();
						//int rr;
						//cin >> rr;
						
						
						/// /////////////////////////////////////
						/// NORMALIZATION TO CROSS-SECTION
						TString pyAMI = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/TreeReader/macros/weightsCompare/config/pyAMI.txt";
						cout << "BEFORE pyAMI call" << endl;
						weight_pyAMY(hh, pyAMI);
						
						vecHistRatios_all_can.at(iArr).push_back(ratio);
						vecHist_all_can.at(iArr).push_back(hh);

					//}//
					// ZDE JE PROBLEM 
					// POTREBUJI NOMINLNI HOSTOGRAM PRO KAZDY SLICE 
					// -> ROZSIRIT CLASS VEC_HIST O METODU RESER_RATIOPLOT
					// UKLADAT SI TYTO RATIO PLOTY DO SAMOSTATNEHO vector<HIST*>
					// A TENTO vector <HIST*> RATIOPLOTU POUZIT PRI INICIALITACI 
					// VEC_HIST
					cout << "\n\nNOMINAL HISTOGRAM WAS SET UP " << endl;
					
					//
					// MAYBE HERE WILL END LOOP OVER ALL ROOT FILES PROBLEM CAN BE IN NOMINAL HISTOGRAM
					//
				}
			}
		}/// END OF rootFile paths
		
		cout << "LOOP OVER ALL CANVASES" << endl;
		for(int i_can = 0; i_can < v_id_weights_in_canvas.size(); i_can++)
		{
			/// /////////////////////
			/// Name of variable
			TString fileName = "inclusiveJetPt_weight";
			fileName += std::to_string(i_can);
			VEC_HIST *myVEC_HIST_can_i = new VEC_HIST(vecHist_all_can.at(i_can), fileName, false);
			myVEC_HIST_can_i->ResetRatia(vecHistRatios_all_can.at(i_can));
			myVEC_HIST_can_i->CreateAddHist();
			//myVEC_HIST_can_i->ShowVector(myVEC_HIST_can_i->Get_vector());
			
			/// ////////////////////////////////////////////////
			/// GET CANVAS USING VEC_HIST CLASS OF ANALHEPLER
			
			//VEC_HIST *myVEC_HIST_can_i = new VEC_HIST(vecHist_all_can.at(i_can), fileName, bRatio, vecHist.at(0));
			
			//VEC_HIST *myVEC_HIST_can_i = new VEC_HIST(vecHist_all_can.at(i_can), fileName, bRatio, nominal);
			//myVEC_HIST_can_i->Set_histNominal(nominal);
			
			//
			// ZDE JE PROBLEM 
			// POTREBUJI NOMINLNI HISTOGRAM PRO KAZDY SLICE 
			// -> ROZSIRIT CLASS VEC_HIST O METODU RESET_RATIA
			// UKLADAT SI TYTO RATIO PLOTY DO SAMOSTATNEHO vector<HIST*>
			// A TENTO vector <HIST*> RATIOPLOTU POUZIT PRI INICIALITACI 
			// VEC_HIST
			
			/// ///////////////////////////////////////////////
			/// Call user-graphic method - poiter to function
			myVEC_HIST_can_i->Set_UserGraphic(PrepareToPlotTest);
			myVEC_HIST_can_i->Set_UserGraphic_ratio(PrepareToPlotRatioTest);
			
			/// Plot canvas with appropriate weights 
			myVEC_HIST_can_i->Plot(OUTPUT_PATH);
			
			cout << "AFTER PLOT" << endl;
			
			//cout << "\n\n\n\n\n GET CANVAS" << endl;
			//cout << "num_canvas: " << num_canvas << endl;
			
			cout << "SHOW VECTOR METHOD" << endl;
			myVEC_HIST_can_i->ShowVector(myVEC_HIST_can_i->Get_vector());
			cout << "vecHist_all_can.size(): " << vecHist_all_can.size() << endl;
			cout << "i_can: " << i_can << endl;
			//cout << "v_id_weights_in_canvas.size(): " << v_id_weights_in_canvas.size() << endl;
			
			
			
			//int ttt;
			//cin >> ttt;
			/// ///////////////////////////////////////////////////////
			/// Get canvas for future multicanvas class from ANALHELPER
			
			/// linear scale
			cout << "lin:"  << endl;
			TCanvas *tt = myVEC_HIST_can_i->Get_canvas();
			vCanvases.at(i_can) = (TCanvas *) tt->Clone();
			
			/// logy scale
			cout << "log" << endl;
			TCanvas *tt_log = myVEC_HIST_can_i->Get_canvas_log();
			vCanvases_log.at(i_can) = (TCanvas *) tt_log->Clone();
			
			/// logy-logx scale
			cout << "loglog" << endl;
			TCanvas *tt_loglog = myVEC_HIST_can_i->Get_canvas_loglog(); 
			vCanvases_loglog.at(i_can) = (TCanvas *) tt_loglog->Clone();
			
			cout << "NEW ITERATION LOOP" << endl;
			
			//vCanvases.at(i_can) = &vCanvasesObj.at(i_can);	//(TCanvas*) myVEC_HIST_can_i->Get_canvas();
			//mapHistograms.insert(std::pair<int, VEC_HIST>(i_can, myVEC_HIST_can_i));
			//i_can += 1;
			
		} /// END OF FOR LOOP OVER vector<vector<HIST*>> vecHist_all_can
		
		
		int ind_h=0;
		
		//cout << "before auto loop" << endl;
		/*
		for (auto h: myVEC_HIST)
		{
			cout << "ind_h: " << h->Get_index() << endl;
			
			TString title = "JZX:i";
			title += jzx + " ";
			
			title += "weight-id:";
			title += std::to_string(h->Get_index());
			
			title += " ";
			
			title += arr_WeightsName[h->Get_index()];
			title.ReplaceAll("_", " ");
			
			title = prepare_lines(title);
			title.ReplaceAll("-", " ");
			int ii;
			//if (h.first == 26 )
			//{
			//	cout << "ind == 26" << endl;
			//	cin >> ii;
			//	
			//}
			TCanvas * canvas = new TCanvas;
			canvas = GetCanvas(h, h->Get_index(), title, data_name);
			/*
			//if (h.first == 26 )
			//{
			//	cout << "ind == 26" << endl;
			//	cout << "canvas: " << bool(canvas);
			//	cout << "bool(0);" << bool(0) << endl;
			//	cout << "bool(1)" << bool(1) << endl;
			//	cin >> ii;
			//}
			
			/*
			TString toSave = outputDir + "/" + "leadingJet_pt_";
			toSave += std::to_string(ind_h);
			toSave += ".pdf";
			canvas->SaveAs(toSave);
			vCanvases.push_back(canvas);
			cout << "after Get Canvas" << endl;
			*/
		//}
		
		
		
		
		
		
		cout << "BEFORE MULTICANVAS" << endl;
		
		cout << "vCanvases.size()" << vCanvases.size() << endl;
		
		int count = 0;


		TString ratio_dir;
		if (bRatio == true) ratio_dir = "/ratio/";
		else ratio_dir = "/no_ratio/";
		
		/// ////////////////////
		/// MULTICANVAS BLOCK
		
		/// linear scale
		cout << "LIN CANVAS MULTICANVAS - DECLARATION" << endl;
		cout << "vCanvases.size: " << vCanvases.size() << endl; 
		TString pdf_name =  OUTPUT_PATH + "/pdf/" + ratio_dir + "inclusiveJetPt_multiCanvas.pdf";
		MultiCanvas myMultiCanvas(vCanvases, 3, 3, pdf_name);

		/// log scale
		cout << "LOG CANVAS MULTICANVAS - DECLARATION" << endl;

		TString pdf_name_log =  OUTPUT_PATH + "/pdf/"+ ratio_dir + "inclusiveJetPt_log_multiCanvas.pdf";
		MultiCanvas myMultiCanvas_log(vCanvases_log, 3, 3, pdf_name_log);
		
		/// log-log scale
		cout << "LOGLOG CANVAS MULTICANVAS - DECLARATION" << endl;
		TString pdf_name_loglog =  OUTPUT_PATH + "/pdf/" + ratio_dir + "inclusiveJetPt_loglog_multiCanvas.pdf";
		MultiCanvas myMultiCanvas_loglog(vCanvases_loglog, 3, 3, pdf_name_loglog);
		
		cout << "\n\nTHE END" << endl;
		
		return(0);
	}
	catch(AnalException l){
		l.printMessage();
		cout << "Exiting now!" << endl;
		return (-1);
	}
}

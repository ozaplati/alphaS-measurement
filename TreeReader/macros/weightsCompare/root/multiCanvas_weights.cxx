#include "../../AnalHelper/AnalHelper/VecTH1D_JZX.h"
#include "../../AnalHelper/root/TH1D_JZX.cxx"

#include "../../AnalHelper/AnalHelper/MultiCanvas.h"
#include "../../AnalHelper/root/MultiCanvas.cxx"

#include "../../AnalHelper/AnalHelper/AnalException.h"

#include "TTree.h"
#include "TBranch.h"
//#include "Event.h"

using namespace std;

//////////////////////////////////////////////
//////////////////////////////////////////////
//////////////////////////////////////////////

int main(int nArg, char **arg) {
	try{
		if (nArg!=1){
		cout << "Error: Wrong number of input parameters!" << endl;
		cout << nArg << " were used" << endl;
		throw AnalException("Error: Wrong number of input parameters!"); 
		}

		////////////////////////////
		// TFile
		//
		
		TString tFile_name = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/scripts/JETM1_test_1305_prepare_to_grid/data-tree/mc16_13TeV.root";
		TFile * tFile = new TFile(tFile_name, "READ");
		//gDirectory->cd("treeAlgo");
		TTree * tree = (TTree *) tFile->Get("treeAlgo/nominal"); 
		//TBranch *brEvent;

		float *arr_jet_pt = 0;
		TBranch *br_jet_pt = 0;
		tree->SetBranchAddress("jet_pt", &arr_jet_pt, &br_jet_pt);
		
		float *arr_mcEventWeights= 0;
		TBranch *br_mcEventWeights = 0;
		tree->SetBranchAddress("mcEventWeights", &arr_mcEventWeights, &br_mcEventWeights);
		
		////////////////////////////
		// set Weights names
		//
		
		vector<TString> vWeights_name;
		vector<TH1D_JZX*> vHists;
		vWeights_name.push_back("Var3cUp");
		vWeights_name.push_back("Var3cDown");
		vWeights_name.push_back("isr:muRfac=2.0_fsr:muRfac=2.0");
		vWeights_name.push_back("isr:muRfac=2.0_fsr:muRfac=1.0");
		vWeights_name.push_back("isr:muRfac=2.0_fsr:muRfac=0.5");
		vWeights_name.push_back("isr:muRfac=1.0_fsr:muRfac=2.0");
		vWeights_name.push_back("isr:muRfac=1.0_fsr:muRfac=0.5");
		vWeights_name.push_back("isr:muRfac=0.5_fsr:muRfac=2.0");
		vWeights_name.push_back("isr:muRfac=0.5_fsr:muRfac=1.0");
		vWeights_name.push_back("isr:muRfac=0.5_fsr:muRfac=0.5");
		vWeights_name.push_back("isr:muRfac=1.75_fsr:muRfac=1.0");
		vWeights_name.push_back("isr:muRfac=1.5_fsr:muRfac=1.0");
		vWeights_name.push_back("isr:muRfac=1.25_fsr:muRfac=1.0");
		vWeights_name.push_back("isr:muRfac=0.625_fsr:muRfac=1.0");
		vWeights_name.push_back("isr:muRfac=0.75_fsr:muRfac=1.0");
		vWeights_name.push_back("isr:muRfac=0.875_fsr:muRfac=1.0");
		vWeights_name.push_back("isr:muRfac=1.0_fsr:muRfac=1.75");
		vWeights_name.push_back("isr:muRfac=1.0_fsr:muRfac=1.5");
		vWeights_name.push_back("isr:muRfac=1.0_fsr:muRfac=1.25");
		vWeights_name.push_back("isr:muRfac=1.0_fsr:muRfac=0.625");
		vWeights_name.push_back("isr:muRfac=1.0_fsr:muRfac=0.75");
		vWeights_name.push_back("isr:muRfac=1.0_fsr:muRfac=0.875");
		vWeights_name.push_back("hardHi");
		vWeights_name.push_back("hardLo");
		vWeights_name.push_back("isr:PDF:plus");
		vWeights_name.push_back("isr:PDF:minus");
		
		
		vector<TH1D*> vH;
		TString hist_name = "leading jet";
		TString variable = "pt";
		for (auto weight_name: vWeights_name)
		{
			TH1D *hW = new TH1D(hist_name," distribution of " + variable + "using - " + weight_name, 100,0,100);
			vH.push_back(hW);
		}
		
		double weight, pt;
		//Event myEvent = 0;
		for(Long64_t iEvnt = 0; tree->GetEntries(); iEvnt++)
		{
			Long64_t tree_entry = tree->LoadTree(iEvnt);
			
			br_jet_pt->GetEntry(tree_entry);
			br_mcEventWeights->GetEntry(tree_entry);
			
			//tree->GetEntry(iEvnt);
			//myEvent
			
			for(int iHist = 0; iHist < vH.size(); iHist++)
			{
				// Get weight
				weight = arr_mcEventWeights[iHist];
				// Get pt value of 1st jet
				pt = arr_jet_pt[0];
				//Fill(pT, weight[i]) 
				vH.at(iHist)->Fill(pt, weight);
				
				cout << "ind i: " << iEvnt << "\t leading jet pt: " << pt << "\t weight: " << weight << endl;
			}
		}
		
		cout << "Event loop done" << endl;
                             
                           

/*
         vector<TH1D_JZX*> vTH1D_JZX;
         vector<double_t> vYmax;
         for (int iJZ=0; iJZ < 13; iJZ++){
			 //JZ8 is missing
			 //if (!(iJZ == 0 || iJZ == 11 || iJZ == 10 || iJZ == 12 )) continue ;
			if(iJZ == 8) continue;

			cout << "working with " << iJZ << " slice" << endl;
			 TString JZX_name = "JZ" + std::to_string(iJZ);

             TFile *tree = new TFile(path + "/" + "JZ" + std::to_string(iJZ) + "_tree.root", "READ");
             TH1D *hist1 = (TH1D*) tree->Get(pathInTFile + histName);

             TH1D_JZX *hist = new TH1D_JZX(hist1, iJZ, "nTuple_name", path, pathInTFile);
             vTH1D_JZX.push_back(hist);
             cout << "push" << endl;
             //TH1D_JZX * hNominal = new (TH1D_JZX) hist->Clone();

             
         }
         
         bool bRatio = false;

         TH1D_JZX * hNominal = (TH1D_JZX*) vTH1D_JZX.at(0)->Clone();
         cout << "Inicialization: " << endl;
         VecTH1D_JZX *vec = new VecTH1D_JZX(vTH1D_JZX, "nTuple_name", "", bRatio);
         cout << "NOMINAL" << hNominal->GetMaximum() << endl;
         vec->Set_histNominal(hNominal);
         cout << "User Graphic: " << endl;
         cout << "SHOW VECTOR:" << endl;
         vec->ShowVector(vTH1D_JZX);
         
         vec->Plot(OUTPUT_PATH);

        */
         
		
		//////////////////////////////////////////
		//		TEST OF MULTI-CANVAS CLASS		//
		//////////////////////////////////////////
		/***
		vector<TCanvas *> vec_Canvas;
		int n = 3;
		for (int i=0; i < n*n; i++)  vec_Canvas.push_back(vHists->Get_canvas());
		
		TString outPut = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/TreeReader/plots/pdf/JZX_compare/myClassMutiCanvas.pdf";
		MultiCanvas myMultiCanvas(vec_Canvas, n,n, outPut);
		//myMultiCanvas.Show();
		cout << "THE END" << endl;
		***/
	}
	catch(AnalException l){
		l.printMessage();
		cout << "Exiting now!" << endl;
		return (-1);
	}
}

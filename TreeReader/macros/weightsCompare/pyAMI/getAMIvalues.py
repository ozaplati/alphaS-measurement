import pyAMI.client
import pyAMI.atlas.api as AtlasAPI
import os, sys

client = pyAMI.client.Client('atlas')
AtlasAPI.init()

######################################################################################################
# EDIT
######################################################################################################

useEVNT = True

samples = [
	"mc16_13TeV.364700.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364701.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364705.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364706.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	#"There is no JZ8 sample"
	"mc16_13TeV.364709.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364710.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364711.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",
	"mc16_13TeV.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.deriv.DAOD_JETM1.e7142_e5984_s3126_r10724_p3749",  
]

###########################################################################################################
# DO NOT MODIFY
###########################################################################################################

# Loop over samples
for dataset in samples:
  run = dataset.split(".",2)
  #info = str(run[1])
  prov = AtlasAPI.get_dataset_prov(client,dataset)
  arr = prov['node']
  for dict in arr:
    d = dict['logicalDatasetName']
    if run[1] in d and "minbias" not in d:
      if not useEVNT:
        if ".AOD." in d:
         dd = AtlasAPI.get_dataset_info(client,d)
         xsec = dd[0]['crossSection']
         eff = dd[0]['genFiltEff']
         break
      elif "evgen" in d:
        dd = AtlasAPI.get_dataset_info(client,d)
        xsec = dd[0]['crossSection']
        eff = dd[0]['genFiltEff']
        break
  
  JZX_index = dataset.index("JZ")
  JZX_substring = dataset[JZX_index: JZX_index +4]
  
  ##################################
  # Print runNumber crossSetion and Efficiancy
  if JZX_substring[3].isdigit():
    pass
  else:
    JZX_substring = JZX_substring[0:3]
  
  if JZX_substring in dataset:
    jzx = JZX_substring
    info = jzx + "_runNumber: "
    info += str(run[1])
    info += "\n"
    
    info += jzx + "_crossScetion: "
    info += xsec
    info += "\n"
    
    info += jzx + "_genFiltEff: "
    info += eff
    info += "\n"
    info += "\n"
    
    #info += " " + str(xsec) + " " + str(eff) 
    print info			  

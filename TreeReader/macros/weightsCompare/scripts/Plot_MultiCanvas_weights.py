#!/usr/bin/python
import os, sys

#ROOT_FILE = [9, 8, 7]
JZX = "[JZ0][JZ1][JZ4]"
#ROOT_FILES =  "[/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/scripts/JETM1_test_1305_prepare_to_grid/data-tree/mc16_13TeV.root]"
#ROOT_FILES += "[/afs/cern.ch/work/o/ozaplati/public/yAnalysis/alphaS-measurement/scripts/JETM1_test_1305_prepare_to_grid/data-tree/mc16_13TeV.root]"

#print (ROOT_FILES)

ROOT_BRANCH = "jet_pt"
DATA_NAME = "[Pythia8,A14NNPDF23LO,][WithSW,JETM1]"

#mode = "ALL"
mode = "[1,3][5]"
command = "./../executable/Plot_MultiCanvas_weights " + JZX + "\t " + ROOT_BRANCH + "\t " + DATA_NAME  + "\t" + mode

print("\n\n\ncommand: \n" + command + "\n\n\n")
os.system(command)

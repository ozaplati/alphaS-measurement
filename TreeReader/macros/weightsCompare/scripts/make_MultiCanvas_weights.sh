#!/bin/bash
ROOTFLAGS=$(root-config --cflags)
ROOTLD=$(root-config --libs)

PATH_BUILD="../build"
PATH_ROOT="../root"
PATH_EXE="../executable"
mkdir -p ${PATH_BUILD}
mkdir -p ${PATH_EXE}

###########################
# COMPILE MAIN
echo "class was compiled"
c++ -o ${PATH_BUILD}/multiCanvas_weights.o -c ${PATH_ROOT}/multiCanvas_weights.cxx -I  $ROOTFLAGS $FJFLAGS 			# compile main cxx file

c++ -o ${PATH_BUILD}/Plot_MultiCanvas_weights.o -c ${PATH_ROOT}/Plot_MultiCanvas_weights.cxx -I  $ROOTFLAGS $FJFLAGS 			# compile main cxx file

##################################
# CREATE EXECUTABLE FILE OF MAIN
echo "main was compiled"
#c++ -o ${PATH_EXE}/multiCanvas_weights ${PATH_BUILD}/multiCanvas_weights.o $ROOTLD -I  $ROOTFLAGS					# create executable file
c++ -o ${PATH_EXE}/Plot_MultiCanvas_weights ${PATH_BUILD}/Plot_MultiCanvas_weights.o $ROOTLD -I  $ROOTFLAGS					# create executable file


echo "DONE!"

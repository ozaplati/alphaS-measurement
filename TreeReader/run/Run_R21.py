#!/usr/bin/python
import os, sys
import subprocess

param=sys.argv

#PATH = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/SM-IJXS-Dijets/"
PATH = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/"
#PATH = "/eos/user/a/aia/Jona/SMtrees/MC16a_Pythia"

MC        = True
Debug     = False # No output, small number of events, only many couts used for debugging
PRW       = False

# Unfolding inputs
UnfoldingInputs = False
nReplicas       = "0" # 0: still produces unfolding inputs (TMs, etc) but with no bootstraps (default: 100)

Generator = "Pythia"
#Generator = "Powheg"
#Generator = "PowhegHerwig"
#Generator = "Sherpa"

OutputFile = "/eos/home-o/ozaplati/yAnalysis/JETM1_allSlices/treeExample/treeReader_test/"
#OutputFile = "OUTPUT_unfolding_100Replicas/"

os.system("rm -rf "+ OutputFile )
os.system("mkdir "+ OutputFile )

LumiFile = "lumi_2016data_finalGRL_All_02Dec.txt" # Only for Data

#FirstTrigger = "HLT_j450"
FirstTrigger = "HLT_j25"

Rel21 = True # Needed for latest trees

####################################################################################
####################################################################################

command = ""
counter = 0

# Get list of folders
Folders = []
for folder in os.walk(PATH).next()[1]:
  if "treeExample" in folder:
    if MC:
      if Rel21:
        fPATH = PATH + folder + "/MC"
      else:
        fPATH = PATH + folder + "/old"
    else:
      fPATH = PATH + folder + "/Data"
      
      
    for rfile in os.walk(fPATH).next()[2]:
      #if rfile != "JZ2_tree.root":
      #  continue
      
      InputFile = rfile	    
      OutFile   = OutputFile + rfile
      
      if MC:
        command += "TreetoHists "
        command += "--path="
        command += fPATH
        command += " --inputFile="
        command += InputFile
        command += " --outputFile="
        command += OutFile
        command += " --isMC=TRUE"
        command += " --mcGenerator="
        command += Generator
        command += " --firstTrigger="
        command += FirstTrigger
        if UnfoldingInputs:
          command += " --unfolding=TRUE"
      command += " --nReplicas="
      command += nReplicas
      if Debug:
        command += " --debug=TRUE"
      if Rel21:
        command += " --rel21=TRUE"	  
      if PRW:
        command += " --prw=TRUE"
        command += " > "
      command += OutputFile
      command += "log_"
      #command += str(counter)
      
      log_name = rfile.replace('.root', '')
      command += log_name
      command += " 2> "
      command += OutputFile
      command += "err_"
      #ommand += str(counter)
      command += log_name
      command += " && "
      command += "\\ \n"
      #command += "\\ \n"
      

      if not MC:
        command += "TreetoHists "
        command += "--path="
        command += fPATH
        command += " --inputFile="
        command += InputFile
        command += " --outputFile="
        command += OutFile
        command += " --isMC=FALSE"
        command += " --lumiFile="
        command += LumiFile
        command += " --firstTrigger="
        command += FirstTrigger
        if UnfoldingInputs:
          command += " --unfolding=TRUE"
        command += " --nReplicas="
        command += nReplicas
        if Debug:
          command += " --debug=TRUE"
        if Rel21:
          command += " --rel21=TRUE"	  
        command += " > "
        command += OutputFile
        
        log_name = rfile.replace('.root', '')
        command += "log_"
        #command += str(counter)
        commmand += log_name
        command += " 2> "
        command += log_name
        command += "err_"
        #command += str(counter)
        command += rfile
        command += " && "
        command += "\\ \n"
        #command += "\\ \n"

        #counter += 1

command = command[:-5]
#command += "&"
print("\n\n")
print command
os.system(command)

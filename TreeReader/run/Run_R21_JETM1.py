#!/usr/bin/python

import os, sys

param=sys.argv

inputPATH = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/RunArea/JETM1_test4/JZ2_tree/"
inputROOTFILE = "JZ2_tree.root"

outputPATH = "/afs/cern.ch/work/o/ozaplati/public/yAnalysis/RunArea/JETM1_test4/"
outputROOTFILE = "treeReader_JETM1_JZ2_tree.root"
outputDIR = "OutputDIR/"

##################################################
MC        = True
Debug     = True         # No output, small number of events, only many couts used for debugging
PRW       = False

# Unfolding inputs
UnfoldingInputs = False
nReplicas       = "0" # 0: still produces unfolding inputs (TMs, etc) but with no bootstraps (default: 100)

Generator = "Pythia"
#Generator = "Powheg"
#Generator = "PowhegHerwig"
#Generator = "Sherpa"

LumiFile = "lumi_2016data_finalGRL_All_02Dec.txt" # Only for Data

FirstTrigger = "HLT_j25"

Rel21 = True # Needed for latest trees

####################################################################################
####################################################################################
precommand_mkdir = "mkdir -p " 
precommand_mkdir += outputDIR
print(precommand_mkdir)
os.system(precommand_mkdir)

command = ""
if MC:
  command += "TreetoHists "
  command += "--path="
  command += inputPATH
  command += " --inputFile="
  command += inputROOTFILE
  command += " --outputFile="
  command += outputDIR + "/" + outputROOTFILE
  command += " --isMC=TRUE"
  command += " --mcGenerator="
  command += Generator
  command += " --firstTrigger="
  command += FirstTrigger
  if UnfoldingInputs:
    command += " --unfolding=TRUE"
    command += " --nReplicas="
    command += nReplicas
  if Debug:
    command += " --debug=TRUE"
  if Rel21:
    command += " --rel21=TRUE"
else:
  print("THERE IS NO MC!!!")
  exit(1)
  
if PRW:
  command += " --prw=TRUE"
  command += " > "
  command += outputDIR + "/" + OutputFile
  command += "log_"
  command += str(counter)
  command += " 2> "
  command += outputDIR + "/" + OutputFile
  command += "err_"
  command += str(counter)
  command += " && "


print("\n\n")
print command + "\n\n"
#os.system(command)
 

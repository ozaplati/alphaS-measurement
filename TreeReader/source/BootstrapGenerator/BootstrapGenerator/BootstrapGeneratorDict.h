#ifndef BootstrapGenerator_BootstrapGeneratorDict_h
#define BootstrapGenerator_BootstrapGeneratorDict_h

#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH3Bootstrap.h"

#endif // BootstrapGenerator_BootstrapGeneratorDict_h

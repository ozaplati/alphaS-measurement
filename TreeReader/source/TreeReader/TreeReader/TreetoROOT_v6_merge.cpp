#pragma once

#include "TreetoROOT_v6_merge_cpp.h"


//------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------

double
GetDeltaPhi(TLorentzVector &jet1, TLorentzVector &jet2)
{
  double tmp_delta = fabs(jet1.Phi() - jet2.Phi());
  if(tmp_delta <= TMath::Pi()){return tmp_delta;}
  else {return (2*TMath::Pi() - tmp_delta);}
}



double 
GetYStar(TLorentzVector &_jet1, TLorentzVector &_jet2)
{
  double yStar = 0.5 * fabs(_jet1.Rapidity() - _jet2.Rapidity());
  return yStar;
}

double
GetYBoost(TLorentzVector &_jet1, TLorentzVector &_jet2)
{
  double yBoost = 0.5 * fabs(_jet1.Rapidity() + _jet2.Rapidity());
  return yBoost;
}


int
GetYStarBin (double yStar)
{
  if (yStar < 0.5) return 0;
  if (yStar < 1.0) return 1;
  if (yStar < 1.5) return 2;
  if (yStar < 2.0) return 3;
  if (yStar < 2.5) return 4;
  if (yStar < 3.0) return 5;
  if (yStar >= 3.0) return 6;
  return 6;
}

void
EnableBranches ()
{

  std::cout << "Enabling branches" << std::endl;

  tree->SetBranchStatus ("*", 0); //disable all branches
  if (MC)
    {
      tree->SetBranchStatus ("mcChannelNumber", 1);
      tree->SetBranchStatus ("mcEventNumber", 1);
      tree->SetBranchStatus ("mcEventWeight", 1);
    }
  if (!MC)
    {
      tree->SetBranchStatus ("runNumber", 1);
      tree->SetBranchStatus ("eventNumber", 1);
      tree->SetBranchStatus ("lumiBlock", 1);
      tree->SetBranchStatus ("bcid", 1);
      tree->SetBranchStatus ("passedTriggers", 1);
    }
  tree->SetBranchStatus ("NPV", 1);
  tree->SetBranchStatus ("averageInteractionsPerCrossing", 1);

  if (MC && m_rel21) tree->SetBranchStatus ("weight", 1); //MIRA: tohle je bug, zadna takova branch tam neni; mozna v Rel21
  if (MC && m_PRW) tree->SetBranchStatus ("weight_pileup", 1);
  tree->SetBranchStatus ("jet_pt", 1);
  tree->SetBranchStatus ("jet_eta", 1);
  tree->SetBranchStatus ("jet_phi", 1);
  tree->SetBranchStatus ("jet_E", 1);
  tree->SetBranchStatus ("jet_rapidity", 1);
  tree->SetBranchStatus ("jet_clean_passTightBad", 1);
  tree->SetBranchStatus ("jet_clean_passLooseBad", 1);
  tree->SetBranchStatus ("jet_EnergyPerSampling", 1);
  tree->SetBranchStatus ("jet_Width", 1);
  tree->SetBranchStatus ("jet_EMFrac", 1);
  tree->SetBranchStatus ("jet_GhostMuonSegmentCount", 1);
  if (MC)
    {
      tree->SetBranchStatus ("truthJet_pt", 1);
      tree->SetBranchStatus ("truthJet_eta", 1);
      tree->SetBranchStatus ("truthJet_phi", 1);
      tree->SetBranchStatus ("truthJet_E", 1);
      tree->SetBranchStatus ("truthJet_rapidity", 1);
    }
  if (m_debug)
  {
     
  }

}// EnableBranches()

void
SetBranches ()
{

  std::cout << "setting branches" << std::endl;
  // Event
  if (MC)
    {
      tree->SetBranchAddress ("mcChannelNumber", &runNumber);
      tree->SetBranchAddress ("mcEventNumber", &mceventNumber);
      tree->SetBranchAddress ("mcEventWeight", &mcEventWeight);
    }
  if (!MC)
    {
      tree->SetBranchAddress ("runNumber", &runNumber);
      tree->SetBranchAddress ("eventNumber", &eventNumber);
      tree->SetBranchAddress ("lumiBlock", &lumiBlock);
      tree->SetBranchAddress ("bcid", &bcid);
      tree->SetBranchAddress ("passedTriggers", &passedTriggers);
    }
  tree->SetBranchAddress ("NPV", &NPV);
  tree->SetBranchAddress ("averageInteractionsPerCrossing", &mu);

  // Reco Jets
  if (MC && m_rel21) tree->SetBranchAddress ("weight", &wgt);
  if (MC && m_PRW && m_rel21) tree->SetBranchAddress ("weight", &weight_pileup); 


  tree->SetBranchAddress ("jet_pt", &jet_pt);
  tree->SetBranchAddress ("jet_eta", &jet_eta);
  tree->SetBranchAddress ("jet_phi", &jet_phi);
  tree->SetBranchAddress ("jet_E", &jet_E);
  tree->SetBranchAddress ("jet_rapidity", &jet_rapidity);
  tree->SetBranchAddress ("jet_EnergyPerSampling", &jet_EnergyPerSampling);
  tree->SetBranchAddress ("jet_clean_passTightBad", &jet_clean_passTightBad);
  tree->SetBranchAddress ("jet_clean_passLooseBad", &jet_clean_passLooseBad);
  
  tree->SetBranchAddress ("jet_Width", &jet_width);
  tree->SetBranchAddress ("jet_EMFrac", &jet_EMFrac);
  tree->SetBranchAddress ("jet_GhostMuonSegmentCount", &jet_Nsegments);
  // Truth jets
  if (MC)
    {
      tree->SetBranchAddress ("truthJet_pt", &truthjet_pt);
      tree->SetBranchAddress ("truthJet_eta", &truthjet_eta);
      tree->SetBranchAddress ("truthJet_phi", &truthjet_phi);
      tree->SetBranchAddress ("truthJet_E", &truthjet_E);
      tree->SetBranchAddress ("truthJet_rapidity", &truthjet_rapidity);
    }

}// SetBranches()

void
SetBinning ()
{

  // eta bins for eta histogram
  for (unsigned int i = 0; i <= netaBins; i++)
    {
      etaBins[i] = -4.5 + (i / 10.0);
    }

  // Finer eta bins for eta histogram
  for (unsigned int i = 0; i <= nfineretaBins; i++)
    {
      fineretaBins[i] = -4.5 + (i / 50.0);
    }

  // EMFrac bins
  for (unsigned int i = 0; i <= nEMFracBins; i++)
    {
      EMFracBins[i] = i / nEMFracBins;
    }

  // phi bins for phi histogram
  for (unsigned int i = 0; i <= nphiBins; i++)
    {
      phiBins[i] = -3.2 + (i / 10.0);
    }

  // y bins for y histograms
  for (unsigned int i = 0; i <= nyBinsyHisto; i++)
    {
      yBinsyHisto[i] = -3. + (i / 10.0);
    }

}// SetBinning()

int
RecoMatch_index (TLorentzVector myjet, std::vector<TLorentzVector> jets, TLorentzVector &matchingJet, int &index, double &DRmin)
{
  int Nmatches = 0;
  double drmin = 999;
  int Min_index = -1;
  for (unsigned int ji = 0; ji < jets.size (); ++ji)
    {
      double dr = myjet.DeltaR (jets[ji]);
      if (dr < 0.3) ++Nmatches;
      //find minimum:
      if (dr < drmin)
        {
          drmin = dr;
          Min_index = ji;
        }
    }
  DRmin = drmin;
  if (drmin < 0.3)
    { //FIXME
      matchingJet = jets[Min_index];
      index = Min_index;
    }
  return Nmatches;
}

int
Match_index_inv (TLorentzVector myjet, std::vector<TLorentzVector> jets, int index_veto, double DRmin)
{
  int Nmatches = 0;
  double drmin = 999;
  for (unsigned int ji = 0; ji < jets.size (); ++ji)
    {
      double dr = myjet.DeltaR (jets[ji]);
      if (ji == (unsigned int) index_veto) continue;
      if (dr < drmin)
        {
          drmin = dr;
        }
    }
  if (drmin < DRmin) ++Nmatches;
  return Nmatches;
}

double
DRmin (TLorentzVector myjet, std::vector<TLorentzVector> jets, double PtMin)
{
  double DRmin = 9999;
  for (unsigned int ji = 0; ji < jets.size (); ++ji)
    {
      if (PtMin > 0 && jets[ji].Pt () < PtMin) continue;
      double DR = myjet.DeltaR (jets[ji]);
      if (DR > 0.001 && DR < DRmin) DRmin = DR;
    }
  return DRmin;
}




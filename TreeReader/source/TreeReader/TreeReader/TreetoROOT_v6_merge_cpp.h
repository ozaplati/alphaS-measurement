#ifndef run_h
#define run_h

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <string>
#include <map>
#include <fstream>
#include <utility>
#include <math.h>

#include "TROOT.h"
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TH1.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"
#include "TreeReader/TreetoROOT_v6_merge.cpp"



#include <stdio.h>

// Bootstrap
#include "BootstrapGenerator/TH2DBootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/BootstrapGenerator.h"

using namespace std;

double GetDeltaPhi(TLorentzVector &jet1, TLorentzVector &jet2);
double GetYStar(TLorentzVector &jet1, TLorentzVector &jet2);
double GetYBoost(TLorentzVector &jet1, TLorentzVector &jet2);
int GetYStarBin(double yStar);
void EnableBranches ();
void SetBranches ();
void SetBinning ();
int RecoMatch_index(TLorentzVector myjet, std::vector<TLorentzVector> jets, TLorentzVector &matchingJet, int &index, double &DRmin);
int Match_index_inv(TLorentzVector myjet, std::vector<TLorentzVector> jets, int index_veto, double DRmin);
double DRmin(TLorentzVector myjet, std::vector<TLorentzVector> jets, double PtMin);


BootstrapGenerator *fGenerator;

#endif

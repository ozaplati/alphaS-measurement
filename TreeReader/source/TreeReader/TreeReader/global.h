//
// global.h
//
// Binning for Inclusive and Dijet analysis
//
// Jonathan Bossio - 15 June 2015
//
///////////////////////////////////////////

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include "TLorentzVector.h"
#include <stdlib.h>
#include <algorithm>
#include <map>
#include <utility>
#include <math.h>
#include <TROOT.h>
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TDirectory.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"
#include <sstream>

//--------
// Binning
//--------

// ystar bins for mass spectra
static const int nystarBins = 7;
double ystarBins[nystarBins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.5};

// y bins for pT spectra
static const int nyBins = 6;
double yBins[nyBins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0};

// y bins for y histograms
static const int nyBinsyHisto = 60;
double yBinsyHisto[nyBinsyHisto+1];

// eta bins for eta histogram
static const int netaBins = 90;
double etaBins[netaBins+1];

static const int nNoteEtaBins = 34;
double NoteEtaBins[nNoteEtaBins+1] = {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.285, 1.37, 1.42, 1.47, 1.51, 1.55, 1.59, 1.63, 1.6775, 1.725, 1.7625, 1.8, 1.9, 2, 2.05, 2.1, 2.2, 2.3, 2.35, 2.4, 2.435, 2.47};

// EMFrac bins for eta histogram
static const int nEMFracBins = 100;
double EMFracBins[nEMFracBins+1];

// eta bins for eta histogram
static const int nfineretaBins = 450;
double fineretaBins[nfineretaBins+1];

// phi bins for phi histogram
static const int nphiBins = 3;
double phiMax1 = (7.0/8.0)*TMath::Pi();
double phiMax2 = (5.0/6.0)*TMath::Pi();
double phiMax3 = (3.0/4.0)*TMath::Pi();
double phiMax4 = (2.0/3.0)*TMath::Pi();
double phiBins[nphiBins+1] = {phiMax1,phiMax2,phiMax3,phiMax4};

// ystar bins for ystar histogram
const int nDelYBins = 9;
double delYBins[nDelYBins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.4 };   // 9 bins

//-----------------
// Inclusive jet pT
//-----------------

// Different pt bins for each y bin
const int nptBins_0 = 35+8;
const int nptBins_1 = 35+8;
const int nptBins_2 = 35+8;
const int nptBins_3 = 35+8;
const int nptBins_4 = 35+8;
const int nptBins_5 = 35+8;
double ptBins_0_aux[nptBins_0+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_1_aux[nptBins_1+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_2_aux[nptBins_2+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_3_aux[nptBins_3+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_4_aux[nptBins_4+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_5_aux[nptBins_5+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double * ptBins_0 = ptBins_0_aux;
double * ptBins_1 = ptBins_1_aux;
double * ptBins_2 = ptBins_2_aux;
double * ptBins_3 = ptBins_3_aux;
double * ptBins_4 = ptBins_4_aux;
double * ptBins_5 = ptBins_5_aux;
double ** ptBinsArray = new double * [nyBins];

// Number of pt bins for each y bin
const int nptBins[nyBins] = {nptBins_0, nptBins_1, nptBins_2, nptBins_3, nptBins_4, nptBins_5};

const int nptFinerBins = 35;
double ptFinerBins[nptFinerBins+1] = {100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};

//---------------
// Invariant Mass
//---------------

// Different m12 bins for each y* bin
const int nMassBins_0 = 44;
const int nMassBins_1 = 44;
const int nMassBins_2 = 44;
const int nMassBins_3 = 44;
const int nMassBins_4 = 44;
const int nMassBins_5 = 44;
const int nMassBins_6 = 44;
double massBins_0_aux[nMassBins_0+1] = { 0., 30., 70., 110., 160., 210., 260., 310., 370., 440., 510., 590., 670., 760., 850., 950., 1060., 1180., 1310., 1450., 1600., 1760., 1940., 2120., 2330., 2550., 2780., 3040., 3310., 3610., 3930., 4270., 4640., 5040., 5470., 5940., 6440., 7000., 7630., 8317., 9066., 9882., 10771., 11740., 12797.};
double massBins_1_aux[nMassBins_1+1] = { 0., 30., 70., 110., 160., 210., 260., 310., 370., 440., 510., 590., 670., 760., 850., 950., 1060., 1180., 1310., 1450., 1600., 1760., 1940., 2120., 2330., 2550., 2780., 3040., 3310., 3610., 3930., 4270., 4640., 5040., 5470., 5940., 6440., 7000., 7630., 8317., 9066., 9882., 10771., 11740., 12797.};
double massBins_2_aux[nMassBins_2+1] = { 0., 30., 70., 110., 160., 210., 260., 310., 370., 440., 510., 590., 670., 760., 850., 950., 1060., 1180., 1310., 1450., 1600., 1760., 1940., 2120., 2330., 2550., 2780., 3040., 3310., 3610., 3930., 4270., 4640., 5040., 5470., 5940., 6440., 7000., 7630., 8317., 9066., 9882., 10771., 11740., 12797.};
double massBins_3_aux[nMassBins_3+1] = { 0., 30., 70., 110., 160., 210., 260., 310., 370., 440., 510., 590., 670., 760., 850., 950., 1060., 1180., 1310., 1450., 1600., 1760., 1940., 2120., 2330., 2550., 2780., 3040., 3310., 3610., 3930., 4270., 4640., 5040., 5470., 5940., 6440., 7000., 7630., 8317., 9066., 9882., 10771., 11740., 12797.};
double massBins_4_aux[nMassBins_4+1] = { 0., 30., 70., 110., 160., 210., 260., 310., 370., 440., 510., 590., 670., 760., 850., 950., 1060., 1180., 1310., 1450., 1600., 1760., 1940., 2120., 2330., 2550., 2780., 3040., 3310., 3610., 3930., 4270., 4640., 5040., 5470., 5940., 6440., 7000., 7630., 8317., 9066., 9882., 10771., 11740., 12797.};
double massBins_5_aux[nMassBins_5+1] = { 0., 30., 70., 110., 160., 210., 260., 310., 370., 440., 510., 590., 670., 760., 850., 950., 1060., 1180., 1310., 1450., 1600., 1760., 1940., 2120., 2330., 2550., 2780., 3040., 3310., 3610., 3930., 4270., 4640., 5040., 5470., 5940., 6440., 7000., 7630., 8317., 9066., 9882., 10771., 11740., 12797.};
double massBins_6_aux[nMassBins_6+1] = { 0., 30., 70., 110., 160., 210., 260., 310., 370., 440., 510., 590., 670., 760., 850., 950., 1060., 1180., 1310., 1450., 1600., 1760., 1940., 2120., 2330., 2550., 2780., 3040., 3310., 3610., 3930., 4270., 4640., 5040., 5470., 5940., 6440., 7000., 7630., 8317., 9066., 9882., 10771., 11740., 12797.};
double * massBins_0 = massBins_0_aux;
double * massBins_1 = massBins_1_aux;
double * massBins_2 = massBins_2_aux;
double * massBins_3 = massBins_3_aux;
double * massBins_4 = massBins_4_aux;
double * massBins_5 = massBins_5_aux;
double * massBins_6 = massBins_6_aux;
double ** massBinsArray = new double * [nystarBins];

// Number of m12 bins for each y* bin
const int nMassBins[nystarBins] = {nMassBins_0, nMassBins_1, nMassBins_2, nMassBins_3, nMassBins_4, nMassBins_5, nMassBins_6};

// Triggers
std::vector<std::string > Triggers;
std::map<std::string, double> minPtTriggers;
std::map<std::string, double> maxPtTriggers;
std::map<std::string, int> TriggertoInt;
// Names of directories
std::map<int, std::string> yDirNames = {{0,"y00to05"},
										{1,"y05to10"},
										{2,"y10to15"},
										{3,"y15to20"},
										{4,"y20to25"},
										{5,"y25to30"}};
std::map<int, std::string> ystarDirNames = {{0,"ystar00to05"},
											{1,"ystar05to10"},
											{2,"ystar10to15"},
											{3,"ystar15to20"},
											{4,"ystar20to25"},
											{5,"ystar25to30"},
											{6,"ystar30to45"}};

// Tree
TTree *tree;

//------------------------
// Branch Variables
//------------------------

// Event Info
int runNumber;
Long64_t eventNumber;
int mceventNumber;
float mcEventWeight;
int lumiBlock;
int bcid;
int NPV;
float mu;
double rho;
double wgt;//MIRA:ehm?
double weight_pileup;//MIRA:nic takoveho ve cvicnem stromu neni

// Reco Jets
int njet;
std::vector<float> *jet_pt = 0;
std::vector<float> *jet_eta = 0;
std::vector<float> *jet_phi = 0;
std::vector<float> *jet_E = 0;
std::vector<float> *jet_rapidity = 0;
std::vector<float> *jet_width = 0;// in MC16d as jet_Width
std::vector<float> *jet_EMFrac = 0;
std::vector<float> *jet_constitScalepT = 0; //in MC16d non-existent or maybe as jet_origin ConstitScalePt
std::vector<float> *jet_etaJESScalepT = 0; // in MC16d as jet_etaJESScalePt
std::vector<float> *jet_gscScalepT = 0; // in MC16d as jet_gscScalePt
std::vector<float> *jet_constitScaleE = 0; // not in MC16d only jet_originConstitScaleM
std::vector<float> *jet_Nsegments = 0;
std::vector<std::vector<float> >*jet_EnergyPerSampling = 0;
std::vector<std::vector<int> > *jet_NumTrkPt1000 = 0;// not in MC16d
std::vector<std::vector<float> > *jet_TrackWidthPt1000 = 0; // not in MC16d
std::vector<int> *jet_clean_passTightBad = 0;
std::vector<int> *jet_clean_passLooseBad = 0;

// Truth Jets
int ntruthjets;
std::vector<float> *truthjet_pt = 0;
std::vector<float> *truthjet_eta = 0;
std::vector<float> *truthjet_phi = 0;
std::vector<float> *truthjet_E = 0;
std::vector<float> *truthjet_rapidity = 0;

// Passed Triggers
std::vector<std::string> *passedTriggers = 0;


//==============================================================================
// Global variables
//==============================================================================
TString m_PATH = "";
TString m_inputFile = "";
TString m_outputFile = "";
TString m_lumiFile = "";
TString m_generator = "Pythia";
TString m_firstTrigger = "HLT_j380";
TString m_unprescaledTrig = "HLT_j450";
bool MC = false;
bool m_debug = false;
bool m_runDijets = true;
bool m_rel21 = false;
bool m_PRW = false;
bool m_unfolding = false; //MIRA: bylo truth
bool m_bootstraps = false;
int m_nReplicas = 100;
double m_intermediateMu = 13.5; // Needs to be updated for 2016 TEMPORARY
// Dijet Selection
double m_j0min = 75;
double m_j1min = 75;
double m_HT2min = 200;


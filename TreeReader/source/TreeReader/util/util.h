#pragma once

bool GetStartingOptions(int argC, char **argV);
int GetTrigger();




bool 
GetStartingOptions(int argC, char **argV)
{
  //---------------------------
  // Decoding the user settings
  //---------------------------

  std::string path = "";
  std::string inputFile = "";
  std::string outputFile = "";
  std::string lumiFile = "";
  std::string generator = "Pythia";
  std::string firstTrigger = "HLT_j380";

  for (int i = 1; i < argC; i++)
    {

      std::string opt (argV[i]);
      std::vector< std::string > v;

      std::istringstream iss (opt);

      std::string item;
      char delim = '=';

      while (std::getline (iss, item, delim))
        {
          v.push_back (item);
        }

      if (opt.find ("--path=") != std::string::npos) path = v[1];

      if (opt.find ("--inputFile=") != std::string::npos) inputFile = v[1];

      if (opt.find ("--outputFile=") != std::string::npos) outputFile = v[1];

      if (opt.find ("--lumiFile=") != std::string::npos) lumiFile = v[1];

      if (opt.find ("--mcGenerator=") != std::string::npos) generator = v[1];

      if (opt.find ("--firstTrigger=") != std::string::npos) firstTrigger = v[1];

      std::string::size_type sz; // alias of size_t
      if (opt.find ("--nReplicas=") != std::string::npos) m_nReplicas = std::stoi (v[1], &sz);
      if (m_debug && m_unfolding) std::cout << "Number of replicas: " << m_nReplicas << std::endl;

      if (opt.find ("--unfolding=") != std::string::npos)
        {
          if (v[1].find ("TRUE") != std::string::npos) m_unfolding = true;
          if (v[1].find ("FALSE") != std::string::npos) m_unfolding = false;
        }

      if (opt.find ("--isMC=") != std::string::npos)
        {
          if (v[1].find ("TRUE") != std::string::npos) MC = true;
          if (v[1].find ("FALSE") != std::string::npos) MC = false;
        }

      if (opt.find ("--debug=") != std::string::npos)
        {
          if (v[1].find ("TRUE") != std::string::npos) m_debug = true;
          if (v[1].find ("FALSE") != std::string::npos) m_debug = false;
        }

      if (opt.find ("--dijets=") != std::string::npos)
        {
          if (v[1].find ("TRUE") != std::string::npos) m_runDijets = true;
          if (v[1].find ("FALSE") != std::string::npos) m_runDijets = false;
        }

      if (opt.find ("rel21=") != std::string::npos)
        {
          if (v[1].find ("TRUE") != std::string::npos) m_rel21 = true;
          if (v[1].find ("FALSE") != std::string::npos) m_rel21 = false;
        }

      if (opt.find ("prw=") != std::string::npos)
        {
          if (v[1].find ("TRUE") != std::string::npos) m_PRW = true;
          if (v[1].find ("FALSE") != std::string::npos) m_PRW = false;
        }

    }//End: Loop over input options

  m_PATH = path;
  m_inputFile = inputFile;
  m_outputFile = outputFile;
  m_lumiFile = lumiFile;
  m_generator = generator;
  m_firstTrigger = firstTrigger;

  if (m_PATH == "")
    {
      std::cout << "ERROR: no path specified, exiting" << std::endl;
      return false;
    }
  if (m_inputFile == "")
    {
      std::cout << "ERROR: no inputFile specified, exiting" << std::endl;
      return false;
    }
  if (m_outputFile == "")
    {
      std::cout << "ERROR: no outputFile specified, exiting" << std::endl;
      return false;
    }
  if (!MC && m_lumiFile == "")
    {
      std::cout << "ERROR: no lumiFile specified, exiting" << std::endl;
      return false;
    }

  if (m_unfolding && m_nReplicas == 0) m_bootstraps = false;
  
  return true;
  
}


int GetTrigger()
{
  //----------
  // Triggers
  //----------

  //Triggers.push_back("HLT_j15");
  Triggers.push_back ("HLT_j25");
  //Triggers.push_back("HLT_j35");
  //Triggers.push_back("HLT_j45");
  //Triggers.push_back("HLT_j55");
  Triggers.push_back ("HLT_j60");
  //Triggers.push_back("HLT_j85");
  Triggers.push_back ("HLT_j110");
  //Triggers.push_back("HLT_j150");
  Triggers.push_back ("HLT_j175");
  //Triggers.push_back("HLT_j200");
  //Triggers.push_back("HLT_j260");
  Triggers.push_back ("HLT_j380");
  Triggers.push_back ("HLT_j450");

  
  minPtTriggers["HLT_j25"] = 50.;
  maxPtTriggers["HLT_j25"] = 100.;

  minPtTriggers["HLT_j60"] = 100.;
  maxPtTriggers["HLT_j60"] = 200.;

  minPtTriggers["HLT_j110"] = 200.;
  maxPtTriggers["HLT_j110"] = 300.;

  minPtTriggers["HLT_j175"] = 300.;
  maxPtTriggers["HLT_j175"] = 500.;

  minPtTriggers["HLT_j380"] = 500.;
  maxPtTriggers["HLT_j380"] = 10000.;

  minPtTriggers["HLT_j450"] = 516.;
  maxPtTriggers["HLT_j450"] = 10000.;

  TriggertoInt["HLT_j25"] = 0;
  TriggertoInt["HLT_j60"] = 1;
  TriggertoInt["HLT_j110"] = 2;
  TriggertoInt["HLT_j175"] = 3;
  TriggertoInt["HLT_j380"] = 4;
  TriggertoInt["HLT_j450"] = 5;
  
  if(m_debug) std::cout << "Trigger " << m_firstTrigger.Data() << std::endl;
 
  return TriggertoInt[m_firstTrigger.Data ()]; // Default: HLT_j380

}


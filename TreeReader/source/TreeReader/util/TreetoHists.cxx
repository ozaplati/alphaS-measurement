//
// TreetoHists
//
// Tree Reader for inclusive jet and dijet analyses
//
// Jonathan Bossio - 28 Sep 2016
//
/////////////////////////////////////////////

#include "TreeReader/global.h"
#include "TreeReader/TreetoROOT_v6_merge_cpp.h"
#include "util.h"
#include <typeinfo>

//------------------------------------------------------------------------------
// Global variables moved into "global.h"
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Function declaration and definitions moved into TreetoRoot_v6_merge*
//------------------------------------------------------------------------------

//------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------

int
main (int argc, char **argv)
{
  // definition of GetStartingOption() in util.h
  bool goAhead = GetStartingOptions(argc, argv); 
  
  if(!goAhead) return 1;


  int firstTrig = GetTrigger(); // definition of GetTrigger() in util.h

  //----------------------------
  // Reading Luminosity Matrix
  //----------------------------

  std::vector<std::vector<double> > wgtDijets;
  if (!MC)
    {
      // Input File
      std::ifstream in;
      TString lumifile = "Luminosities/";
      lumifile += m_lumiFile;
      std::cout << "Reading luminosity from: " << lumifile << std::endl;
      in.open (lumifile, std::ifstream::in);

      double t0, t1, t2, t3, t4;

      // Reading values
      while (in >> t0 >> t1 >> t2 >> t3 >> t4)
        {
          std::vector<double> temp;
          temp.push_back (t0);
          temp.push_back (t1);
          temp.push_back (t2);
          temp.push_back (t3);
          temp.push_back (t4);
          wgtDijets.push_back (temp);
        }

      in.close ();
    }

  //-----------------------
  // Getting File
  //-----------------------

  TString File = m_PATH;
  File += "/";
  File += m_inputFile;
  std::cout << "\nOpening " << File << std::endl;

  //-----------------------
  // Getting Tree
  //-----------------------

  TFile* ft;
  if (!File.Contains ("eos"))
    {
      ft = new TFile (File, "read");
    }
  else
    {
      ft = TFile::Open (File, "read");
    }
  if (ft == NULL)
    {
      std::cout << "File does not exist! Exiting" << std::endl;
      return 1;
    }
  if (m_debug) std::cout << "File opened" << std::endl;
  ft->cd ();
  TString treeName = "nominal";
  TString directoryName = "treeAlgo";
  if (m_rel21)
    {
      std::cout << "Opening file " << File << " containing the TDirectory ";
      std::cout << directoryName << " with the tree " << treeName << std::endl;
      TDirectory* TDirF_tmp = ft->GetDirectory (directoryName.Data ());
      //TDirF_tmp->GetObject(treeName.Data(),tree);
      tree = (TTree*) TDirF_tmp->Get (treeName.Data ());
    }
  if (!tree)
    {
      std::cout << "Tree does not exist! Exiting" << std::endl;
      return 1;
    }
  if (m_debug) std::cout << "Tree opened: " << treeName << std::endl;

  EnableBranches ();
  SetBranches ();

  int entries = tree->GetEntries (); // Number of events
  //if(m_debug) entries = 10;          // Run over only 10 events in debug mode

  //-----------------------------
  // Defining eta, phi and y bins
  //-----------------------------

  if (m_debug) std::cout << "Setting Binning" << std::endl;
  SetBinning ();

  //--------------------------------------
  // Histograms
  //--------------------------------------

  if (m_debug) std::cout << "Creating Histograms" << std::endl;
  TString histname_tmp; // General auxiliar histogram name

  //----------------------
  // Dijet distributions
  //----------------------

  // Bootstraps
  fGenerator = new BootstrapGenerator ("Generator", "Generator", 0);
  //TH2DBootstrap * fBootstrap_m12[nystarBins];
  //TH1DBootstrap * fRecoBootstrap_m12[nystarBins];
  //TH1DBootstrap * fTruthBootstrap_m12[nystarBins];

  //MIRA: tady se definuji histogramy - jako pole histogramu pro jednotlive pod-cuty, napr:
  
  TH1D *h_m12_reco[nystarBins];
  for(int ystarbin=0;ystarbin<nystarBins;++ystarbin)
  {
    histname_tmp  = "h_m12_reco_";
    histname_tmp += ystarbin;
    h_m12_reco[ystarbin] = new TH1D(histname_tmp.Data(),"",nMassBins[ystarbin],massBins_0);
    h_m12_reco[ystarbin]->Sumw2();
  }// END: Loop over ystar bins
   

  //MIRA: nechavam jen neco malo, kdyztak se k tomu vratime v originalu...

  //---------------------
  // Histogram of weights
  //---------------------
  TH1D *h_wgt = new TH1D ("h_wgt", "", 7e4, 0., 7e5); // Pythia
  //TH1D *h_wgt = new TH1D("h_wgt","",7e4,0.,1); // Powheg
  //TH1D *h_wgt = new TH1D("h_wgt","",7e4,0.,1000000); // Powheg

  //-----------------------------
  // Inclusive jets Distributions
  //-----------------------------

  TH1D *h_njet = new TH1D ("h_njet", "", 20, 0, 20);
  h_njet->Sumw2 ();

  //----------------------------------------------------------
  // pT distributions (All jets - Leading and subleading jets)
  //----------------------------------------------------------

  // y inclusive (|y|<3)
  // All Jets
  TH1D *h_pt_with_wgt_y_inclusive = new TH1D ("h_pt_with_wgt_y_inclusive", "", nptBins[0], ptBins_0);
  h_pt_with_wgt_y_inclusive->Sumw2 ();
  TH1D *h_pt_leading_y_inclusive =  new TH1D ("h_pt_leading_y_inclusive", "", nptBins[0], ptBins_0);
  h_pt_leading_y_inclusive->Sumw2 ();



  //--------------------------------------------
  // Control plots for ATLAS 1711.02692.pdf comparison
  // jets in|y| = 3, 
  // inclusive pT in (100 GeV; 3.5 TeV)
  // dijet mass in (300GeV; 9 TeV)
  //--------------------------------------------
  
  // pT and mass distributions
  TH1D * h_pt_truth[nyBins] = {NULL};
  TH1D * h_m_dijet_truth[nyBins] = {NULL};

  TH1D * h_pt[nyBins] = {NULL};
  TH1D * h_m_dijet[nyBins] = {NULL};
  if(MC) // Truth sim
  {
    for (int ybin = 0; ybin < nyBins; ++ybin)
      {
        histname_tmp = "h_pt_truth_ybin";
        histname_tmp += ybin;
        h_pt_truth[ybin] = new TH1D (histname_tmp.Data (), "", nptBins[ybin], ptBins_0);
        h_pt_truth[ybin]->Sumw2 ();
        histname_tmp = "h_m_dijet_truth_ybin";
        histname_tmp += ybin;
        h_m_dijet_truth[ybin] = new TH1D (histname_tmp.Data (), "", nMassBins[ybin], massBins_0);
        h_m_dijet_truth[ybin]->Sumw2 ();
      }
  }
  else // Data distributions
  {
    for (int ybin = 0; ybin < nyBins; ++ybin)
      {
        histname_tmp = "h_pt_ybin";
        histname_tmp += ybin;
        h_pt[ybin] = new TH1D (histname_tmp.Data (), "", nptBins[ybin], ptBins_0);
        h_pt[ybin]->Sumw2 ();

        histname_tmp = "h_m_dijet_truth_ybin";
        histname_tmp += ybin;
        h_m_dijet[ybin] = new TH1D (histname_tmp.Data (), "", nMassBins[ybin], massBins_0);
        h_m_dijet[ybin]->Sumw2 ();
      }
  } // end of if (MC) else

  //---------------------------
  // Event Shape distributions 
  //---------------------------
  TH2D * h2_yStar_yBoost_truth = new TH2D("h2_yStar_yBoost_truth", "",nystarBins,ystarBins, nystarBins, ystarBins);
  h2_yStar_yBoost_truth->Sumw2();

  TH2D * h2_yStar_yBoost = new TH2D("h2_yStar_yBoost", "",nystarBins,ystarBins, nystarBins, ystarBins);
  h2_yStar_yBoost->Sumw2();

  TH1D * h_dPhi_truth[nyBins] = {NULL};
  TH1D * h_dPhi[nyBins] = {NULL};
  if(MC)
  {  
    for (int ybin = 0; ybin < nyBins; ++ybin)
      {
        histname_tmp = "h_dPhi_truth_ybin";
        histname_tmp += ybin;
        h_dPhi_truth[ybin] = new TH1D (histname_tmp.Data (), "", 10, 0, TMath::Pi());
        h_dPhi_truth[ybin]->Sumw2 ();
      }
  }
  else
  {
    for (int ybin = 0; ybin < nyBins; ++ybin)
      {
        histname_tmp = "h_dPhi_ybin";
        histname_tmp += ybin;
        h_dPhi[ybin] = new TH1D (histname_tmp.Data (), "", 10, 0, TMath::Pi());
        h_dPhi[ybin]->Sumw2 ();
      }
  }// end if(MC)else



  //-------------------------------------------------------
  // ATLAS-like distro for potential measurement of Alpha_S
  //-------------------------------------------------------
  
  TH1D* h_dPhiMax_truth[nystarBins][nphiBins] = {NULL};
  TH1D* h_dPhi_truth_ystarbin[nystarBins] = {NULL};
  

  TH1D* h_dPhiMax[nystarBins][nphiBins] = {NULL};
  TH1D* h_dPhi_ystarbin[nystarBins] = {NULL};
  
  if(MC)
  {  
    for(int ystarbin=0;ystarbin<nystarBins;++ystarbin)
    {
      for(int phibin=0;phibin<nphiBins;phibin++)
      {
        histname_tmp  = "h_dPhiMax_truth_ystarbin";
        histname_tmp += ystarbin;
        histname_tmp += "_phibin_";
        histname_tmp += phibin;
        h_dPhiMax_truth[ystarbin][phibin] = new TH1D(histname_tmp.Data(),"",10, 0, TMath::Pi());
        h_dPhiMax_truth[ystarbin][phibin]->Sumw2();
      }
      
      histname_tmp  = "h_dPhi_truth_ystarbin";
      histname_tmp += ystarbin;
      h_dPhi_truth_ystarbin[ystarbin] = new TH1D(histname_tmp.Data(),"",10, 0, TMath::Pi());
      h_dPhi_truth_ystarbin[ystarbin]->Sumw2();
    }// END: Loop over ystar bins
  }
  else
  {
    for(int ystarbin=0;ystarbin<nystarBins;++ystarbin)
    {
      for(int phibin=0;phibin<nphiBins;phibin++)
      {
        histname_tmp  = "h_dPhiMax_ystarbin";
        histname_tmp += ystarbin;
        histname_tmp += "_phibin_";
        histname_tmp += phibin;
        h_dPhiMax[ystarbin][phibin] = new TH1D(histname_tmp.Data(),"",10, 0, TMath::Pi());
        h_dPhiMax[ystarbin][phibin]->Sumw2();
      }
      
      histname_tmp  = "h_dPhi_ystarbin";
      histname_tmp += ystarbin;
      h_dPhi_ystarbin[ystarbin] = new TH1D(histname_tmp.Data(),"",10, 0, TMath::Pi());
      h_dPhi_ystarbin[ystarbin]->Sumw2();
    }// END: Loop over ystar bins
  }



  //------------------
  // NPV distributions
  //------------------
  TH1D *h_NPV = new TH1D ("h_NPV", "", 100, 0, 100);
  h_NPV->Sumw2 ();
  TH1D *h_NPV_wJetSelection = new TH1D ("h_NPV_wJetSelection", "", 100, 0, 100);
  h_NPV_wJetSelection->Sumw2 ();
  TH1D *h_NPV_mu_14_21 = new TH1D ("h_NPV_mu_14_21", "", 200, 0, 100);
  h_NPV_mu_14_21->Sumw2 ();
  TH1D *h_NPV_mu_21_28 = new TH1D ("h_NPV_mu_21_28", "", 200, 0, 100);
  h_NPV_mu_21_28->Sumw2 ();
  TH1D *h_NPV_inclusive = new TH1D ("h_NPV_inclusive", "", 100, 0, 100);
  h_NPV_inclusive->Sumw2 ();

  //-------------------
  // <mu> distributions
  //-------------------
  TH1D *h_mu = new TH1D ("h_mu", "", 100, 0, 100);
  h_mu->Sumw2 ();
  TH1D *h_mu_wJetSelection = new TH1D ("h_mu_wJetSelection", "", 100, 0, 100);
  h_mu_wJetSelection->Sumw2 ();
  TH1D *h_mu_inclusive = new TH1D ("h_mu_inclusive", "", 100, 0, 100);
  h_mu_inclusive->Sumw2 ();
  TH1D *h_mu_dijet = new TH1D ("h_mu_dijet", "", 100, 0, 100);
  h_mu_dijet->Sumw2 ();

  //------------------
  // rho distributions
  //------------------
  TH1D *h_rho = new TH1D ("h_rho", "", 200, 0, 50);
  h_rho->Sumw2 ();
  TH1D *h_rho_without_wgt = new TH1D ("h_rho_without_wgt", "", 200, 0, 50); //MIRA: tady chybi Sumw2
  h_rho_without_wgt->Sumw2();												//OTA: Added
  TH1D *h_rho_wJetSelection = new TH1D ("h_rho_wJetSelection", "", 200, 0, 50);
  h_rho_wJetSelection->Sumw2 ();
  TH1D *h_rho_inclusive = new TH1D ("h_rho_inclusive", "", 200, 0, 50);
  h_rho_inclusive->Sumw2 ();


  // Initializing Number of Final Jets
  //int nInclusiveJets_CentralRegion = 0;
  int nInclusiveJets_y_Inclusive = 0;
  //int nDijets_y_Inclusive = 0;

  float weight = 1.;

  // Vector of jets for Dijets
  std::vector<TLorentzVector> RecoJets_y30;
  std::vector<TLorentzVector> TruthJets_y30;

  // Vector of jets for Inclusive jets
  std::vector<TLorentzVector> RecoJets;
  std::vector<TLorentzVector> TruthJets;

  // Cleaning
  int Skippedevents_Cleaning = 0; // Counter
  bool should_skip;


  //-----------------------------------------
  // Limiting number of events for debugging 
  //-----------------------------------------
  if(m_debug) entries = 10000;
  entries = 10000;

  //----------------------------------------------------------------------------------------
  // Loop over entries
  //----------------------------------------------------------------------------------------

  std::cout << "Loop over entries" << std::endl;

  for (int i = 0; i < entries; ++i)
    {
      should_skip = false;
      RecoJets_y30.clear ();
      TruthJets_y30.clear ();

      RecoJets.clear ();
      TruthJets.clear ();

      tree->GetEntry (i);

      // Show status
      if (i % 1000000 == 0)
        {
          std::cout << "Entry = " << i << " of " << entries << std::endl;
          std::cout << jet_pt->size () << std::endl;
        }
      if (m_debug)
        {
          std::cout << "Entry number: " << i << " out of " << entries <<  endl;
          std::cout << "Number of Jets in the event: " << truthjet_pt->size() << endl;
        }

      // Filling Weight
      if (MC)
        {
          if (m_PRW) weight = weight_pileup * mcEventWeight;
          else
            {
              weight = mcEventWeight;
            }
        }
      else
        {
          weight = 1;
        }

      if (m_rel21) njet = jet_pt->size ();
      if (m_rel21 && MC) ntruthjets = truthjet_pt->size();
      //MIRA: pro !Rel21 se to plni z vetve primo
      if (m_debug) std::cout << "Number of jets: " << njet << std::endl; 

      if (njet < 1) continue; // no jets: skip event
      //MIRA: tady je asi chybka, co delat kdyz name MC jety a ne reco jety?

      // Remove Pileup Events in MC
      if (MC)
        {
          double pTavg = jet_pt->at(0);
          if (njet > 1) pTavg = (jet_pt->at(0) + jet_pt->at(1)) / 2.0;
          if (ntruthjets == 0 || (pTavg / truthjet_pt->at(0) > 1.4))
            {
              if (m_debug) std::cout << "Skipping event because its pileup" << std::endl;
              continue; // skip event
            }
          if (m_generator == "Powheg" && runNumber == 426005 && mceventNumber == 1652845) continue; // See if it's still needed in MC15c TEMPORARY
        }

      //-----------------
      // Event Cleaning
      //-----------------

      bool passJetCleaning;
      if (m_debug)std::cout << "Applying Event Jet Cleaning" << std::endl;
      for (int j = 0; j < njet; ++j)
        { // Loop over jets
          if (j == 0) passJetCleaning = jet_clean_passTightBad->at(j); // TightBad for leading jets
          else
            {
              passJetCleaning = jet_clean_passLooseBad->at(j);
            } // LooseBad for the rest of jets
          //MIRA: tohle musime promyslet; chceme druhy jet taky tight?

          if (jet_pt->at(j) > 60 && passJetCleaning != 1)
            { // jet not passing cleaning criteria
              should_skip = true;
              break;
            }
        }
      if (m_debug)std::cout << "Checking if event should be skipped" << std::endl;
      if (should_skip)
        {
          Skippedevents_Cleaning++;
          continue; // skip event due to a non clean jet
        }

      //--------------------
      // Filling Histograms
      //--------------------

      // Fill Event Histograms
      if (m_debug) std::cout << "Filling Event histograms" << std::endl;
      h_NPV->Fill (NPV, weight);
      if (m_debug) std::cout << "Filled NPV" << std::endl;
      h_mu->Fill (mu, weight);
      if (m_debug) std::cout << "Filled MU" << std::endl;
      
      if (mu >= 14 && mu < 21) h_NPV_mu_14_21->Fill (NPV, weight); //MIRA: tohle bylo typo, bylo tu mu<14
      if (m_debug) std::cout << "Weigted MU 1/2" << std::endl;
      if (mu >= 21 && mu < 28) h_NPV_mu_21_28->Fill (NPV, weight);
      if (m_debug) std::cout << "Weigted MU 2/2" << std::endl;

      h_rho->Fill (rho * 0.001, weight);
      h_rho_without_wgt->Fill (rho * 0.001, 1);
      h_njet->Fill (njet, 1);


      if (m_debug) std::cout << "Check m_unfolding and m_bootstraps" << std::endl;

      if (m_unfolding && m_bootstraps)
        {
          if (MC) fGenerator->Generate (runNumber, eventNumber, runNumber);
          else fGenerator->Generate (runNumber, eventNumber, -1);
        }


      //---------------------------------
      // Filling Inclusive Distributions
      //---------------------------------

      if (m_debug) std::cout << "Filling Inclusive histograms" << std::endl;


      double pT_min = minPtTriggers[m_firstTrigger.Data ()];
      double mass_min = 300.;
      double mass_max = 9000.;

      if (m_debug) cout << "pT_min  " << pT_min << endl;

      // Loop over truth jets to fill inclusive-jet distributions
      if (MC)
        {
          if(m_debug) std::cout << "Loop over truthjets" << std::endl;
          for (int j = 0; j < ntruthjets; ++j)
            {
              if (truthjet_pt->at(j) >= pT_min && fabs (truthjet_rapidity->at(j)) < 3.0)
                {// Inclusive-jet selection
                  TLorentzVector Jet;
                  Jet.SetPtEtaPhiE (truthjet_pt->at(j), truthjet_eta->at(j), truthjet_phi->at(j), truthjet_E->at(j));
                  TruthJets.push_back (Jet);


                  for (unsigned int l = 0; l < nyBins; ++l)
                    { // Looping over different y bins
                      if (fabs (truthjet_rapidity->at(j)) >= yBins[l] &&
                          fabs (truthjet_rapidity->at(j)) < yBins[l + 1])
                        {
                          h_pt_truth[l]->Fill (truthjet_pt->at(j), weight);
                          //if(m_unfolding && m_bootstraps) fTruthBootstrap_inc[l]->Fill(truthjet_pt->at(j),weight);
                        }
                    }
                }
            }
          if(m_debug) 
            {
              std::cout << "ending truth jet loop" << std::endl;
              std::cout << "Number of TruthJets after cut: "<< TruthJets.size() << endl;
              std::cout << "Number of TruthJets in ntruthjets: "<< ntruthjets << endl;
            }
            if(ntruthjets != TruthJets.size())
              {
                std::cout << "Not machting size of TruthJets skipping" << std::endl;
                runNumber = -999;
                eventNumber = -999;
                mceventNumber = -999;
                mcEventWeight = -999;
                lumiBlock = -999;
                bcid = -999;
                wgt = -999.; //MIRA
                weight_pileup = -999.; //MIRA
                NPV = -999.;
                mu = -999.;
                rho = -999.;
                njet = -1;
                ntruthjets = -1;
                jet_pt->clear ();
                jet_eta->clear ();
                jet_phi->clear ();
                jet_E->clear ();
                jet_rapidity->clear ();
                jet_clean_passLooseBad->clear ();
                jet_clean_passTightBad->clear ();
                jet_width->clear ();
                jet_EMFrac->clear ();
                jet_Nsegments->clear ();
                if (MC)
                  {
                    truthjet_pt->clear ();
                    truthjet_eta->clear ();
                    truthjet_phi->clear ();
                    truthjet_E->clear ();
                    truthjet_rapidity->clear ();
                  }
                if (!MC) passedTriggers->clear ();
              }
            if(ntruthjets>1)
              {
                if(m_debug) std::cout << " ntruthjets > 1" << std::endl;
                double tmp_yStar, tmp_yBoost, tmp_dPhi, tmp_mass_dijet;
                TLorentzVector dijet;


                dijet = TruthJets.at(0) + TruthJets.at(1);
                tmp_yStar = GetYStar(TruthJets.at(0),TruthJets.at(1));
                tmp_yBoost = GetYBoost(TruthJets.at(0),TruthJets.at(1));
                if(m_debug) std::cout << " Dijet mass is: " << dijet.M() << std::endl;


                h2_yStar_yBoost_truth->Fill(tmp_yStar,tmp_yBoost,weight);



                tmp_dPhi = GetDeltaPhi(TruthJets.at(0),TruthJets.at(1));
                for (unsigned int l = 0; l < nyBins; ++l)
                { // Looping over different y bins
                  if (fabs (truthjet_rapidity->at(0)) >= yBins[l] &&
                      fabs (truthjet_rapidity->at(0)) < yBins[l+1] &&
                      fabs (truthjet_rapidity->at(1)) >= yBins[l] &&
                      fabs (truthjet_rapidity->at(1)) < yBins[l+1])
                    {
                      h_dPhi_truth[l]->Fill (tmp_dPhi, weight);
                      if((dijet.M() > mass_min) && 
                         (dijet.M() < mass_max))
                        h_m_dijet_truth[l]->Fill(dijet.M(),weight);
                    }
                }
                //filling ATLAS-like dPhiMax for potential a
                for(unsigned int l = 0;l < nystarBins; l++)
                {
                  if(fabs (truthjet_rapidity->at(0)) >= ystarBins[l] &&
                      fabs (truthjet_rapidity->at(0)) < ystarBins[l+1] &&
                      fabs (truthjet_rapidity->at(1)) >= ystarBins[l] &&
                      fabs (truthjet_rapidity->at(1)) < ystarBins[l+1])
                  {
                    h_dPhi_truth_ystarbin[l]->Fill(tmp_dPhi,weight);
                    for(unsigned int k = 0; k < nphiBins; k++)
                    {
                      if(tmp_dPhi < phiBins[k])
                        {h_dPhiMax_truth[l][k]->Fill(tmp_dPhi,weight);}
                    }
                  }
                }//end for l < nystarbin
              }//end if ntruthjet>1
        }//end if MC

      //MIRA: tohle je pak potreba pro to vedet ktery trigger je treba prevazit na prescale
      int trigUnprescaled = TriggertoInt[m_unprescaledTrig.Data ()];

      //-----------------------------------------------------------------------------------------------------
      // Loop over RECO Jets
      //-----------------------------------------------------------------------------------------------------

      std::string trigger = "";

      if (m_debug) std::cout << "Loop over Jets" << std::endl;

      for (int j = 0; j < njet; ++j)
        {

          TLorentzVector jet;
          jet.SetPtEtaPhiE (jet_pt->at(j), jet_eta->at(j), jet_phi->at(j), jet_E->at(j));

          if (fabs (jet_rapidity->at(j)) < 3.0) RecoJets_y30.push_back (jet); // Saving jets for Dijets distributions

          int trig = 0;

          if (m_debug) std::cout << "Applying Inclusive Jet Selection" << std::endl;
          //cout << njet << "jet " << j << "   pt: " << jet_pt->at(j) << "  rap:" << jet_rapidity->at(j) << endl;

          
          if (jet_pt->at(j) >= pT_min && fabs (jet_rapidity->at(j)) < 3.)
            { // Due to efficiency lowest-pt trigger

              if (!MC)
                { // Data
                  // Deciding Trigger and Weight
                  for (unsigned int m = firstTrig; m < Triggers.size (); m++)
                    {
                      if (jet_pt->at(j) >= minPtTriggers[Triggers.at(m)] &&
                          jet_pt->at(j) < maxPtTriggers[Triggers.at(m)])
                        {
                          trigger = Triggers.at(m);
                        }
                    }
                  if (m_debug) std::cout << "After deciding trigger: " << trigger << std::endl;
                  trig = TriggertoInt[trigger];
                  if (m_debug) std::cout << "Corresponding number for the trigger: " << trig << std::endl;
                  if (trigUnprescaled == trig) weight = 1.;
                  else
                    {
                      weight = wgtDijets.at(trigUnprescaled).at(trigUnprescaled) / wgtDijets.at(trig).at(trig);
                    }
                  if (m_debug) std::cout << "weight decided: " << weight << std::endl;
                  bool trigger_passed = false;
                  for (unsigned int lll = 0; lll < passedTriggers->size (); lll++)
                    {// Loop over passed triggers
                      //MIRA: tohle jsme taky delali - kontrolujeme ze zafiroval trigger co mel podle pt jetu
                      if (passedTriggers->at(lll) == trigger) trigger_passed = true;
                    }
                  if (!trigger_passed) continue; // skip jet
                  //if(m_debug)
                  std::cout << "Trigger passed!" << std::endl;

                  //MIRA: plni se nejake histogramy per trigger
                }//Data only

              if (MC)
                {
                  if (m_debug) std::cout << "Eta distributions in pT Bins (Triggers)" << std::endl;
                  for (unsigned int m = firstTrig; m < Triggers.size (); m++)
                    {
                      if (jet_pt->at(j) >= minPtTriggers[Triggers.at(m)] &&
                          jet_pt->at(j) < maxPtTriggers[Triggers.at(m)])
                        {
                          //MIRA: to same pro MC RECO jety az na to ze vsechny triggry jsou jako ze zafirovaly ... takze se jen checkuje to pt

                          //MIRA: plni se nejake histogramy per trigger
                        }
                    }
                }//MC only


              h_wgt->Fill (weight);
              nInclusiveJets_y_Inclusive++;
              h_NPV_wJetSelection->Fill (NPV, weight);
              h_rho_wJetSelection->Fill (rho * 0.001, weight);
              h_mu_wJetSelection->Fill (mu, weight);

              h_pt_with_wgt_y_inclusive->Fill (jet_pt->at(j), weight);

              if (j == 0)
                { // Leading pT jets
                  h_pt_leading_y_inclusive->Fill(jet_pt->at(j),weight);
                  h_mu_inclusive->Fill (mu, weight);
                  h_NPV_inclusive->Fill (NPV, weight);
                  h_rho_inclusive->Fill (rho * 0.001, weight);
                }
            }// End if (jet_ptmin jet absrap...)

        }// End: for j < njets




      //------------------------------
      // Filling Dijets distributions
      //------------------------------

      //MIRA: tady se jde na novou selekci tak se resetuje weight eventu
      if (!MC) weight = 1;

      if (m_runDijets)
        {
	  /////////		
      for (int ybin = 0; ybin < nyBins; ++ybin)
      {
        histname_tmp = "h_pt_ybin";
        histname_tmp += ybin;
        h_pt[ybin] = new TH1D (histname_tmp.Data (), "", nptBins[ybin], ptBins_0);
        h_pt[ybin]->Sumw2 ();

        histname_tmp = "h_m_dijet_truth_ybin";
        histname_tmp += ybin;
        h_m_dijet[ybin] = new TH1D (histname_tmp.Data (), "", nMassBins[ybin], massBins_0);
        h_m_dijet[ybin]->Sumw2 ();
      }
      ///////                   
         if(m_debug){
              //std::cout << "h_pt->size:  " << sizeof(h_pt)/sizeof(*TH1D) << std::endl; 
              std::cout << "h_pt->size:  " << sizeof(h_pt)/sizeof(h_pt[0]) << std::endl; 

              for(int jj=0; jj<  sizeof(h_pt)/sizeof(h_pt[0]); jj++)  std::cout << jj << " in loop h_pt:" << "typeid():  " << typeid(h_pt[jj]).name() << std::endl; 
          }

          if (m_debug) std::cout << "Filling dijets histograms" << std::endl;

          double massReco = -1;
          double yStarReco = -1;
          double massTruth = -1; 
          double yStarTruth = -1;
          int yStarRecoBin = -1;
          int yStarTruthBin = -1;
          std::string trigger_j0 = "";
          std::string trigger_j1 = "";
          int trigj0 = 0;
          int trigj1 = 0;
          bool passReco_dijets = false;
          bool passTruth_dijets = false;
          if (RecoJets_y30.size () > 1)
            { // At least two Reco Jets

              // Trigger
              if (!MC)
                {
                  // Deciding Trigger and Weight
                  for (unsigned int m = firstTrig; m < Triggers.size (); m++)
                    {
                      if (RecoJets_y30.at(0).Pt () >= minPtTriggers[Triggers.at(m)] &&
                          RecoJets_y30.at(0).Pt () < maxPtTriggers[Triggers.at(m)])
                        {
                          trigger_j0 = Triggers.at(m);
                        }
                      if (RecoJets_y30.at(1).Pt () >= minPtTriggers[Triggers.at(m)] &&
                          RecoJets_y30.at(1).Pt () < maxPtTriggers[Triggers.at(m)])
                        {
                          trigger_j1 = Triggers.at(m);
                        }
                    }
                  if (trigger_j0 == "" || trigger_j1 == "") continue; // skip event
                  trigj0 = TriggertoInt[trigger_j0];
                  trigj1 = TriggertoInt[trigger_j1];
                  if (trigUnprescaled == trigj1) weight = 1.;
                  else
                    {
                      weight = wgtDijets.at(trigUnprescaled).at(trigUnprescaled) / wgtDijets.at(trigj0).at(trigj1);
                    }
                  bool trigger_j0_passed = false;
                  bool trigger_j1_passed = false;
                  for (unsigned int lll = 0; lll < passedTriggers->size (); lll++)
                    {// Loop over passed triggers
                      if (passedTriggers->at(lll) == trigger_j0) trigger_j0_passed = true;
                      if (passedTriggers->at(lll) == trigger_j1) trigger_j1_passed = true;
                    }
                  if (!(trigger_j0_passed || trigger_j1_passed)) continue; // skip event
                }
              else
                { // MC
                  if (!(RecoJets_y30.at(1).Pt () >= minPtTriggers[Triggers.at(firstTrig)])) continue; // skip event to be consistent with Data
                }
     
              if(m_debug) std::cout << "Filling dijets histograms - Dijet selection" << std::endl;

              // Dijet selection
              if (RecoJets_y30.at(0).Pt () > m_j0min &&
                  ((RecoJets_y30.at(0).Pt ())+(RecoJets_y30.at(1).Pt ())) > m_HT2min &&
                  RecoJets_y30.at(1).Pt () > m_j1min)
                {
                  passReco_dijets = true;
                  if(m_debug) std::cout << "    Filling dijets histograms - for loop conmtinues" << std::endl;

                  for (unsigned int l = 0; l < nyBins; ++l)
                    { // Looping over different y bins
                      if(m_debug) std::cout << "        Filling dijets histograms - for loop - bin: " << l << "of " << nyBins << std::endl;

                      if (fabs (RecoJets_y30.at(0).Rapidity ()) >= yBins[l] &&
                          fabs (RecoJets_y30.at(0).Rapidity ()) < yBins[l + 1])
                        {
                          if(m_debug) std::cout << "        Filling dijets histograms - for loop - bin: " << l << " h_pt[l]->Fill( ... )" << std::endl;
                          if(m_debug) std::cout << "          RecoJets_y30.at(0).Pt() : " << RecoJets_y30.at(0).Pt() << std::endl;
                          if(m_debug) std::cout << "          weight : " << weight << std::endl;
                          if(m_debug) std::cout << "h_pt[l] typeid():" << typeid(h_pt[l]).name() << std::endl; 
                          //if(m_debug){
							//  TString testName = "Test Name";
							  //std::cout << testName << std::endl;
							  //h_pt[l]->SetName(testName);
							  //std::cout << "h_pt[" << l << "] name:" << h_pt[l]->GetName() << std::endl; 
                         //}
                         
                          h_pt[l]->Fill(RecoJets_y30.at(0).Pt(),weight);
                          if(m_debug) std::cout << "          afrer calling h_pt->fill" << std::endl;

                        }
                      if (fabs (RecoJets_y30.at(1).Rapidity ()) >= yBins[l] &&
                          fabs (RecoJets_y30.at(1).Rapidity ()) < yBins[l + 1])
                        {
                          if(m_debug) std::cout << "          EMPTY IF BLOCK : " << weight << std::endl;
                          //h_j1_pt_mjj[l]->Fill(RecoJets_y30.at(1).Pt(),weight);
                        }
                    }
                    
                  if(m_debug) std::cout << "Filling dijets histograms - h_mu_dijet->Fill (mu, weight)" << std::endl;
                  h_mu_dijet->Fill (mu, weight);
                  // rework using auxiliary functions for better readability 
                  yStarReco = GetYStar(RecoJets_y30.at(0), RecoJets_y30.at(1));
                  yStarRecoBin = GetYStarBin (yStarReco);

                  //h_ystar->Fill(yStarReco,weight);
                  massReco = ((RecoJets_y30.at(0))+(RecoJets_y30.at(1))).M ();
                  //h_m12_reco_ystar_inclusive->Fill(massReco,weight);//y*<3


                }// End: if RECO whatever

            }// End: if RecoJets.size()>1

          if(m_debug) std::cout << "Filling dijets histograms - TruthJets_y30.size() > 1 Continues" << std::endl;

          if (TruthJets_y30.size () > 1)
            { // At least two Truth Jets

              // Dijet selection - truth
              if (TruthJets_y30.at(0).Pt () > m_j0min &&
                  ((TruthJets_y30.at(0).Pt ())+(TruthJets_y30.at(1).Pt ())) > m_HT2min &&
                  TruthJets_y30.at(1).Pt () > m_j1min)
                {
                  passTruth_dijets = true;
                  yStarTruth = GetYStar(TruthJets_y30.at(0),TruthJets_y30.at(1));
                  yStarTruthBin = GetYStarBin (yStarTruth);
                  massTruth = ((TruthJets_y30.at(0))+(TruthJets_y30.at(1))).M ();
                }

            }// End: if(TruthJets_y30.size()>1)


        }// End: if(m_runDijets)

      if(m_debug) std::cout << "Start Clean and reset." << std::endl;
        runNumber = -999;
        eventNumber = -999;
        mceventNumber = -999;
        mcEventWeight = -999;
        lumiBlock = -999;
        bcid = -999;
        wgt = -999.; //MIRA
        weight_pileup = -999.; //MIRA
        NPV = -999.;
        mu = -999.;
        rho = -999.;
        njet = -1;
        ntruthjets = -1;
        jet_pt->clear ();
        jet_eta->clear ();
        jet_phi->clear ();
        jet_E->clear ();
        jet_rapidity->clear ();
        jet_clean_passLooseBad->clear ();
        jet_clean_passTightBad->clear ();
        jet_width->clear ();
        jet_EMFrac->clear ();
        jet_Nsegments->clear ();
        if (MC)
          {
            truthjet_pt->clear ();
            truthjet_eta->clear ();
            truthjet_phi->clear ();
            truthjet_E->clear ();
            truthjet_rapidity->clear ();
          }
        if (!MC) passedTriggers->clear ();
        if(m_debug) std::cout << "All cleaned." << std::endl;
    } // End: for (entries)

  //------------------------------------
  // Saving Histogramas into a ROOT file
  //------------------------------------

  std::cout << "Saving plots into a ROOT file..." << std::endl; 
  // Opening output file
  TFile* tout;
  //if(!m_debug){
  tout = new TFile (m_outputFile, "recreate");
  std::cout << "output file = " << m_outputFile << std::endl;
  tout->cd ();
  TString dirname = "d_ycuts";
  TDirectory *d_ycuts = tout->mkdir(dirname.Data());
  dirname = "d_ystarcuts";
  TDirectory *d_ystarcuts = tout->mkdir(dirname.Data());

  TDirectory *d_yDir[nyBins];
  TDirectory *d_ystarDir[nystarBins];

  // Writing histograms
  h_wgt->Write ();
  h_njet->Write ();

  h_NPV->Write ();
  h_NPV_inclusive->Write ();
  h_NPV_wJetSelection->Write ();
  h_NPV_mu_14_21->Write ();
  h_NPV_mu_21_28->Write ();

  h_mu->Write ();
  h_mu_wJetSelection->Write ();
  h_mu_inclusive->Write ();
  h_mu_dijet->Write ();

  h_rho->Write ();
  h_rho_inclusive->Write ();
  h_rho_wJetSelection->Write ();
  h_rho_without_wgt->Write ();

  h_pt_with_wgt_y_inclusive->Write ();
  h_pt_leading_y_inclusive->Write();

  h2_yStar_yBoost_truth->Write();
  
  d_ycuts->cd();
  for (unsigned int i = 0; i < nyBins; ++i)
    {
      dirname = yDirNames[i];
      d_yDir[i] = d_ycuts->mkdir(dirname.Data());
      d_yDir[i]->cd();
      if(MC)
      {
        h_pt_truth[i]->Write ();
        h_dPhi_truth[i]->Write();
        h_m_dijet_truth[i]->Write();
      }
      else
      {
        h_pt[i]->Write ();
        h_dPhi[i]->Write();
        h_m_dijet[i]->Write();
      }
      d_yDir[i]->Write();
      
    }
  d_ycuts->Write();


  d_ystarcuts->cd();
  for(unsigned int i = 0; i < nystarBins;i++)
    {
      dirname = ystarDirNames[i];
      d_ystarDir[i] = d_ystarcuts->mkdir(dirname.Data());
      d_ystarDir[i]->cd();
      if(MC)
      {  
        h_dPhi_truth_ystarbin[i]->Write();
        for (int j = 0; j < nphiBins; j++)
        {
          h_dPhiMax_truth[i][j]->Write();
        }
      }
      else
      {
        h_dPhi_ystarbin[i]->Write();
        for (int j = 0; j < nphiBins; j++)
        {
          h_dPhiMax[i][j]->Write();
        }
      }
      d_ystarDir[i]->Write();
    }
  d_ystarcuts->Write();

  tout->Close ();
  //}//End: if debug

  std::cout << "Number of Jets (|y|<3.0): " << nInclusiveJets_y_Inclusive << std::endl;
  std::cout << "Skipped events due to cleaning: " << Skippedevents_Cleaning << std::endl;

  std::cout << "Done!" << std::endl;

  return 0;

}

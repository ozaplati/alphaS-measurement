///////////////////////////////////
//
// global.h
//
// Jonathan Bossio  - 27 June 2015
// 
///////////////////////////////////

//----------------------
// Applied to both plots
//----------------------

//----
// rho
//----
double __xmin_rho = 0.;
double __xmax_rho = 20.;
double __ymin_rho = 0;
double __ymax_rho = 2000000.;

//----
// mjj
//----
double __xmin_mjj = 200.;
double __xmax_mjj = 10000.;

//----
// pT
//----
double __xmin_pT = 116.;
//double __xmax_pT = 3936.;
double __xmax_pT = 4940.;

//----
// phi
//----
double __xmin_phi = -3.;
double __xmax_phi = 3.;
double __ymin_phi = 0;

//----
// eta
//----
double __xmin_eta = -3.5;
double __xmax_eta = 3.5;


//----------
// trackWITH
//----------
double __xmin_trackWIDTH = 0;
double __xmax_trackWIDTH = 0.2;


//------------
// Ratio plots
//------------

//----
// pT
//----
//double __ymin_Ratio_pT = 0.5;
//double __ymin_Ratio_pT = 0.8;
double __ymin_Ratio_pT = 0.8;
//double __ymin_Ratio_pT = 0.96;
//double __ymax_Ratio_pT = 1.3;
//double __ymax_Ratio_pT = 1.1;
//double __ymax_Ratio_pT = 1.3;
double __ymax_Ratio_pT = 1.4;

//----
// mjj
//----
double __ymin_Ratio_mjj = 0.6;
double __ymax_Ratio_mjj = 1.3;

//----
// eta
//----
//double __ymin_Ratio_eta = 0.9;
double __ymin_Ratio_eta = 0.6;
//double __ymax_Ratio_eta = 1.15;
double __ymax_Ratio_eta = 1.6;

//----
// phi
//----
double __ymin_Ratio_phi = 0.6;
double __ymax_Ratio_phi = 1.4;

//----
// y
//----
double __ymin_Ratio_y = 0.4;
double __ymax_Ratio_y = 2.0;

//----
// rho
//----
double __ymin_Ratio_rho = 0.7;
double __ymax_Ratio_rho = 1.6;

//----
// mu
//----
double __ymin_Ratio_mu = 0.8;
double __ymax_Ratio_mu = 1.2;

//-----------
// trackWIDTH
//-----------
double __ymin_Ratio_trackWIDTH = 0.4;
double __ymax_Ratio_trackWIDTH = 1.6;

//-----------
// nTrk
//-----------
double __ymin_Ratio_nTrk = 0.6;
double __ymax_Ratio_nTrk = 1.6;

//-----------
// EMFrac
//-----------
double __ymin_Ratio_EMFrac = 0.2;
double __ymax_Ratio_EMFrac = 2;

//-----------
// JVF
//-----------
double __ymin_Ratio_JVF = 0;
double __ymax_Ratio_JVF = 5;

//-----------
// width
//-----------
double __ymin_Ratio_width = 0;
double __ymax_Ratio_width = 2;


// Triggers
std::vector<std::string > Triggers;
